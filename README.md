Includes:

* The LaTeX documentation of my masters's thesis with the title: "A Framework-Based Approach to the Development of Multiplayer Serious Games".
* The Unity3D project of the developed framework including example games which were requested by local schools.