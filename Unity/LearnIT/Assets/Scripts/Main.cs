﻿using UnityEngine;
using System.Collections.ObjectModel;
using System.Collections.Generic;

public static class Main {

    //Use this to add new games to the game selection
    public static void main()
    {
        CreateInformationTheory();
        CreateComplexity();
        CreateCustomGame();
    }

    private static void CreateCustomGame()
    {
        Game custom = new Game(Global.STRING_CUSTOM_TITLE);
        custom.AddGameMode(new GameModeTheory(TheoryLoader.THEORY_CUSTOM, Global.STRING_THEORY_NAME, string.Format(Global.STRING_THEORY_DESCRIPTION, Global.STRING_CUSTOM_TITLE), Global.PATH_ICON_THEORY, SceneLoader.SCENE_THEORY));
        custom.AddGameMode(new GameModeQuiz(QuizLoader.QUIZ_CUSTOM, Global.STRING_CUSTOM_QUIZ_NAME, Global.STRING_CUSTOM_QUIZ_DESCRIPTION, Global.PATH_ICON_QUIZ, SceneLoader.SCENE_QUIZ, Global.PATH_TUTORIAL_QUIZ_SINGLE_PLAYER));
        custom.AddGameMode(new GameModeQuiz(QuizLoader.QUIZ_CUSTOM, Global.STRING_CUSTOM_QUIZ_MP_NAME, Global.STRING_CUSTOM_QUIZ_MP_DESCRIPTION, Global.PATH_ICON_QUIZ_MP, SceneLoader.SCENE_QUIZ_MP, Global.PATH_TUTORIAL_QUIZ_MULTI_PLAYER, MPQuizPlayer.PREFAB_PATH));
    }

    private static void CreateInformationTheory()
    {
        Game it = new Game(Global.STRING_IT_TITLE);

        //sp game modes
        it.AddGameMode(new GameModeTheory(TheoryLoader.THEORY_IT, Global.STRING_THEORY_NAME, string.Format(Global.STRING_THEORY_DESCRIPTION, Global.STRING_IT_TITLE), Global.PATH_ICON_THEORY, SceneLoader.SCENE_THEORY));
        it.AddGameMode(new GameMode(Global.STRING_IT_NUMBER_SURVIVAL_NAME, Global.STRING_IT_NUMBER_SURVIVAL_DESCRIPTION, Global.PATH_ICON_IT_NUMBER_GUESSING_SURVIVAL, SceneLoader.SCENE_IT_NUMBER_SURVIVAL, Global.PATH_TUTORIAL_NUMBER_SURVIVAL));
        it.AddGameMode(new GameMode(Global.STRING_IT_NUMBER_ARCADE_NAME, Global.STRING_IT_NUMBER_ARCADE_DESCRIPTION, Global.PATH_ICON_IT_NUMBER_GUESSING_ARCADE, SceneLoader.SCENE_IT_NUMBER_ARCADE, Global.PATH_TUTORIAL_NUMBER_ARCADE));
        it.AddGameMode(new GameModeQuiz(QuizLoader.QUIZ_IT, Global.STRING_IT_QUIZ_NAME, Global.STRING_IT_QUIZ_DESCRIPTION, Global.PATH_ICON_QUIZ, SceneLoader.SCENE_QUIZ, Global.PATH_TUTORIAL_QUIZ_SINGLE_PLAYER));

        //mp game modes
        it.AddGameMode(new GameMode(Global.STRING_IT_NUMBER_SURVIVAL_MP_NAME, Global.STRING_IT_NUMBER_SURVIVAL_MP_DESCRIPTION, Global.PATH_ICON_IT_NUMBER_GUESSING_MP_KOOP, SceneLoader.SCENE_IT_NUMBER_SURVIVAL_MP, Global.PATH_TUTORIAL_NUMBER_COOP, MPITNumberPlayer.PREFAB_PATH));
        it.AddGameMode(new GameMode(Global.STRING_IT_NUMBER_BLIND_SURVIVAL_MP_NAME, Global.STRING_IT_NUMBER_BLIND_SURVIVAL_MP_DESCRIPTION, Global.PATH_ICON_IT_NUMBER_GUESSING_MP_BLIND, SceneLoader.SCENE_IT_NUMBER_BLIND_SURVIVAL_MP, Global.PATH_TUTORIAL_NUMBER_COOP_BLIND, MPITNumberPlayer.PREFAB_PATH));
        it.AddGameMode(new GameModeQuiz(QuizLoader.QUIZ_IT, Global.STRING_IT_QUIZ_MP_NAME, Global.STRING_IT_QUIZ_MP_DESCRIPTION, Global.PATH_ICON_QUIZ_MP, SceneLoader.SCENE_QUIZ_MP, Global.PATH_TUTORIAL_QUIZ_MULTI_PLAYER, MPQuizPlayer.PREFAB_PATH));
    }

    private static void CreateComplexity()
    {
        //possible colors for graph colouring
        Color[] two_colors = new Color[2] { Color.red, Color.green };
        Color[] three_colors = new Color[3] { Color.red, Color.green, Color.blue };
        Color[] four_colors = new Color[4] { Color.red, Color.green, Color.blue, Color.yellow };

        //graph data
        List<GCGraphData> simple = new List<GCGraphData>();
        simple.Add(new GCGraphData("gc00", 1024, 3, 20f, two_colors));
        simple.Add(new GCGraphData("gc01", 1024, 3, 20f, two_colors));
        simple.Add(new GCGraphData("gc02", 1024, 3, 25f, two_colors));
        simple.Add(new GCGraphData("gc03", 1024, 4, 30f, three_colors));
        simple.Add(new GCGraphData("gc04", 1024, 5, 30f, three_colors));

        List<GCGraphData> logos = new List<GCGraphData>();
        logos.Add(new GCGraphData("gc_logo_chrome", 5, 3, 20f, four_colors));
        logos.Add(new GCGraphData("gc_logo_superman", 10, 3, 25f, two_colors));
        logos.Add(new GCGraphData("gc_logo_bmw", 3, 4, 30f, three_colors));
        logos.Add(new GCGraphData("gc_logo_batman", 5, 4, 30f, three_colors));
        logos.Add(new GCGraphData("gc_logo_captain", 2, 5, 40f, three_colors));

        List<GCGraphData> simple_maps = new List<GCGraphData>();
        simple_maps.Add(new GCGraphData("gc_cs0", 6, 3, 20f, three_colors));
        simple_maps.Add(new GCGraphData("gc_cs1", 7, 3, 20f, two_colors));
        simple_maps.Add(new GCGraphData("gc_cs2", 6, 4, 25f, three_colors));
        simple_maps.Add(new GCGraphData("gc_cs3", 6, 4, 30f, four_colors));

        List<GCGraphData> complex_maps = new List<GCGraphData>();
        complex_maps.Add(new GCGraphData("gc_map_saar", 2, 2, 20f, three_colors));
        complex_maps.Add(new GCGraphData("gc_map_ger", 2, 4, 60f, four_colors));
        complex_maps.Add(new GCGraphData("gc_map_afg", 2, 6, 100f, four_colors));
        complex_maps.Add(new GCGraphData("gc_map_usa", 2, 8, 140f, four_colors));

        List<GCGraphData> all_maps = new List<GCGraphData>();
        all_maps.AddRange(simple_maps);
        all_maps.AddRange(complex_maps);

        List<GCGraphData> master = new List<GCGraphData>();
        master.Add(new GCGraphData("gc_max", 3, 25, 300f, four_colors));

        //determine 2 color and 4 color data entries, for coop mode
        List<GCGraphData> coop_two_color = new List<GCGraphData>();
        List<GCGraphData> coop_four_color = new List<GCGraphData>();
        foreach (GCGraphData data in GCGraphData.GetAllGraphData())
        {
            int numOfColors = data.GetUsableColors().Length;
            if (numOfColors == 2)
                coop_two_color.Add(data);
            else if (numOfColors == 4)
                coop_four_color.Add(data);
        }

        //Game and Game Modes
        Game cp = new Game(Global.STRING_CP_TITLE);

        //singleplayer modes
        cp.AddGameMode(new GameModeTheory(TheoryLoader.THEORY_CP, Global.STRING_THEORY_NAME, string.Format(Global.STRING_THEORY_DESCRIPTION, Global.STRING_CP_TITLE), Global.PATH_ICON_THEORY, SceneLoader.SCENE_THEORY));
        cp.AddGameMode(new GameModeCP(simple, Global.STRING_CP_GC_EASY, Global.STRING_CP_GC_EASY_DESCRIPTION, Global.PATH_ICON_CP_GC_EASY, SceneLoader.SCENE_CP_GC_SURVIVAL, Global.PATH_TUTORIAL_GC, null));
        cp.AddGameMode(new GameModeCP(logos, Global.STRING_CP_GC_INTERMEDIATE, Global.STRING_CP_GC_INTERMEDIATE_DESCRIPTION, Global.PATH_ICON_CP_GC_INTERMEDIATE, SceneLoader.SCENE_CP_GC_SURVIVAL, Global.PATH_TUTORIAL_GC, null));
        cp.AddGameMode(new GameModeCP(simple_maps, Global.STRING_CP_GC_MEDIUM, Global.STRING_CP_GC_MEDIUM_DESCRIPTION, Global.PATH_ICON_CP_GC_MEDIUM, SceneLoader.SCENE_CP_GC_ARCADE, Global.PATH_TUTORIAL_GC, null));
        cp.AddGameMode(new GameModeCP(complex_maps, Global.STRING_CP_GC_HARD, Global.STRING_CP_GC_HARD_DESCRIPTION, Global.PATH_ICON_CP_GC_HARD, SceneLoader.SCENE_CP_GC_ARCADE, Global.PATH_TUTORIAL_GC, null));
        cp.AddGameMode(new GameModeCP(master, Global.STRING_CP_GC_EXTREME, Global.STRING_CP_GC_EXTREME_DESCRIPTION, Global.PATH_ICON_CP_GC_EXTREME, SceneLoader.SCENE_CP_GC_ARCADE, Global.PATH_TUTORIAL_GC, null));
        cp.AddGameMode(new GameModeQuiz(QuizLoader.QUIZ_CP, Global.STRING_CP_QUIZ_NAME, Global.STRING_CP_QUIZ_DESCRIPTION, Global.PATH_ICON_QUIZ, SceneLoader.SCENE_QUIZ, Global.PATH_TUTORIAL_QUIZ_SINGLE_PLAYER));

        //duell modes
        cp.AddGameMode(new GameModeCP(simple, Global.STRING_CP_GC_EASY_MP, Global.STRING_CP_GC_EASY_DESCRIPTION_MP, Global.PATH_ICON_CP_GC_EASY_MP, SceneLoader.SCENE_CP_GC_MP, Global.PATH_TUTORIAL_GC_DUELL, MPGCPlayer.PREFAB_PATH));
        cp.AddGameMode(new GameModeCP(logos, Global.STRING_CP_GC_INTERMEDIATE_MP, Global.STRING_CP_GC_INTERMEDIATE_DESCRIPTION_MP, Global.PATH_ICON_CP_GC_INTERMEDIATE_MP, SceneLoader.SCENE_CP_GC_MP, Global.PATH_TUTORIAL_GC_DUELL, MPGCPlayer.PREFAB_PATH));
        cp.AddGameMode(new GameModeCP(all_maps, Global.STRING_CP_GC_HARD_MP, Global.STRING_CP_GC_HARD_DESCRIPTION_MP, Global.PATH_ICON_CP_GC_HARD_MP, SceneLoader.SCENE_CP_GC_MP, Global.PATH_TUTORIAL_GC_DUELL, MPGCPlayer.PREFAB_PATH));
        cp.AddGameMode(new GameModeCP(master, Global.STRING_CP_GC_EXTREME_MP, Global.STRING_CP_GC_EXTREME_DESCRIPTION_MP, Global.PATH_ICON_CP_GC_EXTREME_MP, SceneLoader.SCENE_CP_GC_MP, Global.PATH_TUTORIAL_GC_DUELL, MPGCPlayer.PREFAB_PATH));
        cp.AddGameMode(new GameModeCP(null, Global.STRING_CP_GC_RANDOM_MP, Global.STRING_CP_GC_RANDOM_DESCRIPTION_MP, Global.PATH_ICON_CP_GC_RANDOM_MP, SceneLoader.SCENE_CP_GC_MP, Global.PATH_TUTORIAL_GC_DUELL, MPGCPlayer.PREFAB_PATH));

        //coop modes
        cp.AddGameMode(new GameModeCP(coop_two_color, Global.STRING_CP_GC_COOP_ONE_MP, Global.STRING_CP_GC_COOP_ONE_DESCRIPTION_MP, Global.PATH_ICON_CP_GC_COOP_ONE, SceneLoader.SCENE_CP_GC_COOP_MP, Global.PATH_TUTORIAL_GC_COOP, MPGCPlayerCoop.PREFAB_PATH));
        cp.AddGameMode(new GameModeCP(coop_four_color, Global.STRING_CP_GC_COOP_TWO_MP, Global.STRING_CP_GC_COOP_TWO_DESCRIPTION_MP, Global.PATH_ICON_CP_GC_COOP_TWO, SceneLoader.SCENE_CP_GC_COOP_MP, Global.PATH_TUTORIAL_GC_COOP, MPGCPlayerCoop.PREFAB_PATH));

        //quiz duell
        cp.AddGameMode(new GameModeQuiz(QuizLoader.QUIZ_CP, Global.STRING_CP_QUIZ_MP_NAME, Global.STRING_CP_QUIZ_MP_DESCRIPTION, Global.PATH_ICON_QUIZ_MP, SceneLoader.SCENE_QUIZ_MP, Global.PATH_TUTORIAL_QUIZ_MULTI_PLAYER, MPQuizPlayer.PREFAB_PATH));
    }
}
