﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Utils;

public class UIGameMenuTabHolder : UIElement {
    private UIGameMenuTab leftTab;
    private UIGameMenuTab rightTab;

    public UIGameMenuTab GetLeftTab()
    {
        return leftTab;
    }

    public UIGameMenuTab GetRightTab()
    {
        return rightTab;
    }

    void Awake()
    {
        rightTab = UIElement.Instantiate<UIGameMenuTab>(Global.PATH_PREFABS_DIR + "UIGameMenuTabRight", this.transform);
        leftTab = UIElement.Instantiate<UIGameMenuTab>(Global.PATH_PREFABS_DIR + "UIGameMenuTabLeft", this.transform);

        //change tab on button click
        rightTab.GetTabButton().onClick.AddListener(() =>
        {
            SetRightTabActive();
        });
        leftTab.GetTabButton().onClick.AddListener(() =>
        {
            SetLeftTabActive();
        });
    }

    public void SetRightTabActive()
    {
        rightTab.transform.SetAsLastSibling();
        rightTab.GetTabButton().image.color = Color.white;
        rightTab.GetTabContent().gameObject.SetActive(true);

        leftTab.GetTabButton().image.color = Global.COLOR_INACTIVE_GRAY;
        leftTab.GetTabContent().gameObject.SetActive(false);
    }

    public void SetLeftTabActive()
    {
        leftTab.transform.SetAsLastSibling();
        leftTab.GetTabButton().image.color = Color.white;
        leftTab.GetTabContent().gameObject.SetActive(true);

        rightTab.GetTabButton().image.color = Global.COLOR_INACTIVE_GRAY;
        rightTab.GetTabContent().gameObject.SetActive(false);
    }
}
