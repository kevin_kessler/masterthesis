﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Utils;

public class UIGameMenuTabContent : UIElement {
    [SerializeField] private GameObject gameModesHolder;
    private UISimpleToggleListGroup gameModes;

    [SerializeField] private GameObject gameModeInfoHolder;
    private UIGameMenuModeInfo gameModeInfo;

    public UISimpleToggleListGroup GetGameModeList()
    {
        return gameModes;
    }

    public UIGameMenuModeInfo GetGameModeInfo()
    {
        return gameModeInfo;
    }

    void Awake()
    {
        gameModes = UIElement.Instantiate<UISimpleToggleListGroup>(gameModesHolder.transform);
        gameModes.SetListTitle(Global.STRING_GAME_MODE);
        gameModes.SetAlignmentOfScrollbar(false);

        gameModeInfo = UIElement.Instantiate<UIGameMenuModeInfo>(gameModeInfoHolder.transform);
        gameModeInfo.SetHeaderText(Global.STRING_GAME_MODE_INFO);

    }

}
