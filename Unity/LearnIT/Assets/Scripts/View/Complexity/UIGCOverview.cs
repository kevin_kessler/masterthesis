﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine.Events;
public class UIGCOverview : UIGameOverview {

    [SerializeField] private Text colTextCorrect;
    [SerializeField] private Text graphTableGraphText;
    [SerializeField] private Text graphTableSolutionText;
    [SerializeField] private GameObject graphTablePlayerNameList;
    [SerializeField] private GameObject graphTablePlayerNamePrefab;
    [SerializeField] private GameObject graphTableResultList;
    private UIGCResultEntry[] gcResultEntries;

    protected override void Awake()
    {
        base.Awake();

        colTextCorrect.text = Global.STRING_OVERVIEW_CORRECT;
        graphTableGraphText.text = Global.STRING_OVERVIEW_QUESTION;
        backToLobbyIndicationText.text = Global.STRING_OVERVIEW_WAITING_FOR_PLAYER;
        graphTableSolutionText.text = Global.STRING_SOLUTION;
    }

    public override void AddPlayerUI(UIMPGamePlayer playerUI)
    {
        base.AddPlayerUI(playerUI);

        GameObject playerNameHolder = Instantiate(graphTablePlayerNamePrefab);
        playerNameHolder.transform.SetParent(graphTablePlayerNameList.transform, false);
        playerNameHolder.SetActive(true);
        Text playerName = playerNameHolder.GetComponent<Text>();
        playerName.text = playerUI.GetPlayerName();
    }

    public void InitGraphResults(ReadOnlyCollection<GCGraph> graphs, UnityAction<int> graphSolutionButtonHandler)
    {
        gcResultEntries = new UIGCResultEntry[graphs.Count];
        for (int i = 0; i < graphs.Count; i++)
        {
            UIGCResultEntry result = UIElement.Instantiate<UIGCResultEntry>(graphTableResultList.transform);
            result.SetGraph(i, graphs[i].GetImage(), playerUIs.Count);
            result.SetOnSolutionButtonHandler(graphSolutionButtonHandler);
            gcResultEntries[i] = result;
        }
    }

    public void InitGraphResults(GCGraph[] graphs, UnityAction<int> graphSolutionButtonHandler)
    {
        InitGraphResults(System.Array.AsReadOnly<GCGraph>(graphs), graphSolutionButtonHandler);
    }

    public void UpdateGraphResult(int graphIndex, bool success, UIGCPlayer answeringPlayer)
    {
        int markerIndex = playerUIs.IndexOf(answeringPlayer);
        gcResultEntries[graphIndex].UpdateMarker(markerIndex, graphIndex, success);
    }

    public void ShowSolutions()
    {
        graphTableSolutionText.gameObject.SetActive(true);
        for (int i = 0; i < gcResultEntries.Length; i++)
            gcResultEntries[i].ShowSolution();
    }

    public void HideSolutions()
    {
        graphTableSolutionText.gameObject.SetActive(false);
        for (int i = 0; i < gcResultEntries.Length; i++)
            gcResultEntries[i].HideSolution();
    }
}
