﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections.Generic;
using Utils;
using System.Collections;
using UnityEngine.EventSystems;

public class UIMPGraphColoringCoop : UIGraphColoring
{
    [SerializeField] private Text localPlayerName;
    [SerializeField] private Image localPlayerBackground;

    //Remote Player Area
    [SerializeField] private Text remotePlayerName;
    [SerializeField] private Image remotePlayerBackground;
    [SerializeField] private Button remoteEraseButton;
    [SerializeField] private GameObject remoteColorButtonHolder;
    private Dictionary<Color, Button> remoteColorButtons = new Dictionary<Color, Button>();

    public void SetupLocalPlayerVisuals(MPGamePlayer localPlayer)
    {
        localPlayerName.text = localPlayer.GetPlayerName();
        localPlayerBackground.color = localPlayer.GetPlayerColor();
    }

    public void SetupRemotePlayerVisuals(MPGamePlayer remotePlayer)
    {
        remotePlayerName.text = remotePlayer.GetPlayerName();
        remotePlayerBackground.color = remotePlayer.GetPlayerColor();
        remoteEraseButton.interactable = false;
    }

    public override void SetGraph(GCGraph graph)
    {
        base.SetGraph(graph);

        //clear color buttons. they will be added player specificly
        colorButtons.Clear();
        foreach (Transform child in colorButtonHolder.transform)
        {
            if (child.gameObject.activeSelf)
                Destroy(child.gameObject);
        }
        colorButtons.Add(graph.GetDefaultNodeColor(), eraseButton);

        remoteColorButtons.Clear();
        foreach (Transform child in remoteColorButtonHolder.transform)
        {
            if (child.gameObject.activeSelf)
                Destroy(child.gameObject);
        }
        remoteColorButtons.Add(graph.GetDefaultNodeColor(), remoteEraseButton);
    }

    public void SetLocalColorButtons(Color[] localColors)
    {
        SetColorButtons(colorButtonHolder, colorButtons, localColors);
    }

    public void SetRemoteColorButtons(Color[] remoteColors)
    {
        SetColorButtons(remoteColorButtonHolder, remoteColorButtons, remoteColors);
        foreach (Button b in remoteColorButtons.Values)
            b.interactable = false;
    }

    private void SetColorButtons(GameObject buttonHolder, Dictionary<Color,Button> buttonList, Color[] colors)
    {
        foreach (Color color in colors)
        {
            Button colorButton = CreateColorButton(color);
            colorButton.transform.SetParent(buttonHolder.transform, false);
            buttonList.Add(color, colorButton);
        }
    }

    public void SetRemoteColorButtonActive(Color color)
    {
        if (!remoteColorButtons.ContainsKey(color))
            return;

        SetColorButtonActive(remoteColorButtons[color], remoteColorButtons);
    }

    public void RemovePauseButton()
    {
        gameHeaderWithSliderUI.RemovePauseButton();
    }
}
