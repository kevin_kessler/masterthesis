﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Utils;
using System.Linq;

public class UISimpleToggleListGroup : UISimpleListGroup
{
    private ToggleGroup listGroup;
    private List<Toggle> toggles = new List<Toggle>();

    private event UnityAction<int> onToggleSelectHandler;

    protected override void Awake()
    {
        base.Awake();
        InitListGroup();
    }

    private void InitListGroup()
    {
        listGroup = itemHolder.GetComponent<ToggleGroup>();
        DebugUtils.Assert(null != listGroup, DebugUtils.GenerateComponentMissingMsg(() => itemHolder, typeof(ToggleGroup)));
        listGroup.allowSwitchOff = false;
    }

    public Toggle CreateAndAddToggle(string toggleName, bool isInteractable = true, Sprite toggleImage = null)
    {
        //instantiate toggle
        UIToggleListItem listItem = UIElement.Instantiate<UIToggleListItem>();
        AddItem(listItem.gameObject);

        listItem.SetText(toggleName);
        listItem.SetInteractable(isInteractable);
        listItem.SetItemImage(toggleImage);

        //add select handler
        listItem.AddOnValueChangedHandler((value) =>
        {
            if (value)
                HandleOnToggleSelect();
        });

        //assign toggle group
        if (null == listGroup)
        {
            InitListGroup();
        }
        listItem.SetGroup(listGroup);
        listGroup.RegisterToggle(listItem.GetToggle());

        toggles.Add(listItem.GetToggle());
        return listItem.GetToggle();
    }

    public Toggle GetSelectedToggle()
    {
        return listGroup.ActiveToggles().FirstOrDefault();
    }

    public int GetSelectedIndex()
    {
        Toggle selectedToggle = GetSelectedToggle();
        return toggles.IndexOf(selectedToggle);
    }

    public override void Clear()
    {
        base.Clear();
        toggles.Clear();
    }

    public int GetNumOfToggles()
    {
        return toggles.Count;
    }

    private void HandleOnToggleSelect()
    {
        if (null != onToggleSelectHandler)
        {
            int selectedIndex = GetSelectedIndex();
            onToggleSelectHandler.Invoke(selectedIndex);
        }
    }

    //the onToggleSelectCallback will have the index of the toggle item that was selected when the event is fired
    public void AddOnToggleSelectAction(UnityAction<int> onToggleSelectCallback)
    {
        onToggleSelectHandler += onToggleSelectCallback;
    }

    public void ClearOnToggleSelectActions()
    {
        onToggleSelectHandler = null;
    }
}
