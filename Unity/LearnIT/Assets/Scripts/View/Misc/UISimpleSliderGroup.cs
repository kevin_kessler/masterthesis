﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections.Generic;
using Utils;

public class UISimpleSliderGroup : UIElement
{
    [SerializeField] private Slider slider;
    [SerializeField] private Text valueText;
    [SerializeField] private Text descriptionText;

    private UnityAction<float> onBeginDrag;
    private UnityAction<float> onEndDrag;

    private bool isBeingDragged = false;
    public bool IsBeingDragged()
    {
        return isBeingDragged;
    }

    public Slider GetSlider()
    {
        return slider;
    }

    public void SetDescriptionText(string text)
    {
        descriptionText.text = text;
    }

    void Awake()
    {
        Init();
        Refresh();
    }

    private void Init()
    {
        slider.onValueChanged.AddListener((value) => { Refresh(); });
    }

    public void SetOnBeginDragHandler(UnityAction<float> handler)
    {
        if (null == handler)
            return;
        onBeginDrag = handler;
    }

    public void SetOnEndDragHandler(UnityAction<float> handler)
    {
        if (null == handler)
            return;
        onEndDrag = handler;
    }

    public void Refresh()
    {
        float curVal = slider.value;
        float maxVal = slider.maxValue;
        float ratio = (curVal / maxVal);

        valueText.text = string.Format("{0:0%}", ratio);
    }

    public void AddOnValueChangedListener(UnityAction<float> valueChangedListener)
    {
        if (null == valueChangedListener)
            return;
        slider.onValueChanged.AddListener(valueChangedListener);
    }

    public void OnEndDrag()
    {
        isBeingDragged = false;
        if (null == onEndDrag)
            return;

        onEndDrag.Invoke(slider.value);
    }

    public void OnBeginDrag()
    {
        isBeingDragged = true;
        if (null == onBeginDrag)
            return;

        onBeginDrag.Invoke(slider.value);
    }

    public void SetInteractable(bool interactable)
    {
        slider.interactable = interactable;
    }
}
