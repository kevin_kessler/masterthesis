﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections.Generic;
using Utils;
using System.Collections;

public class UIQuiz: UIElement
{
    //Header
    [SerializeField] private GameObject headerHolder;
    protected UIGameHeaderWithSlider gameHeaderWithSliderUI;

    //Progress Area
    [SerializeField] private GameObject progressHolder;
    private UIProgressMarker[] progressMarkers;
    
    //Question Area
    [SerializeField] private GameObject textQuestionHolder;
    [SerializeField] private Text textQuestionText;
    [SerializeField] private GameObject imageQuestionHolder;
    [SerializeField] private Text imageQuestionText;
    [SerializeField] private Image imageQuestionImage;

    //Answer Area
    [SerializeField] private Button answerButton00;
    [SerializeField] private Button answerButton01;
    [SerializeField] private Button answerButton02;
    [SerializeField] private Button answerButton03;

    [SerializeField] private Text answerButtonText00;
    [SerializeField] private Text answerButtonText01;
    [SerializeField] private Text answerButtonText02;
    [SerializeField] private Text answerButtonText03;

    void Awake()
    {
        gameHeaderWithSliderUI = UIElement.Instantiate<UIGameHeaderWithSlider>(headerHolder.transform);
        gameHeaderWithSliderUI.SetLevelText(Global.STRING_OVERVIEW_QUIZ_QUESTION);
    }

    private IEnumerator RecalculateAnswerButtons()
    {
        AutoGridLayout agl = answerButton00.transform.parent.GetComponent<AutoGridLayout>();
        RectTransform buttonRectTransform = answerButton00.gameObject.transform as RectTransform;
        do
        {
            yield return null;
            agl.CalculateLayoutInputHorizontal();
        } while (buttonRectTransform.rect.height < 0 || buttonRectTransform.rect.width < 0);
    }

    public override void Show()
    {
        base.Show();
        StartCoroutine(RecalculateAnswerButtons());
    }

    public virtual void Setup(QuizQuestion firstQuestion, int amountOfQuestions, UnityAction<int> onAnswerSelectAction)
    {
        progressMarkers = new UIProgressMarker[amountOfQuestions];
        for (int i = 0; i < amountOfQuestions; i++)
        {
            progressMarkers[i] = UIElement.Instantiate<UIProgressMarker>(progressHolder.transform);
            progressMarkers[i].SetNumber(i + 1);
        }

        SetQuestion(firstQuestion);
        SetProgressText(0, amountOfQuestions);

        if (null != onAnswerSelectAction)
        {
            answerButton00.onClick.AddListener(() => { onAnswerSelectAction.Invoke(0); });
            answerButton01.onClick.AddListener(() => { onAnswerSelectAction.Invoke(1); });
            answerButton02.onClick.AddListener(() => { onAnswerSelectAction.Invoke(2); });
            answerButton03.onClick.AddListener(() => { onAnswerSelectAction.Invoke(3); });
        }
    }

    public void UpdateProgressMarker(int questionIndex, bool correct)
    {
        Debug.Assert(questionIndex >= 0 && questionIndex <= progressMarkers.Length);

        if(correct)
            progressMarkers[questionIndex].SetCorrect();
        else
            progressMarkers[questionIndex].SetWrong();
    }

    public void UpdateProgressMarker(int questionIndex)
    {
        Debug.Assert(questionIndex >= 0 && questionIndex <= progressMarkers.Length);
        progressMarkers[questionIndex].SetNumber(questionIndex+1);
    }

    public void SetGameSlider(UICustomSlider slider)
    {
        gameHeaderWithSliderUI.SetSlider(slider);
    }

    public void AddPauseButtonAction(UnityAction pauseButtonAction)
    {
        gameHeaderWithSliderUI.AddOnPauseAction(pauseButtonAction);
    }

    public void SetPauseButtonImage(Sprite image)
    {
        gameHeaderWithSliderUI.SetPauseButtonImage(image);
    }

    public void SetQuestion(QuizQuestion question)
    {
        if (question.IsImageQuestion())
            SetImageQuestion(question);
        else
            SetTextQuestion(question);
    }

    public void SetProgressText(int currentQuestionIndex, int amountOfQuestions)
    {
        gameHeaderWithSliderUI.SetLevelCountText(string.Format("{0} / {1}", currentQuestionIndex + 1, amountOfQuestions));
    }

    private void SetTextQuestion(QuizQuestion question)
    {
        imageQuestionHolder.gameObject.SetActive(false);
        textQuestionHolder.gameObject.SetActive(true);
        textQuestionText.text = question.GetQuestionText();
        SetPossibleAnswers(question);
    }

    private void SetImageQuestion(QuizQuestion question)
    {
        //attempt to load image
        Sprite image = null;
        bool loadingFromBytes = null != question.GetQuestionImageBytes() && question.GetQuestionImageBytes().Length > 0;
        if (loadingFromBytes)
        {
            byte[] imageBytes = question.GetQuestionImageBytes();
            image = AssetUtils.LoadSpriteFromBytes(ref imageBytes);
        }
        else if (!string.IsNullOrEmpty(question.GetQuestionImageName()))
            image = QuizLoader.LoadQuizImageFromFile(question.GetQuestionImageName());

        //load textquestion if not able to load image
        if (null == image)
        {
            SetTextQuestion(question);
            return;
        }

        textQuestionHolder.gameObject.SetActive(false);
        imageQuestionHolder.gameObject.SetActive(true);
        imageQuestionText.text = question.GetQuestionText();
        imageQuestionImage.sprite = image;
        SetPossibleAnswers(question);
    }

    private void SetPossibleAnswers(QuizQuestion question)
    {
        answerButtonText00.text = question.GetPossibleAnswer(0);
        answerButtonText01.text = question.GetPossibleAnswer(1);
        answerButtonText02.text = question.GetPossibleAnswer(2);
        answerButtonText03.text = question.GetPossibleAnswer(3);
    }
   
    public void SetTitleText(string text)
    {
        gameHeaderWithSliderUI.SetTitleText(text);
    }

    public IEnumerator HighlightAnswerButton(QuizQuestion answeredQuestion)
    {
        //find the button which represents the given answer
        Image buttonBackground = null;
        switch (answeredQuestion.GetGivenAnswerIndex())
        {
            case 0:
                buttonBackground = answerButton00.GetComponent<Image>();
                break;
            case 1:
                buttonBackground = answerButton01.GetComponent<Image>();
                break;
            case 2:
                buttonBackground = answerButton02.GetComponent<Image>();
                break;
            case 3:
                buttonBackground = answerButton03.GetComponent<Image>();
                break;
            default:
                yield break;
        }

        bool correct = answeredQuestion.GetAnswerState() == QuizQuestion.AnswerState.CORRECT_ANSWERED;

        //determine colors to interpolate between
        Color originalButtonColor = buttonBackground.color;
        Color highlightColor = correct ? Global.COLOR_FLASH_GREEN : Global.COLOR_FLASH_RED;

        //set the animation / lerp time
        float lerpTime = 0.25f;
        float remainingLerpTime = 0.0f;
        float stayDuration = 0.25f;

        //highlight
        while(remainingLerpTime < lerpTime){
            yield return null;
            remainingLerpTime += Time.deltaTime;
            buttonBackground.color = Color.Lerp(originalButtonColor, highlightColor, remainingLerpTime / lerpTime);
        }

        //wait while highlighted
        yield return new WaitForSeconds(stayDuration);
        remainingLerpTime = lerpTime;

        //back to original color
        while (remainingLerpTime > 0)
        {
            yield return null;
            remainingLerpTime -= Time.deltaTime;
            buttonBackground.color = Color.Lerp(originalButtonColor, highlightColor, remainingLerpTime / lerpTime);
        }

        //wait again to slow down game pace a bit
        yield return new WaitForSeconds(stayDuration);
    }
}
