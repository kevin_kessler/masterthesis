﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine.Events;
public class UIQuizOverview : UIGameOverview {

    [SerializeField] private Button solutionButton;
    [SerializeField] private Text solutionButtonText;
    [SerializeField] private Text colTextCorrect;
    [SerializeField] private Text questionTableQuestionText;
    [SerializeField] private Text questionTableSolutionText;
    [SerializeField] private GameObject questionTablePlayerNameList;
    [SerializeField] private GameObject questionTablePlayerNamePrefab;
    [SerializeField] private GameObject questionTableResultList;
    private UIQuizResultEntry[] quizResultEntries;

    protected override void Awake()
    {
        base.Awake();

        colTextCorrect.text = Global.STRING_OVERVIEW_CORRECT;
        questionTableQuestionText.text = Global.STRING_OVERVIEW_QUESTION;
        backToLobbyIndicationText.text = Global.STRING_OVERVIEW_WAITING_FOR_PLAYER;
        questionTableSolutionText.text = Global.STRING_SOLUTION;
        solutionButtonText.text = Global.STRING_SHOW_SOLUTION;

        solutionButton.onClick.RemoveAllListeners();
        solutionButton.onClick.AddListener(ToggleSolutions);
    }

    public override void AddPlayerUI(UIMPGamePlayer playerUI)
    {
        base.AddPlayerUI(playerUI);

        GameObject playerNameHolder = Instantiate(questionTablePlayerNamePrefab);
        playerNameHolder.transform.SetParent(questionTablePlayerNameList.transform, false);
        playerNameHolder.SetActive(true);
        Text playerName = playerNameHolder.GetComponent<Text>();
        playerName.text = playerUI.GetPlayerName();
    }

    public void InitQuestionResults(ReadOnlyCollection<QuizQuestion> questions)
    {
        quizResultEntries = new UIQuizResultEntry[questions.Count];
        for (int i = 0; i < questions.Count; i++)
        {
            UIQuizResultEntry result = UIElement.Instantiate<UIQuizResultEntry>(questionTableResultList.transform);
            result.SetQuestion(i, questions[i].GetQuestionText(), questions[i].GetCorrectAnswer(), questions[i].GetAnswerState(), playerUIs.Count);
            quizResultEntries[i] = result;
        }
    }

    public void InitQuestionResults(QuizQuestion[] questions)
    {
        InitQuestionResults(System.Array.AsReadOnly<QuizQuestion>(questions));
    }

    public void UpdateQuestionResult(int questionIndex, QuizQuestion.AnswerState answer, UIQuizPlayer answeringPlayer)
    {
        int markerIndex = playerUIs.IndexOf(answeringPlayer);

        quizResultEntries[questionIndex].UpdateMarker(markerIndex, questionIndex, answer);
        if (answer == QuizQuestion.AnswerState.CORRECT_ANSWERED)
            answeringPlayer.IncreaseNumOfCorrectAnswers();
    }

    public void UpdateQuestionResult(int questionIndex, QuizQuestion.AnswerState answer)
    {
        quizResultEntries[questionIndex].UpdateMarker(questionIndex, answer);

        if (answer == QuizQuestion.AnswerState.CORRECT_ANSWERED)
            for (int i = 0; i < playerUIs.Count; i++)
            {
                ((UIQuizPlayer)playerUIs[i]).IncreaseNumOfCorrectAnswers();
            }
    }

    public void ShowSolutions()
    {
        solutionButtonText.text = Global.STRING_HIDE_SOLUTION;
        questionTableSolutionText.gameObject.SetActive(true);
        for (int i = 0; i < quizResultEntries.Length; i++)
            quizResultEntries[i].ShowSolution();
    }

    public void HideSolutions()
    {
        solutionButtonText.text = Global.STRING_SHOW_SOLUTION;
        questionTableSolutionText.gameObject.SetActive(false);
        for (int i = 0; i < quizResultEntries.Length; i++)
            quizResultEntries[i].HideSolution();
    }

    private void ToggleSolutions()
    {
        if (questionTableSolutionText.IsActive())
            HideSolutions();
        else
            ShowSolutions();
    }

    public override void Show()
    {
        base.Show();
        solutionButton.gameObject.SetActive(true);
    }

    public override void ShowWithoutFooter()
    {
        base.ShowWithoutFooter();
        solutionButton.gameObject.SetActive(false);
    }
}
