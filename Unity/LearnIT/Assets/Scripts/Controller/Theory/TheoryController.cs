﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;

public class TheoryController : SPGameController
{
    private TheoryPage[] pages;
    private int currentBrowseIndex;
    private int browseSize;

    private UITheory theoryUI;
    private UICountSlider progressSlider;

    protected override void Init()
    {
        //init UI and assign slider
        Game currentGame = GameSelectionController.GetLastChosenGame();
        theoryUI = UIElement.Instantiate<UITheory>(this.transform);
        theoryUI.SetTitleText(string.Format(Global.STRING_THEORY_TITLE, currentGame.GetName()));
        theoryUI.SetOnLeftButtonClickListener(BrowseBack);
        theoryUI.SetOnRightButtonClickListener(BrowseForth);
        theoryUI.SetOnBackButtonClickListener(OnQuitGame);

        progressSlider = UIElement.Instantiate<UICountSlider>();
        progressSlider.SetIsCountingDown(false);
        progressSlider.SetPrependingText(Global.STRING_THEORY_PAGE+" ");
        
        theoryUI.SetProgressSlider(progressSlider);
        theoryUI.SetOnPauseButtonClickListener(PauseGame);
    }

    protected override IEnumerator LoadGame()
    {
        //load theory of current chosen game
        GameMode lastChosenMode = GameMenuController.GetLastChosenMode();
        GameModeTheory theoryMode = lastChosenMode is GameModeTheory ? (GameModeTheory)lastChosenMode : null;
        TheoryBook book = null;
        if (null != theoryMode)
            book = theoryMode.GetTheoryBook();
        if (null != book)
            pages = book.GetPages();
        yield break;
    }

    protected override void OnGameLoaded()
    {
        if (null == pages || pages.Length < 1)
        {
            Debug.LogWarning("[TheoryController]: Could not load theory data. Returning to previous scene.");
            FinishGame("Keine Daten gefunden. Ist die Theorie XML korrupt?");
            return;
        }

        //per browseindex we display two book pages
        browseSize = Mathf.CeilToInt((pages.Length / (float)2));
        progressSlider.Reset(browseSize);
        progressSlider.SetAppendingText(" / " + browseSize);

        //set first page
        currentBrowseIndex = 0;
        SetCurrentPages(currentBrowseIndex);
    }

    void Update()
    {
        KeyCode pressedKey = Utils.InputUtils.GetPressedKey();
        if (pressedKey == KeyCode.LeftArrow)
            BrowseBack();
        else if (pressedKey == KeyCode.RightArrow)
            BrowseForth();
    }

    private void BrowseForth()
    {
        if (currentBrowseIndex >= browseSize - 1)
            return;

        currentBrowseIndex++;
        SetCurrentPages(currentBrowseIndex);
    }

    private void BrowseBack()
    {
        if (currentBrowseIndex <= 0)
            return;

        currentBrowseIndex--;
        SetCurrentPages(currentBrowseIndex);
    }

    private void SetCurrentPages(int browseIndex)
    {
        Debug.Assert(browseIndex >= 0 && browseIndex < browseSize);
        int leftPageIndex = browseIndex * 2;
        int rightPageIndex = leftPageIndex + 1;

        TheoryPage leftPage = pages[leftPageIndex];
        TheoryPage rightPage = pages.Length > rightPageIndex ? pages[rightPageIndex] : null;

        //update ui
        progressSlider.SetCurrentValue(browseIndex + 1);
        theoryUI.SetLeftButtonInteractable(browseIndex > 0);
        theoryUI.SetRightButtonInteractable(browseIndex < browseSize - 1);
        theoryUI.SetPages(leftPage, rightPage);
    }

    ///////////////////////// INTERFACE IPAUSEHANDLER IMPLEMENTATION BEGIN //////////////////////////////////
    public override void OnRestartGame()
    {
        currentBrowseIndex = 0;
        SetCurrentPages(currentBrowseIndex);
        OnResumeGame();
    }
    ///////////////////////// INTERFACE IPAUSEHANDLER IMPLEMENTATION END //////////////////////////////////

    protected override void OnGameFinished()
    {
    }
}
