﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Utils;
using GuessingHint = ITNumberGuessingRound.GuessingHint;

public class ITNumberSurvivalController : ITNumberController
{

    private const int SURVIVAL_DURATION = 25; //in seconds
    private const int SURVIVAL_BONUS_TIME = 2; //in seconds, will be multiplied by level progress


    private UITimeSlider timeSlider;

    private int roundCounter = 1;

    protected override UICustomSlider InitGameSlider()
    {
        timeSlider = UIElement.Instantiate<UITimeSlider>();
        timeSlider.Reset(SURVIVAL_DURATION);
        timeSlider.Pause();
        return timeSlider;
    }

    protected override void OnGameLoaded()
    {
        base.OnGameLoaded();
        timeSlider.Continue();
    }

    protected override string GetGameTitle()
    {
        return Global.STRING_IT_NUMBER_SURVIVAL_NAME;
    }

    protected override System.Collections.IEnumerator LoadGame()
    {
        yield break;
    }

    protected override ITNumberGuessingRound GetFirstRound()
    {
        return ITNumberGuessingRound.GenerateRoundByLevelProgress(1);
    }

    protected override ITNumberGuessingRound GetNextRound()
    {
        roundCounter++;
        return ITNumberGuessingRound.GenerateRoundByLevelProgress(roundCounter);
    }

    protected override void UpdateRoundSpecificUI(ITNumberGuessingRound round)
    {
        numberGuessingUI.SetRoundInfo(currentRound, roundCounter.ToString());
    }

    protected override void OnCorrectGuess()
    {
        int correctNumber = currentRound.GetNumberToGuess();

        NextRound();
        float bonusTime = roundCounter * SURVIVAL_BONUS_TIME;
        float currentTime = timeSlider.GetCurrentValue();
        if (bonusTime + currentTime > SURVIVAL_DURATION)
            bonusTime = SURVIVAL_DURATION - currentTime;

        timeSlider.AddTime(bonusTime);
        ShowNextRoundFeedback(string.Format(
            "\"{0}\"" + System.Environment.NewLine
            + "Level {1}" + System.Environment.NewLine
            + "+{2}s", correctNumber, roundCounter, Mathf.RoundToInt(bonusTime)));
    }

    protected override void OnTooHighGuess()
    {
        ShowWrongNumberFeedback();
    }

    protected override void OnTooLowGuess()
    {
        ShowWrongNumberFeedback();
    }

    public override void OnSliderFinished()
    {
        //TODO show blink animation before ends -> maybe in slider script itself
        ShowTimeIsUpFeedback(); 
        ShowTimeIsUpDialog();
        audioPlayer.StopBgMusic();
    }

    public void ShowTimeIsUpFeedback()
    {
        //TODO: play alarm / troete
        //TODO: show red flash

        //for now...
        ShowGameOverFeedback();
    }

    UIInfoDialog timeIsUpDialog;
    private void ShowTimeIsUpDialog()
    {
        if (null == timeIsUpDialog)
        {
            timeIsUpDialog = UIElement.Instantiate<UIInfoDialog>();
            timeIsUpDialog.Setup(
                Global.STRING_DIALOG_TIME_UP_TITLE,
                string.Format(Global.STRING_IT_DIALOG_NUMBER_SURVIVAL_TIME_UP_DESCRIPTION, roundCounter-1),
                Resources.Load<Sprite>(Global.PATH_ICON_WIN), //TODO: icon depending on number of rounds / beating highscore?
                FinishGame
            );
        }

        timeIsUpDialog.Show();
    }

    public override void OnRestartGame()
    {
        timeSlider.Reset(SURVIVAL_DURATION);
        roundCounter = 1;
        SetCurrentRound(GetFirstRound());
        base.OnRestartGame();
    }

    private UIInfoDialog helpDialog;
    protected override UIDialog GetHelpDialog()
    {
        if (null == helpDialog)
        {
            helpDialog = UIElement.Instantiate<UIInfoDialog>(transform);
            helpDialog.Setup(
                Global.STRING_HELP,
                Global.STRING_IT_NUMBER_SURVIVAL_HELP_TEXT,
                Resources.Load<Sprite>(Global.PATH_ICON_QUESTIONMARK)
            );
        }
        return helpDialog;
    }

    public override void OnQuitGame()
    {
        timeSlider.Pause();
        base.OnQuitGame();
    }
}
