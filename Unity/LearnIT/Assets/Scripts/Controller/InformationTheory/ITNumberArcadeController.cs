﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Utils;
using GuessingHint = ITNumberGuessingRound.GuessingHint;

public class ITNumberArcadeController : ITNumberController {

    private UICountSlider countSlider;

    private ReadOnlyCollection<ITNumberGuessingRound> gameRounds;

    protected override UICustomSlider InitGameSlider()
    {
        countSlider = UIElement.Instantiate<UICountSlider>();
        return countSlider;
    }

    protected override string GetGameTitle()
    {
        return Global.STRING_IT_NUMBER_ARCADE_NAME;
    }

    protected override System.Collections.IEnumerator LoadGame()
    {
        gameRounds = ITNumberGuessingRound.GUESSING_ROUNDS;
        yield break;
    }

    protected override ITNumberGuessingRound GetFirstRound()
    {
        if (gameRounds.Count <= 0)
            return null;

        return gameRounds[0];
    }

    protected override ITNumberGuessingRound GetNextRound()
    {
        if (!NextRoundIsAvailable())
            return null;

        int indexOfNextRound = gameRounds.IndexOf(currentRound) + 1;
        return gameRounds[indexOfNextRound];
    }

    /**
     * Returns whether there is a next round to advance to.
     */
    private bool NextRoundIsAvailable()
    {
        int indexOfCurRound = gameRounds.IndexOf(currentRound);
        if (indexOfCurRound == -1)
            return false;

        int indexOfNextRound = indexOfCurRound + 1;
        return indexOfNextRound < gameRounds.Count;
    }

    protected override void UpdateRoundSpecificUI(ITNumberGuessingRound round)
    {
        //adjust number of allowed guesses
        int numOfAllowedGuesses = currentRound.GetMaxAllowedGuesses();
        countSlider.SetAppendingText(" / " + numOfAllowedGuesses);
        countSlider.Reset(numOfAllowedGuesses);

        //set round information in the ui //TODO:
        numberGuessingUI.SetRoundInfo(currentRound, (gameRounds.IndexOf(currentRound) + 1) + " / " + gameRounds.Count);
    }

    protected override void OnCorrectGuess()
    {
        if (NextRoundIsAvailable()) {
            int indexOfNextRound = gameRounds.IndexOf(currentRound) + 1;
            ShowNextRoundFeedback("\""+currentRound.GetNumberToGuess()+"\"" + System.Environment.NewLine + "Level " + (indexOfNextRound+1));
            NextRound();
        }
        else {
            ShowGameWonFeedback();
            ShowGameWonDialog();
            //TODO: game won logic
        }
    }

    protected override void OnTooHighGuess()
    {
        ShowWrongNumberFeedback();
        countSlider.Decrement();
    }

    protected override void OnTooLowGuess()
    {
        ShowWrongNumberFeedback();
        countSlider.Decrement();
    }

    public override void OnSliderFinished()
    {
        ShowGameOverFeedback();
        ShowGameOverDialog();
        audioPlayer.StopBgMusic();
    }

    UIInfoDialog gameWonDialog;
    private void ShowGameWonDialog()
    {
        if (null == gameWonDialog)
        {
            gameWonDialog = UIElement.Instantiate<UIInfoDialog>();
            gameWonDialog.Setup(
                Global.STRING_DIALOG_WIN_TITLE,
                Global.STRING_IT_DIALOG_NUMBER_ARCADE_WIN_DESCRIPTION,
                Resources.Load<Sprite>(Global.PATH_ICON_WIN),
                FinishGame
                );
        }

        gameWonDialog.Show();
    }

    UIInfoDialog gameOverDialog;
    private void ShowGameOverDialog()
    {
        if (null == gameOverDialog)
        {
            gameOverDialog = UIElement.Instantiate<UIInfoDialog>();
            gameOverDialog.Setup(
                Global.STRING_DIALOG_LOSE_TITLE,
                Global.STRING_IT_DIALOG_NUMBER_ARCADE_LOSE_DESCRIPTION,
                Resources.Load<Sprite>(Global.PATH_ICON_LOSE),
                FinishGame
                );
        }

        gameOverDialog.Show();
    }

    public override void OnRestartGame()
    {
        SetCurrentRound(currentRound);
        base.OnRestartGame();
    }

    private UIInfoDialog helpDialog;
    protected override UIDialog GetHelpDialog()
    {
        if (null == helpDialog)
        {
            helpDialog = UIElement.Instantiate<UIInfoDialog>(transform);
            helpDialog.Setup(
                Global.STRING_HELP,
                Global.STRING_IT_NUMBER_ARCADE_HELP_TEXT,
                Resources.Load<Sprite>(Global.PATH_ICON_QUESTIONMARK)
            );
        }
        return helpDialog;
    }
}
