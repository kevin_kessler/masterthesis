﻿using UnityEngine;
using System.Collections;
using System;

public class GameManager : Singleton<GameManager>
{
    private NetworkController networkController;
    private SettingsController settingsController;

    void Awake()
    {
        //ensure logs are being sent to log central
        //Application.logMessageReceived +=  (HandleLog);

        settingsController = gameObject.AddComponent<SettingsController>();

        GameObject networkControllerObject = new GameObject("NetworkController");
        networkControllerObject.transform.SetParent(gameObject.transform);
        networkController = networkControllerObject.AddComponent<NetworkController>();
        networkController.Initialize(settingsController.GetPlayerInfo());
    }

    public LocalClientInfo GetPlayerInfo()
    {
        return settingsController.GetPlayerInfo();
    }

    public NetworkController GetNetworkController()
    {
        return networkController;
    }

    public SettingsController GetSettingsController()
    {
        return settingsController;
    }

    //Capture debug.log output
    public void HandleLog(string logString, string stackTrace, LogType type)
    {
        StartCoroutine(SendLogToLoggly(logString, stackTrace, type));
    }

    //send logs to Loggly
    public IEnumerator SendLogToLoggly(string logString, string stackTrace, LogType type)
    {
        //Initialize WWWForm and store log level as a string
        string level = type.ToString();
        WWWForm loggingForm = new WWWForm();

        //Add log message to WWWForm
        loggingForm.AddField("LEVEL", level);
        loggingForm.AddField("Message", logString);
        loggingForm.AddField("Stack_Trace", stackTrace);
        loggingForm.AddField("Device_Model", SystemInfo.deviceModel);
        loggingForm.AddField("Date", (new DateTime()).ToString("yyyy-mm-dd HH:mm:ss"));
        
        //Send WWW Form to Loggly
        WWW sendLog = new WWW("http://logs-01.loggly.com/inputs/2d697600-ca0d-447f-b1b9-94ba4d0135a9/tag/http/", loggingForm);

        yield return sendLog;

        if (null != sendLog.error)
        {
            //TODO: maybe log "Could not send request" -> sendLog.error.ToString();
        }
    }
}
