﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using Utils;


public class GCLoader : MonoBehaviour
{
    private const int MIN_IMAGE_SIZE = 512;
    private const int MAX_IMAGE_SIZE = 512;
    private const int MIN_NODE_SIZE = 20;
    private static readonly string LOG_PREFIX = "[" + typeof(GCLoader).Name + "]: ";
    private static readonly string GC_IMAGE_DIRECTORY = "Complexity" + Path.AltDirectorySeparatorChar;
    private static readonly string GC_SOLUTION_DIRECTORY = GC_IMAGE_DIRECTORY + "Solutions" + Path.AltDirectorySeparatorChar;

    private static Dictionary<string, GCGraph> graphByName = new Dictionary<string, GCGraph>();

    private enum Direction
    {
        NONE = -1,
        N = 0,
        E = 1,
        S = 2,
        W = 3,
        NE = 4,
        SE = 5,
        SW = 6,
        NW = 7
    }

    #region LOADING_PROGRESS
    private GCGraph currentLoadingGraph = null;
    private int currentPixelsChecked = 0;
    private int currentNodesChecked = 0;
    private float startTime;
    private float endTime;

    public bool GraphIsLoaded(string name)
    {
        return graphByName.ContainsKey(name);
    }

    public bool IsLoading()
    {
        return null != currentLoadingGraph;
    }

    public float GetLoadingProgress()
    {
        if (!IsLoading())
            return 1;

        float findNodesProgress = GetFindNodesProgress();
        if (findNodesProgress < 1)
            return findNodesProgress * 0.5f;

        float findNeighborsProgress = GetFindNeighborsProgress();
        return (findNodesProgress + findNeighborsProgress) * 0.5f;
    }

    private float GetFindNodesProgress()
    {
        Texture2D graphTexture = currentLoadingGraph.GetImage().texture;
        int pixelsToCheck = graphTexture.width * graphTexture.height;
        return currentPixelsChecked / (float)pixelsToCheck;
    }

    private float GetFindNeighborsProgress()
    {
        int nodesToCheck = currentLoadingGraph.GetNodes().Count;
        return currentNodesChecked / (float)nodesToCheck;
    }
    #endregion

    public GCGraph GetGraphByName(string name)
    {
        if (!graphByName.ContainsKey(name))
            return null;

        return graphByName[name];
    }

    public IEnumerator LoadGraph(GCGraphData graphData)
    {
        if (IsLoading())
            yield break;

        startTime = Time.realtimeSinceStartup;

        //get the required graph image from resources
        Sprite sprite = LoadGraphImage(graphData.GetGraphImageName());
        if (null == sprite)
            yield break;

        //prepare graph object to be populated
        GCGraph graph = new GCGraph(sprite, graphData);
        Color nodeColor = graphData.GetDefaultNodeColor();
        currentLoadingGraph = graph;

        //prepare bool array to remember which pixels have been visited already to improve performance
        Texture2D texture = sprite.texture;
        bool[][] visited = new bool[texture.width][];
        for (int i = 0; i < visited.Length; i++)
            visited[i] = new bool[texture.height];

        //find nodes
        Debug.Log(LOG_PREFIX + "Looking for nodes in graph image for graph " + graph.GetName() + " ...");
        for (int y = 0; y < texture.height; y++)
        {
            currentPixelsChecked++;

            for (int x = 0; x < texture.width; x++)
            {
                currentPixelsChecked++;
                if (visited[x][y])
                    continue;

                Color pixelColor = texture.GetPixel(x, y);

                //only default color areas are interpreted as colorizable nodes
                if (!AssetUtils.ColorsAreSimilar(pixelColor, nodeColor))
                    continue;

                //default color pixel was found. create node and flood fill the node from this point up to its borders.
                GCNode node = new GCNode(graph);
                //node.SetColor(pixelColor);
                node.SetColor(nodeColor);
                FindNodes(x, y, texture, node, ref visited);

                //remove node if too small to see / click
                if (node.GetPixels().Count < MIN_NODE_SIZE)
                    graph.GetNodes().Remove(node);

                Debug.Log(LOG_PREFIX + "Found Node " + node + " and added to Graph");
                yield return null;
            }
        }

        Debug.Log(LOG_PREFIX + "Finished looking for nodes. Found " + graph.GetNodes().Count + " nodes.");
        Debug.Log(LOG_PREFIX + "Looking for neighbors of each graph node ...");

        //find neighbors of each discovered node
        foreach (GCNode node in graph.GetNodes())
        {
            FindNeighborsByFlooding(node, graph, texture);
            yield return null;

            currentNodesChecked++;
            Debug.Log(LOG_PREFIX + "Found " + node.GetNeighbors().Count + " neighbors for node " + currentNodesChecked);
        }

        //insert loaded graph into dictionary to offer it for future load requests
        if (graphByName.ContainsKey(graph.GetName()))
            graphByName[graph.GetName()] = currentLoadingGraph;
        else
            graphByName.Add(graph.GetName(), currentLoadingGraph);

        //finish loading
        endTime = Time.realtimeSinceStartup;
        Debug.Log(LOG_PREFIX + "Finished loading graph " + graph.GetName() + " in " + (endTime - startTime) + " seconds");
        startTime = 0;
        endTime = 0;
        currentPixelsChecked = 0;
        currentNodesChecked = 0;
        currentLoadingGraph = null;
    }

    //finds the nodes of the given source texture.
    //based on flood-fill algorithm
    private void FindNodes(int xStart, int yStart, Texture2D source, GCNode fillingNode, ref bool[][] visited)
    {
        Queue<GCNode.Point> pixelQueue = new Queue<GCNode.Point>();
        GCNode.Point startPixel = new GCNode.Point(xStart, yStart);
        pixelQueue.Enqueue(startPixel);

        //flood in 4 directions from start point to borders and add the pixels
        while (pixelQueue.Count > 0)
        {
            GCNode.Point curPixel = pixelQueue.Dequeue();

            //reached image border?
            if (curPixel.x >= source.width || curPixel.x < 0 || curPixel.y >= source.height || curPixel.y < 0)
                continue;

            //check for border. if found, add to border pixels of the node. and continue with next pixel in queue.
            Color curColor = source.GetPixel(curPixel.x, curPixel.y);
            if (!AssetUtils.ColorsAreSimilar(curColor, fillingNode.GetColor()))
            {
                fillingNode.AddBorderPixel(curPixel);
                continue;
            }

            //already visited? check happens after border check, because nodes can share borders
            if (visited[curPixel.x][curPixel.y])
                continue;
            visited[curPixel.x][curPixel.y] = true;

            //add pixel and expand to north, south, east, west
            fillingNode.AddPixel(curPixel);
            pixelQueue.Enqueue(new GCNode.Point(curPixel.x, curPixel.y + 1));
            pixelQueue.Enqueue(new GCNode.Point(curPixel.x, curPixel.y - 1));
            pixelQueue.Enqueue(new GCNode.Point(curPixel.x + 1, curPixel.y));
            pixelQueue.Enqueue(new GCNode.Point(curPixel.x - 1, curPixel.y));
        }
    }

    //Finds the direct neighbors of the given node.
    //Floods the image from sourceNodePixels as origin, wanders in all directions towards its border.
    //When crossing the borders, it asks the graph to give the nodes to which the pixels across the border belong to.
    //the graph returns the node, which then is added as neighbor.
    private void FindNeighborsByFlooding(GCNode sourceNode, GCGraph parentGraph, Texture2D source)
    {
        bool[][] visited = new bool[source.width][];
        for (int i = 0; i < visited.Length; i++)
            visited[i] = new bool[source.height];

        List<GCNode.Point> nodePixels = sourceNode.GetPixels();
        for (int i = 0; i < nodePixels.Count; i++)
        {
            if (visited[nodePixels[i].x][nodePixels[i].y])
                continue;

            //spread to node borders in all directions and cross them to determine the neighbors
            SpreadToNodeBorders(nodePixels[i].x, nodePixels[i].y, source, sourceNode, ref visited, parentGraph);
        }
    }

    //spreads from the given start coordinates into all 8 directions in order to find the nodes borders.
    //when a border is found it will be crossed and the first not-border-pixel will be used to determine the neighbor node.
    private void SpreadToNodeBorders(int xStart, int yStart, Texture2D source, GCNode node, ref bool[][] visited, GCGraph parentGraph)
    {
        Queue<GCNode.Point> pixelQueue = new Queue<GCNode.Point>();
        Queue<Direction> directionQueue = new Queue<Direction>();

        GCNode.Point startPixel = new GCNode.Point(xStart, yStart);
        pixelQueue.Enqueue(startPixel);
        directionQueue.Enqueue(Direction.NONE);

        while (pixelQueue.Count > 0)
        {
            GCNode.Point curPixel = pixelQueue.Dequeue();

            //the heading direction is needed in order to know in which direction the border shall be crossed.
            Direction headingDirection = directionQueue.Dequeue();

            //reached image border?
            if (curPixel.x >= source.width || curPixel.x < 0 || curPixel.y >= source.height || curPixel.y < 0)
                continue;

            //reached border? begin to search in one direction only (the direction in which the border was found)
            Color curColor = source.GetPixel(curPixel.x, curPixel.y);
            if (!AssetUtils.ColorsAreSimilar(curColor, node.GetColor()))
            {
                FindNeighborInDirection(curPixel.x, curPixel.y, source, node, parentGraph, headingDirection);
                continue;
            }

            //already visited?
            if (visited[curPixel.x][curPixel.y])
                continue;
            visited[curPixel.x][curPixel.y] = true;

            //spread in each direction to reach the border and remember in which direction the border was found
            pixelQueue.Enqueue(new GCNode.Point(curPixel.x, curPixel.y + 1));
            directionQueue.Enqueue(Direction.N);

            pixelQueue.Enqueue(new GCNode.Point(curPixel.x, curPixel.y - 1));
            directionQueue.Enqueue(Direction.S);

            pixelQueue.Enqueue(new GCNode.Point(curPixel.x + 1, curPixel.y));
            directionQueue.Enqueue(Direction.E);

            pixelQueue.Enqueue(new GCNode.Point(curPixel.x - 1, curPixel.y));
            directionQueue.Enqueue(Direction.W);

            pixelQueue.Enqueue(new GCNode.Point(curPixel.x + 1, curPixel.y + 1));
            directionQueue.Enqueue(Direction.NE);

            pixelQueue.Enqueue(new GCNode.Point(curPixel.x - 1, curPixel.y + 1));
            directionQueue.Enqueue(Direction.NW);

            pixelQueue.Enqueue(new GCNode.Point(curPixel.x + 1, curPixel.y - 1));
            directionQueue.Enqueue(Direction.SE);

            pixelQueue.Enqueue(new GCNode.Point(curPixel.x - 1, curPixel.y - 1));
            directionQueue.Enqueue(Direction.SW);
        }
    }

    //walks along the border in the given direction until a not-border-pixel is found.
    //determines the node to which the not-border-pixel belongs and adds it as neighbor.
    //if no not-border-pixel is found within the graphs borderThickness-Range, no neighbor will be added.
    private void FindNeighborInDirection(int xStart, int yStart, Texture2D source, GCNode node, GCGraph parentGraph, Direction direction)
    {
        int x = xStart;
        int y = yStart;

        for (int i = 0; i <= parentGraph.GetBorderThickness(); i++)
        {
            //reached image border?
            if (x >= source.width || x < 0 || y >= source.height || y < 0)
                return;

            //reached a transparent pixel? (if nodes are separated by transparent pixels, they are not considered as neighbors)
            Color curColor = source.GetPixel(x, y);
            if (curColor.a < 0.25f)
                return;

            //reached a non-border-pixel? --> add as neighbor
            if (AssetUtils.ColorsAreSimilar(curColor, node.GetColor()))
            {
                GCNode neighbor = parentGraph.GetNodeByPosition(x, y);
                node.AddNeighbor(neighbor);
                return;
            }

            //continue heading in the given direction
            switch (direction)
            {
                case Direction.N: y++; break;
                case Direction.S: y--; break;
                case Direction.E: x++; break;
                case Direction.W: x--; break;
                case Direction.NE: x++; y++; break;
                case Direction.NW: x--; y++; break;
                case Direction.SE: x++; y--; break;
                case Direction.SW: x--; y--; break;
            }
        }
    }   

    public Sprite LoadGraphImage(string graphImageName)
    {
        Texture2D originalImage = Resources.Load<Texture2D>(GC_IMAGE_DIRECTORY + graphImageName);
        Texture2D copiedImage = AssetUtils.CopyTexture(originalImage);

        if (copiedImage.width > MAX_IMAGE_SIZE || copiedImage.height > MAX_IMAGE_SIZE)
            TextureScale.Bilinear(copiedImage, MAX_IMAGE_SIZE);
        else if (copiedImage.width < MIN_IMAGE_SIZE || copiedImage.height < MIN_IMAGE_SIZE)
            TextureScale.Bilinear(copiedImage, MIN_IMAGE_SIZE);

        return Sprite.Create(copiedImage, new Rect(0, 0, copiedImage.width, copiedImage.height), new Vector2(0.5f, 0.5f));
    }

    public Sprite LoadGraphSolution(string graphImageName)
    {
        return Resources.Load<Sprite>(GC_SOLUTION_DIRECTORY + graphImageName + "_sol");
    }


    /////////////////FAILED APPROACHES
    /*
    //not used because of too deep recursion when using big images. --> call stack overflows
    private void FloodFindNodeRecursive(int x, int y, Texture2D source, GCNode fillingNode, ref bool[][] visited)
    {
        //reached image border?
        if (x >= source.width || x < 0 || y >= source.height || y < 0)
            return;

        //already visited?
        if (visited[x][y])
            return;

        visited[x][y] = true;

        //different color?
        Color currentColor = source.GetPixel(x, y);
        if (!ColorsAreSimilar(currentColor, fillingNode.GetColor()))
            return;

        //pixelcolor matches --> add to group and spread in all directions
        fillingNode.AddPixel(x, y);
        FloodFindNodeRecursive(x, y + 1, source, fillingNode, ref visited);
        FloodFindNodeRecursive(x, y - 1, source, fillingNode, ref visited);
        FloodFindNodeRecursive(x + 1, y, source, fillingNode, ref visited);
        FloodFindNodeRecursive(x - 1, y, source, fillingNode, ref visited);
    }

    //NOT USING. TOO SLOW IF MANY NODES IN GRAPH
    //finds neighbors of the given node within the potentialNeighbors collection.
    //done by comparing the border pixels of the nodes.
    //a node is interpreted as neighbor if their borders are close enough to each other.
    private IEnumerator FindNeighborsByBorderDistance(GCNode sourceNode, IEnumerable<GCNode> potentialNeighbors, int maxBorderDistance)
    {
        List<GCNode.Point> borderPixels = sourceNode.GetBorder();
        foreach (GCNode potentialNeighbor in potentialNeighbors)
        {
            if (sourceNode.Equals(potentialNeighbor) || sourceNode.IsNeighborTo(potentialNeighbor))
                continue;

            bool added = false;
            List<GCNode.Point> potNeighborBorder = potentialNeighbor.GetBorder();
            foreach (GCNode.Point borderPixel in borderPixels)
            {
                foreach (GCNode.Point potNeigborPixel in potNeighborBorder)
                {
                    bool isVerticallyClose = Mathf.Abs(potNeigborPixel.y - borderPixel.y) < maxBorderDistance;
                    bool isHorizontallyClose = Mathf.Abs(potNeigborPixel.x - borderPixel.x) < maxBorderDistance;
                    if (isVerticallyClose && isHorizontallyClose)
                    {
                        sourceNode.AddNeighbor(potentialNeighbor);
                        added = true;
                        break;
                    }
                }

                if (added)
                    break;
            }

            yield return null;
        }
    }
     * */
}

