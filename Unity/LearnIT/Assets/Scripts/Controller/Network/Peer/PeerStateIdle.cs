﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using PeerMsgTypes;

public class PeerStateIdle : PeerState {

    public PeerStateIdle(Peer localPeer)
        :base(localPeer)
    {
        
    }

    public override void OnStateEnter()
    {
        base.OnStateEnter();

        //make sure we are not connected to any server in idle state
        localPeer.DisconnectFromServer();

        //make sure, there is no active lobby in idle state
        LobbyController[] lobbys = Object.FindObjectsOfType<LobbyController>();
        foreach(LobbyController lobby in lobbys)
            Object.Destroy(lobby.gameObject);
    }

    public override PeerState OnClientError(NetworkMessage netMsg)
    {
        base.OnClientError(netMsg);
        localPeer.DisconnectFromServer();
        return this;
    }

    public override PeerState OnClientJoinedServer(NetworkMessage netMsg)
    {
        base.OnClientJoinedServer(netMsg);
        return RequestMatchInvitation();
    }

    public override PeerState OnMatchInvitationRequest(NetworkMessage netMsg)
    {
        base.OnMatchInvitationRequest(netMsg);
        return new PeerStateInviteReceived(localPeer, netMsg);
    }

    private PeerState RequestMatchInvitation()
    {
        MatchInvitationRequest request = new MatchInvitationRequest();
        request.requestedGameModeID = localPeer.GetCurrentGameMode().GetId();
        request.requestingPlayerName = localPeer.GetLocalClientInfo().GetPlayerName();

        localPeer.SendRequest(MatchInvitationRequest.ID, request);
        return new PeerStateInvitePending(localPeer, localPeer.GetCurrentGameMode());
    }
}
