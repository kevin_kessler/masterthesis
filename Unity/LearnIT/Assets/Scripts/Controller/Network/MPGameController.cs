﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Utils;
using System.Collections;

public abstract class MPGameController : NetworkBehaviour
{
    //seconds to pass until automatically returning to lobby after match finished. can be overriden by sub classes in "GetMaxCountdownToReturnToLobby()"
    private const int COUNTDOWN_TO_RETURN_TO_LOBBY = 15; 
    protected string LOG_PREFIX;

    protected AudioPlayer audioPlayer;
    protected LobbyController lobby;
    protected UIGameOverview gameOverviewUI;
    protected Dictionary<uint, MPGamePlayer> gamePlayers;
    protected UILoadingIndicator loadingGameIndicator;
    private MPGamePlayer localGamePlayer = null;

    [SyncVar]
    private bool matchIsFinished;
    private bool isInitialized = false;

    private void OnStartClientOrServer()
    {
        this.LOG_PREFIX = "[" + this.GetType().Name + "]: ";
        Debug.Log(LOG_PREFIX + "OnStartClientOrServer");

        lobby = GameObject.FindObjectOfType<LobbyController>();
        gamePlayers = new Dictionary<uint, MPGamePlayer>();
        matchIsFinished = false;

        InitBothSides();
    }

    public sealed override void OnStartServer()
    {
        base.OnStartServer();
        OnStartClientOrServer();
        Debug.Log(LOG_PREFIX + "OnStartServer");

        InitServerSide();

        //hosts will also call OnStartClient()...
        if(!isClient)
            isInitialized = true;
    }

    public sealed override void OnStartClient()
    {
        base.OnStartClient();

        /*host = client and server at same time. 
         * in that case, OnStartClientOrServer has already been invoked in OnStartServer*/
        if (!isServer) 
            OnStartClientOrServer();

        Debug.Log(LOG_PREFIX + "OnStartClient");

        //init audio for clients
        audioPlayer = gameObject.AddComponent<AudioPlayer>();

        //show loadingGame indicator until all clients loaded the game scene
        this.transform.position = new Vector3(0, 0, 0);
        loadingGameIndicator = UIElement.Instantiate<UILoadingIndicator>(this.transform,"LoadingGameIndicator");
        loadingGameIndicator.SetIndicationText(Global.STRING_INDICATE_LOADING_GAME);
        loadingGameIndicator.SetIsCancelAble(false);
        loadingGameIndicator.Show();

        //prepare gameOverview for clients
        gameOverviewUI = InitGameOverview();
        gameOverviewUI.SetBackButtonAction(() => { GetLocalGamePlayer().CmdReadyToReturnToLobby(); });
        gameOverviewUI.Hide();
        isInitialized = true;

        InitClientSide();
    }
    
    public bool IsInitialized()
    {
        return isInitialized;
    }

    //////// GamePlayer Management Begin /////////
    public void AddGamePlayer(MPGamePlayer player)
    {
        Debug.Log(LOG_PREFIX + "AddGamePlayer: " + player);

        if (null == player || gamePlayers.ContainsKey(player.netId.Value))
            return;

        gamePlayers.Add(player.netId.Value, player);
        gameOverviewUI.AddPlayerUI(player.GetUI());
    }

    public void RemoveGamePlayer(MPGamePlayer player)
    {
        Debug.Log(LOG_PREFIX + "RemoveGamePlayer " + player);

        if (null == player || !gamePlayers.ContainsKey(player.netId.Value))
            return;
        
        gamePlayers.Remove(player.netId.Value);
        gameOverviewUI.RemovePlayerUI(player.GetUI());
    }

    public MPGamePlayer GetGamePlayer(uint playerID)
    {
        return gamePlayers[playerID];
    }

    public MPGamePlayer GetLocalGamePlayer()
    {
        if (null == localGamePlayer)
            foreach (MPGamePlayer player in gamePlayers.Values)
            {
                if (player!=null && player.isLocalPlayer)
                    localGamePlayer = player;
            }
        
        return localGamePlayer;
    }
    //////// GamePlayer Management End /////////

    //////// Start and Finish Match Functionality Begin //////

    //called on the server when all clients are ready to begin match.
    //notifies all clients about match has started
    [Server]
    protected void StartMatch()
    {
        Debug.Log(LOG_PREFIX + "invoking RpcStartMatch...");
        RpcStartMatch();
    }

    [ClientRpc]
    private void RpcStartMatch()
    {
        Debug.Log(LOG_PREFIX + "RpcStartMatch");
        loadingGameIndicator.Hide();
        OnMatchStarted();
    }

    //called on the server when game ends. notfies clients about match finished
    [Server]
    protected void FinishMatch()
    {
        Debug.Log(LOG_PREFIX + "invoking RpcFinishMatch...");
        matchIsFinished = true;
        StartCoroutine(ServerCountdownToReturnToLobbyRoutine());
        RpcMatchFinished();
    }

    [ClientRpc]
    private void RpcMatchFinished()
    {
        Debug.Log(LOG_PREFIX + "RpcMatchFinished");
        ShowGameOverview();
        OnMatchFinished();
    }

    [Server]
    public void OnPlayerReadyToBeginMatch(MPGamePlayer gamePlayer)
    {
        Debug.Log(LOG_PREFIX + "OnLobbyServerPlayerReadyToBeginMatch player=" + gamePlayer + " (currentPlayerCount=" + gamePlayers.Values.Count + ")");

        if (gamePlayers.Values.Count < lobby.minPlayers)
            return;

        bool allReady = true;
        foreach (MPGamePlayer player in gamePlayers.Values)
        {
            if (!player.IsReadyToBeginMatch())
            {
                allReady = false;
                break;
            }
        }

        //start match when all clients are ready to begin
        if (allReady)
        {
            OnAllPlayersReadyToBeginMatch();
        }
    }

    //////// Start and Finish Match Functionality End //////

    //////// Return To Lobby Functionality Begin /////////

    [Server]
    private IEnumerator ServerCountdownToReturnToLobbyRoutine()
    {
        int currentCountDown = GetCountdownForLobby() + 1;
        float remainingTime = currentCountDown;

        while (currentCountDown > 0)
        {
            yield return null;

            //to avoid flooding the network of message, we only inform client when the number of plain seconds change.
            int newFloorTime = Mathf.FloorToInt(remainingTime);
            if (newFloorTime != currentCountDown)
            {
                currentCountDown = newFloorTime;
                RpcUpdateReturnToLobbyCountdown(currentCountDown);
            }

            remainingTime -= Time.deltaTime;
        }

        //if we aren't yet returned to lobby, do it now
        if (!Application.loadedLevelName.Equals(lobby.lobbyScene))
            OnAllPlayersReadyToReturnToLobby();
    }

    [Server]
    public void OnPlayerReadyToReturnToLobby(MPGamePlayer gamePlayer)
    {
        Debug.Log(LOG_PREFIX + "OnLobbyServerGamePlayerReadyToReturnToLobby player=" + gamePlayer);

        bool allReady = true;
        foreach (MPGamePlayer player in gamePlayers.Values)
        {
            if (!player.IsReadyToReturnToLobby())
            {
                allReady = false;
                break;
            }
        }

        //return to lobby if all players are ready to return
        if (allReady)
        {
            OnAllPlayersReadyToReturnToLobby();
        }
    }

    [Server]
    private void OnAllPlayersReadyToReturnToLobby()
    {
        StopCoroutine(ServerCountdownToReturnToLobbyRoutine());
        lobby.InvokeReturnToLobby();
    }
    //////// Return To Lobby Functionality End /////////


    //////// Game Overview Functionality Begin /////////

    [Client]
    private void ShowGameOverview()
    {
        if (null == gameOverviewUI)
            return;
        audioPlayer.PlayNotificationSound();
        gameOverviewUI.Show();
    }

    //updates text of returnToLobbyCountdown on each client
    [ClientRpc]
    private void RpcUpdateReturnToLobbyCountdown(int countdown)
    {
        if (null != gameOverviewUI)
            gameOverviewUI.SetBackToLobbyTimerText(countdown);
    }

    public virtual void Update()
    {
        ShowGameOverviewOnTab();
    }

    [Client]
    protected virtual void ShowGameOverviewOnTab()
    {
        if (matchIsFinished || null == gameOverviewUI)
            return;

        //display overview during match when pressing tab
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            gameOverviewUI.ShowWithoutFooter();
        }

        if (Input.GetKeyUp(KeyCode.Tab))
        {
            gameOverviewUI.Hide();
        }
    }

    //////// Game Overview Functionality End /////////

    /// HOOKS ///

    protected abstract void InitBothSides();
    protected abstract void InitServerSide();
    [Client]
    protected virtual UIGameOverview InitGameOverview()
    {
        if (gameOverviewUI == null)
            gameOverviewUI = UIElement.Instantiate<UIGameOverview>();
        return gameOverviewUI;
    }
    protected abstract void InitClientSide();
    //called when all gameplayers are finished initialising and ready to start. should invoke "StartMatch()"
    [Server]
    protected abstract void OnAllPlayersReadyToBeginMatch();
    //called on the client when the match was started by the server
    [Client]
    protected abstract void OnMatchStarted();
    //called on the client when the match was finished by the server
    [Client]
    protected abstract void OnMatchFinished();
    [Server]
    protected virtual int GetCountdownForLobby()
    {
        return 25;
    }
}
