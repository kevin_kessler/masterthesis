﻿using UnityEngine;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.IO;

public class GameMode {
    private static List<GameMode> ALL_GAME_MODES = new List<GameMode>();
    private static int idCounter = 0;

    public static GameMode FindGameModeByID(int id)
    {
        foreach (GameMode gm in ALL_GAME_MODES)
            if (gm.id == id)
                return gm;

        Debug.LogError("Could not find GameMode with id: " + id);

        return null;
    }

    private int id;
    private string name;
    private string description;
    private Sprite spriteImage;
    private string gameSceneName; //name of the scene to be loaded when playing this game mode
    private string tutorialPath;
    private string mpGamePlayerPrefabPath; //path to the prefab of the multiplayer GamePlayer Prefab for this game mode

    public GameMode(string name, string description, string spritePath, string gameSceneName, string tutorialPath = null, string mpGamePlayerPrefabPath = null)
    {
        this.id = idCounter++;
        this.name = name;
        this.description = description;
        this.spriteImage = Resources.Load<Sprite> (spritePath);
        this.gameSceneName = gameSceneName;
        this.tutorialPath = tutorialPath;
        this.mpGamePlayerPrefabPath = mpGamePlayerPrefabPath;

        ALL_GAME_MODES.Add(this);
    }

    public int GetId()
    {
        return id;
    }

    public string GetName()
    {
        return name;
    }

    public bool IsMultiplayer()
    {
        return null != mpGamePlayerPrefabPath;
    }

    public Sprite GetSpriteImage()
    {
        return spriteImage;
    }

    public string GetGameSceneName()
    {
        return gameSceneName;
    }

    public string GetDescription()
    {
        return description;
    }

    public bool HasTutorial()
    {
        return null != tutorialPath;
    }

    public string GetTutorialPath()
    {
        return tutorialPath;
    }

    public string GetMPGamePlayerPrefabPath()
    {
        return mpGamePlayerPrefabPath;
    }
}
