﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GCGraph {

    private List<GCNode> nodes = null;
    private Sprite image;
    private GCGraphData graphData;

    public GCGraph(Sprite sourceImage, GCGraphData data)
    {
        this.nodes = new List<GCNode>();
        this.image = sourceImage;
        this.graphData = data;
    }

    public void SetGraphImage(Sprite image)
    {
        this.image = image;
    }

    public void AddNode(GCNode node)
    {
        if (nodes.Contains(node))
            return;

        nodes.Add(node);
    }

    public List<GCNode> GetNodes()
    {
        return nodes;
    }

    public void ResetColorOfAllNodes()
    {
        Color defaultColor = GetDefaultNodeColor();
        foreach (GCNode node in nodes)
            ColorizeNode(node, defaultColor);
    }

    public void ResetColorOfNode(GCNode node)
    {
        ColorizeNode(node, GetDefaultNodeColor());
    }

    public void ColorizeNode(GCNode node, Color color)
    {
        if (node.GetColor() == color)
            return;

        node.SetColor(color);
        foreach (GCNode.Point pixel in node.GetPixels())
            image.texture.SetPixel(pixel.x, pixel.y, color);
        image.texture.Apply();
    }

    public void ColorizeNodeBorder(GCNode node, Color color)
    {
        foreach (GCNode.Point pixel in node.GetBorder())
            image.texture.SetPixel(pixel.x, pixel.y, color);
        image.texture.Apply();
    }

    public Sprite GetImage()
    {
        return image;
    }

    public int GetBorderThickness()
    {
        return graphData.GetBorderThickness();
    }

    public string GetName()
    {
        return graphData.GetGraphImageName();
    }

    public Color GetDefaultNodeColor()
    {
        return graphData.GetDefaultNodeColor();
    }

    public Color[] GetUsableColors()
    {
        return graphData.GetUsableColors();
    }

    public int GetAllowdNumberOfAttempts()
    {
        return graphData.GetAllowdNumberOfAttempts();
    }

    public float GetSolveDuration()
    {
        return graphData.GetSolveDuration();
    }

    public GCNode GetNodeByPosition(int xPos, int yPos)
    {
        GCNode.Point pos = new GCNode.Point(xPos, yPos);

        return GetNodeByPosition(pos);
    }

    public GCNode GetNodeByPosition(GCNode.Point pos)
    {
        foreach (GCNode node in nodes)
        {
            if (node.HasPixel(pos))
                return node;
        }

        return null;
    }

}
