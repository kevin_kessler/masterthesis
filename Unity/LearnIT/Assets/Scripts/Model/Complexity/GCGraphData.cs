﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GCGraphData {

    private static System.Random rng = new System.Random();
    private static Dictionary<int, GCGraphData> graphDataById = new Dictionary<int, GCGraphData>();
    private static int idCounter = 0;

    public static GCGraphData GetGraphDataById(int id)
    {
        if (!graphDataById.ContainsKey(id))
            return null;

        return graphDataById[id];
    }
    
    public static List<GCGraphData> GetRandomSelectionOfGraphData(int amount)
    {
        Debug.Assert(amount <= graphDataById.Count);
        
        //add all graph data to the list, and then randomly remove entries until amount is reached
        List<GCGraphData> dataList = new List<GCGraphData>();
        foreach (GCGraphData data in graphDataById.Values)
            dataList.Add(data);
        while(dataList.Count>amount)
            dataList.RemoveAt(rng.Next(0, dataList.Count));

        return dataList;
    }

    public static List<GCGraphData> GetAllGraphData()
    {
        List<GCGraphData> dataList = new List<GCGraphData>();
        foreach (GCGraphData data in graphDataById.Values)
            dataList.Add(data);

        return dataList;
    }

    private int id;
    private string graphImageName = null;
    private int borderThickness = -1;
    private int allowdNumberOfAttempts = -1;
    private float solveDuration = -1;
    private Color[] usableColors;
    private Color defaultNodeColor = Color.white; //the color that the nodes are going to be interpreted from the image

    public GCGraphData(string graphImageName, int borderThickness, int allowdNumberOfAttempts, float solveDuration, Color[] usableColors)
    {
        this.id = idCounter++;
        this.graphImageName = graphImageName;
        this.borderThickness = borderThickness;
        this.allowdNumberOfAttempts = allowdNumberOfAttempts;
        this.solveDuration = solveDuration;
        this.usableColors = usableColors;

        graphDataById.Add(id, this);
    }

    public string GetGraphImageName()
    {
        return graphImageName;
    }

    public int GetBorderThickness()
    {
        return borderThickness;
    }

    public int GetAllowdNumberOfAttempts()
    {
        return allowdNumberOfAttempts;
    }

    public float GetSolveDuration()
    {
        return solveDuration;
    }

    public Color[] GetUsableColors()
    {
        return usableColors;
    }

    public Color GetDefaultNodeColor()
    {
        return defaultNodeColor;
    }

    public int GetId()
    {
        return id;
    }
}
