﻿using UnityEngine;
using System.Collections;
using Utils;

public class MySPMode : SPGameController {

    [SerializeField] GameObject holder;
    UITimeSlider timer;

    protected override void Init(){
        //init UI
        timer = UIElement.Instantiate<UITimeSlider>(holder.transform);
        timer.Reset(10);
        timer.Pause();
        timer.Hide();

        //game over condition
        timer.OnFinished += FinishGame;
    }

    protected override IEnumerator LoadGame(){
        //simulate heavy tasks such as reading files
        yield return new WaitForSeconds(5);
    }

    protected override void OnGameLoaded(){
        //everything prepared. game can start.
        timer.Show();
        timer.Continue();
    }

    protected override void OnGameFinished(){
        //clean up used resources, save highscore, etc. 
    }

    void Update(){
        //gameplay / input processing
        if (InputUtils.GetPressedKey() == KeyCode.Return)
            timer.AddTime(2);
    }
}
