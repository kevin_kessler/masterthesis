﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using Utils;

public class MyMPMode : MPGameController {
    [SerializeField] GameObject holder;
    UITimeSlider timer;

    protected override void InitBothSides(){
        //server needs timer for sync and control
        //client needs timer for visual representation
        timer = UIElement.Instantiate<UITimeSlider>();
        timer.Reset(10); timer.Pause(); timer.Hide();
    }

    [Server] protected override void InitServerSide(){
        //only server determines when game is over
        timer.OnFinished += FinishMatch;
    }

    [Client] protected override void InitClientSide(){
        //only client needs to display the timer
        timer.transform.SetParent(holder.transform, false);
    }

    [Server] protected override void OnAllPlayersReadyToBeginMatch(){
        //all players ready. start game on server side.
        StartMatch(); timer.Continue();
    }

    [Client] protected override void OnMatchStarted(){
        //game started on server side. start on client side as well.
        timer.Show(); timer.Continue();
    }

    [Client] protected override void OnMatchFinished(){
        //clean up used resources, etc.
        timer.Hide();
    }

    //gameplay specific implementation
    [Server] public void PlayerPressedReturn(){
        timer.AddTime(2);
        RpcSyncTime(timer.GetCurrentValue());
    }

    [ClientRpc] void RpcSyncTime(float time){
        timer.SetTime(time);
    }
}
