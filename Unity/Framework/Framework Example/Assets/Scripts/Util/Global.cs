﻿using UnityEngine;
using System.Collections;
using System.IO;
using System;

public class Global {
    //miscellaneous Strings
    public static readonly string APPLICATION_NAME = "MyApp_v1.0";
    public static readonly string STRING_APPLICATION_TITLE = "My App";
    public static readonly string STRING_PAUSE = "Pause";
    public static readonly string STRING_CHALLENGE = "Challenge";
    public static readonly string STRING_GAME_MODE = "Gamemode";
    public static readonly string STRING_GAME_MODE_INFO = "Description";
    public static readonly string STRING_SOLUTION = "Solution";
    public static readonly string STRING_SHOW_SOLUTION = "Show Solution";
    public static readonly string STRING_HIDE_SOLUTION = "Hide Solution";
    public static readonly string STRING_HELP = "Help";
    public static readonly string STRING_OK = "OK";
    public static readonly string STRING_CANCEL = "Cancel";
    public static readonly string STRING_CLOSE = "Close";

    //Colors
    public static readonly Color COLOR_PINK = new Color(1.0f, 0.65f, 1.0f);
    public static readonly Color COLOR_FLASH_RED = new Color(0.77f, 0.27f, 0.27f);
    public static readonly Color COLOR_FLASH_GREEN = new Color(0.27f, 0.75f, 0.4f);
    public static readonly Color COLOR_DISABLED_GRAY = new Color(0.61f, 0.61f, 0.61f);
    public static readonly Color COLOR_INACTIVE_GRAY = new Color(0.85f, 0.85f, 0.85f, 0.8f);

    //Image Paths
    public static readonly string PATH_IMAGES = "Images" + Path.AltDirectorySeparatorChar;
    public static readonly string PATH_ICONS = PATH_IMAGES + "Icons" + Path.AltDirectorySeparatorChar;
    public static readonly string PATH_ICON_QUIT = PATH_ICONS + "quit";
    public static readonly string PATH_ICON_INFO = PATH_ICONS + "info";
    public static readonly string PATH_ICON_WIN = PATH_ICONS + "win";
    public static readonly string PATH_ICON_LOSE = PATH_ICONS + "lose";
    public static readonly string PATH_ICON_ERROR = PATH_ICONS + "cancel";
    public static readonly string PATH_ICON_PLAYER = PATH_ICONS + "player";
    public static readonly string PATH_ICON_PAUSE = PATH_ICONS + "pause";
    public static readonly string PATH_ICON_GAMEPAD = PATH_ICONS + "gamepad";
    public static readonly string PATH_ICON_QUESTIONMARK = PATH_ICONS + "questionmark";

    //Prefabs
    public static readonly string PATH_PREFABS_DIR = "Prefabs" + Path.AltDirectorySeparatorChar;

    //Indicator strings
    public static readonly string STRING_INDICATE_NO_CONNECTION = "Connection could not be established";
    public static readonly string STRING_INDICATE_MATCH_DECLINED = "Your request has been declined";
    public static readonly string STRING_INDICATE_TRYING_TO_CONNECT = "Connecting...";
    public static readonly string STRING_INDICATE_WAITING_FOR_RESPONSE = "Waiting for response...";
    public static readonly string STRING_INDICATE_WAITING_FOR_PLAYERS = "Waiting for other player...";
    public static readonly string STRING_INDICATE_WAITING_FOR_INVITATION_RESPONSE = "Player invited. Waiting for response...";
    public static readonly string STRING_INDICATE_ENTERING_LOBBY = "Entering Lobby ...";
    public static readonly string STRING_INDICATE_RETURNING_TO_LOBBY = "Returning to Lobby...";
    public static readonly string STRING_INDICATE_LOADING_GAME = "Loading Game ...";
    public static readonly string STRING_INDICATE_PLAYER_LEFT_LOBBY = "Other player left lobby";
    
    //Settings Strings
    public static readonly string STRING_SETTINGS_TITLE = "Settings";
    public static readonly string STRING_SETTINGS_MUSICVOL = "Music Volume";
    public static readonly string STRING_SETTINGS_SFXVOL = "SFX Volume";
    public static readonly string STRING_SETTINGS_MUTE = "Mute";
    public static readonly string STRING_SETTINGS_EDIT_PLAYERNAME_TITLE = "Change Playername";
    public static readonly string STRING_SETTINGS_EDIT_PLAYERNAME_DESCRIPTION = "You are playing as" + Environment.NewLine + Environment.NewLine + 
                                                                    "\"{0}\"." + Environment.NewLine + Environment.NewLine +
                                                                    "You can change your name here:";
    //Choose Opponent Dialog Strings
    public static readonly string STRING_DIALOG_CHOOSE_OPPONENT_TITLE = "Choose a Player";
    public static readonly string STRING_DIALOG_CHOOSE_OPPONENT_LIST_TITLE = "Players in local network";
    public static readonly string STRING_DIALOG_MATCH_INVITATION_TITLE = "Received Invitation";
    public static readonly string STRING_DIALOG_MATCH_INVITATION_DESCRIPTION = "\"{0}\" challenged you to play \"{1}\"!" + Environment.NewLine + Environment.NewLine
                                                                               +"Do you want to accept the challenge?";
    //Leave Lobby Dialog Strings
    public static readonly string STRING_DIALOG_LEAVE_LOBBY_TITLE = "Really leave Lobby?";
    public static readonly string STRING_DIALOG_LEAVE_LOBBY_DESCRIPTION = "Are you sure to leave the lobby?";
                                                       
    //Lobby Strings
    public static readonly string STRING_LOBBY_TITLE = "Lobby";
    public static readonly string STRING_LOBBY_CHAT_TITLE = "Chat";
    public static readonly string STRING_LOBBY_CHAT_SEND = "Send";
    public static readonly string STRING_LOBBY_CHAT_PLACEHOLDER = "Type...";
    public static readonly string STRING_LOBBY_PLAYER_LIST_TITLE = "Player";
    public static readonly string STRING_LOBBY_PLAYER_COLOR = "Colour";
    public static readonly string STRING_LOBBY_PLAYER_NAME = "Name";
    public static readonly string STRING_LOBBY_PLAYER_READY = "Ready";
    public static readonly string STRING_LOBBY_LEAVE = "Leave";

    //Game Overview Strings
    public static readonly string STRING_OVERVIEW_TITLE = "Overview";
    public static readonly string STRING_OVERVIEW_COLOR = "Colour";
    public static readonly string STRING_OVERVIEW_NAME = "Player";
    public static readonly string STRING_OVERVIEW_BACK_TO_LOBBY = "Back to Lobby";
    public static readonly string STRING_OVERVIEW_RETURNING_TO_LOBBY = "Back to Lobby in {0}s ...";
    public static readonly string STRING_OVERVIEW_CORRECT = "Correct";
    public static readonly string STRING_OVERVIEW_QUESTION = "Question";
    public static readonly string STRING_OVERVIEW_WAITING_FOR_PLAYER = "Waiting for other players...";
    
    //Quiz specific strings
    public static readonly string STRING_OVERVIEW_QUIZ_QUESTION = "Question";
    public static readonly string STRING_INDICATE_LOADING_QUIZ = "Downloading questions...";
    public static readonly string STRING_QUIZ_HELP_TEXT = "Try to answer all questions correctly." + Environment.NewLine + Environment.NewLine +
                                                          "Each question offers exactly four answers, but only one is correct." + Environment.NewLine + Environment.NewLine +
                                                          "Furthermore, each question has an individual timer, which will count the question as \"Answered Wrong\" if it ends.";
    //Theory specific Strings
    public static readonly string STRING_THEORY_TITLE = "Theory of \"{0}\"";
    public static readonly string STRING_THEORY_PAGE = "Page";
}
