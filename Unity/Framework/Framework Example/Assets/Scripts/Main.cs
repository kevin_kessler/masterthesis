﻿using UnityEngine;
using System.Collections.ObjectModel;
using System.Collections.Generic;

public static class Main {

    //Use this to add new games to the game selection
    public static void main()
    {
        CreateExampleGame();

        Game myGame = new Game("My Game");
        myGame.AddGameMode(new GameMode(
            "SP Mode Name",
            "SP Mode Description",
            "Path to Mode Image",
            "MySPScene"
        ));
        myGame.AddGameMode(new GameMode(
            "MP Mode Name",
            "MP Mode Description",
            "Path to Mode Image",
            "MyMPScene",
            null,
            "MyMPPlayer"
        ));
    }

    private static void CreateExampleGame()
    {
        Game example = new Game("Example");

        //Theory Mode Examlpe
        example.AddGameMode(
            new GameModeTheory(
                "custom_theory", // Theory XML
                "Theory", //Title
                "Theory Description", //Description
                "Images/Icons/book", //Path to Mode Icon
                "Theory Scene" //Mode Scene Name
        ));

        //Singleplayer Quiz Mode Example
        example.AddGameMode(
            new GameModeQuiz(
                "custom_quiz", //quiz xml
                "Quiz", //title
                "Quiz Description", //description
                "Images/Icons/quiz", //Path to Mode Icon 
                "Quiz Scene", //Mode Scene Name
                "Video/quizsp" //Path to Mode Tutorial Video
        ));

        //Multiplayer Quiz Mode Example
        example.AddGameMode(
            new GameModeQuiz(
                "custom_quiz", //quiz xml
                "Quiz Multiplayer", //title
                "Quiz Description", //description
                "Images/Icons/quiz_mp", //Path to Mode Icon 
                "MP Quiz Scene", //Mode Scene Name
                "Video/quizmp", //Path to Mode Tutorial Video
                "Prefabs/Network/MPQuizPlayer" //path to multiplayer prefab
        ));
    }
}
