﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.ObjectModel;
using PeerMsgTypes;

public class NetworkController : MonoBehaviour {

    public const int BROADCAST_PORT = 42420;
    public const int MATCHMAKING_PORT = 42421;
    public const int LOBBY_PORT = 42422;

    private NetworkDiscoveryService discoveryService;
    private OpponentSelectionController opponentSelection;
    //private NetworkLobbyManager lobby;
    private Peer localPeer;
    private GameMode suggestedGameMode; //the gamemode, suggested to an opponent

    private UILoadingIndicator connectingIndicator;
    private UIToastFlasher toaster;

    public void Initialize(LocalClientInfo clientInfo)
    {
        //start the network discovery broadcasting. (the listening is only active while opponent selection is active)
        GameObject discoveryObject = new GameObject("DiscoveryService");
        discoveryObject.transform.SetParent(gameObject.transform);
        discoveryService = discoveryObject.AddComponent<NetworkDiscoveryService>();
        discoveryService.SetInfoToBroadcast(clientInfo);
        discoveryService.StartBroadcasting();

        //prepare the opponent selection menu. handover discoveryservice to enable / disable listening
        GameObject oppnentSelectionObject = new GameObject("OpponentSelection");
        oppnentSelectionObject.transform.SetParent(gameObject.transform);
        opponentSelection = oppnentSelectionObject.AddComponent<OpponentSelectionController>();
        opponentSelection.SetNetworkDiscoveryService(discoveryService);
        opponentSelection.AddOnSelectionSubmittedHandler(OnOpponentSelected);

        //prepare local peer for sending and receiving messages to / from other peers
        GameObject peerObject = new GameObject("LocalPeer");
        peerObject.transform.SetParent(gameObject.transform);
        localPeer = peerObject.AddComponent<Peer>();
        localPeer.Init(MATCHMAKING_PORT, clientInfo);
    }


    public void OfferOpponentSelection(GameMode gameModeToPlay){
        if (!gameModeToPlay.IsMultiplayer())
            return;

        suggestedGameMode = gameModeToPlay;
        opponentSelection.ShowOpponentSelectionDialog();
    }

    private void OnOpponentSelected(NetworkClientInfo selectedOpponent)
    {
        Debug.Log("Selected " + selectedOpponent.GetPlayerName() + " as opponent!");

        if (selectedOpponent.IsPlaying())
        {
            Debug.LogWarning("Can not request match against " + selectedOpponent.GetPlayerName() + " since the user is already in a match");
            return;
        }

        //connect to selected opponent, offering a match in the currentGameMode
        if (null == connectingIndicator) InitConnectingIndicator();
        connectingIndicator.Show();
        StartCoroutine(localPeer.ConnectToServer(selectedOpponent.GetIP(), suggestedGameMode, OnConnectingFinished));
    }

    private void InitConnectingIndicator()
    {
        connectingIndicator = UIElement.Instantiate<UILoadingIndicator>();
        connectingIndicator.SetIndicationText(Global.STRING_INDICATE_TRYING_TO_CONNECT);
        connectingIndicator.AddOnCancelAction(() =>
        {
            OnConnectingCanceled();
        });
        connectingIndicator.Hide();
    }

    private void ToastErrorMessage(string msg)
    {
        if (null == toaster){
            toaster = UIElement.Instantiate<UIToastFlasher>();
        }

        StartCoroutine(toaster.Flash(Color.white, Global.COLOR_FLASH_RED, msg));
    }

    //callback for peer.ConnectToServer. triggered after connection established or timed out
    private void OnConnectingFinished(bool success)
    {
        if (!success)
        {
            //abort connection attempt and display timeout msg
            localPeer.ResetClient();
            ToastErrorMessage(Global.STRING_INDICATE_NO_CONNECTION);
        }
        connectingIndicator.Hide();
    }

    //connection canceled by user
    private void OnConnectingCanceled()
    {
        localPeer.ResetClient();
        connectingIndicator.Hide();
    }

    
}
