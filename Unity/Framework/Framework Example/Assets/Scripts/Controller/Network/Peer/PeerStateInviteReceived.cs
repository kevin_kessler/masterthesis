﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using PeerMsgTypes;

public class PeerStateInviteReceived : PeerState {

    private NetworkMessage invitationRequestMsg;
    private MatchInvitationRequest invitationRequest;
    private UIDecisionDialog invitationReceivedDialog;
    private GameMode requestedGameMode;

    public PeerStateInviteReceived(Peer localPeer, NetworkMessage invitationRequestMsg)
        :base(localPeer)
    {
        Debug.Assert(invitationRequestMsg.msgType == MatchInvitationRequest.ID);
        
        this.invitationRequestMsg = invitationRequestMsg;
        this.invitationRequest = invitationRequestMsg.ReadMessage<MatchInvitationRequest>();
    }

    public override void OnStateEnter()
    {
        base.OnStateEnter();

        invitationReceivedDialog = UIElement.Instantiate<UIDecisionDialog>();
        invitationReceivedDialog.Setup(
                Global.STRING_DIALOG_MATCH_INVITATION_TITLE,
                "",
                null,
                () => { localPeer.TransitionTo( RespondToMatchInvitation(false) );},
                () => { localPeer.TransitionTo( RespondToMatchInvitation(true) );}
            );

        PrepareMatchInvitationResponse();
    }

    public override void OnStateExit()
    {
        base.OnStateExit();
        invitationReceivedDialog.Hide();
        Object.Destroy(invitationReceivedDialog.gameObject);
    }

    //when client disconnects before we answered the invitation, return to idle
    public override PeerState OnServerLostClient(NetworkMessage netMsg)
    {
        base.OnServerLostClient(netMsg);
        return new PeerStateIdle(localPeer);
    }

    //if we get another invitation while we are still handling the current one, dismiss it
    public override PeerState OnMatchInvitationRequest(NetworkMessage netMsg)
    {
        base.OnMatchInvitationRequest(netMsg);
        MatchInvitationResponse response = new MatchInvitationResponse();
        response.accepted = false;
        localPeer.SendResponse(MatchInvitationResponse.ID, response, invitationRequestMsg);
        return this;
    }

    public override PeerState OnCancelMatchInvitationRequest(NetworkMessage netMsg)
    {
        base.OnCancelMatchInvitationRequest(netMsg);
        //send ack
        localPeer.SendResponse(CancelMatchInvitationResponse.ID, new CancelMatchInvitationResponse(), netMsg);
        return new PeerStateIdle(localPeer);
    }

    private void PrepareMatchInvitationResponse()
    {
        //dont accept any invitation, if player is in match
        if (localPeer.GetLocalClientInfo().IsPlaying())
        {
            Debug.LogError("Received MatchInvitation while playing. This should never happen.");
            PeerState newState = RespondToMatchInvitation(false);
            localPeer.TransitionTo(newState);
            return;
        }

        //read request
        string requestingPlayerName = invitationRequest.requestingPlayerName;
        requestedGameMode = GameMode.FindGameModeByID(invitationRequest.requestedGameModeID);
        if (null == requestedGameMode)
        {
            Debug.LogError("Couldn't find requested GameMode of invitation.");
            PeerState newState = RespondToMatchInvitation(false);
            localPeer.TransitionTo(newState);
            return;
        }

        //update info text of dialog according to requesting player info
        invitationReceivedDialog.SetInfoText(
            string.Format(
                Global.STRING_DIALOG_MATCH_INVITATION_DESCRIPTION, 
                requestingPlayerName, 
                requestedGameMode.GetName()
                )
            );
        invitationReceivedDialog.SetInfoIcon(requestedGameMode.GetSpriteImage());

        //show decision dialog
        invitationReceivedDialog.Show();
    }

    private PeerState RespondToMatchInvitation(bool accepted)
    {
        MatchInvitationResponse response = new MatchInvitationResponse();
        response.accepted = accepted;
        localPeer.SendResponse(MatchInvitationResponse.ID, response, invitationRequestMsg);

        if (response.accepted)
            //transition to lobby
            return new PeerStateInLobby(localPeer, requestedGameMode);
        else
            return new PeerStateIdle(localPeer);
    }
}
