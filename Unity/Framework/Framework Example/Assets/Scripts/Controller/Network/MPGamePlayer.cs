﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;
using System.Collections;

public abstract class MPGamePlayer : NetworkBehaviour {

    protected string LOG_PREFIX;

    protected MPGameController gameController;
    protected UIMPGamePlayer playerUI;

    [SyncVar(hook = "SetPlayerColor")]
    private Color playerColor;

    [SyncVar(hook = "SetPlayerName")]
    private string playerName;

    [SyncVar(hook = "SetReadyToReturnToLobby")]
    private bool isReadyToReturnToLobby = false;

    [SyncVar(hook = "SetReadyToBeginMatch")]
    private bool isReadyToBeginMatch = false;

    public sealed override void OnStartClient()
    {
        LOG_PREFIX = "[" + this.GetType().Name + "][" + playerName + "]: ";
        Debug.Log(LOG_PREFIX + "OnStartClient");

        playerUI = InitPlayerUI();

        SetPlayerColor(playerColor);
        SetPlayerName(playerName);
        SetReadyToReturnToLobby(isReadyToReturnToLobby);
        SetReadyToBeginMatch(isReadyToBeginMatch);

        //find current active gamecontroller. it might still be initializing, so we wait for it in co routine.
        StartCoroutine(FindGameControllerRoutine());
    }

    private IEnumerator FindGameControllerRoutine()
    {
        Debug.Log(LOG_PREFIX + "FindGameControllerRoutine");
        //wait for the gamecontroller to be loaded in order to find it
        do
        {
            yield return null;
            Debug.Log(LOG_PREFIX + "@FindGameControllerRoutine: looking for object of type MPGameController...");
            gameController = (MPGameController)Object.FindObjectOfType(typeof(MPGameController));
        } while (null == gameController || !gameController.IsInitialized());

        OnGameControllerReady();
    }

    private void OnGameControllerReady()
    {
        Debug.Log(LOG_PREFIX + "OnGameControllerReady");
        gameController.AddGamePlayer(this);
        InitClientSide();
        if (isLocalPlayer)
        {
            Debug.Log(LOG_PREFIX + "calling CmdSetReadyToBeginMatch...");
            //notify server about client is ready to start match
            CmdReadyToBeginMatch();
        }
    }

    public UIMPGamePlayer GetUI()
    {
        return playerUI;
    }

    public void SetPlayerName(string name)
    {
        this.playerName = name;
        LOG_PREFIX = "[" + this.GetType().Name + "][" + playerName + "]: ";
        if (null != playerUI)
            playerUI.SetPlayerName(playerName);
    }

    public void SetPlayerColor(Color color)
    {
        this.playerColor = color;
        if (null != playerUI)
            playerUI.SetPlayerColor(playerColor);
    }

    public void SetReadyToReturnToLobby(bool ready)
    {
        this.isReadyToReturnToLobby = ready;
        if (null != playerUI)
            playerUI.SetReadyToggleState(ready);
    }

    public void SetReadyToBeginMatch(bool ready)
    {
        this.isReadyToBeginMatch = ready;
    }

    public string GetPlayerName()
    {
        return playerName;
    }

    public Color GetPlayerColor()
    {
        return playerColor;
    }

    public bool IsReadyToReturnToLobby()
    {
        return isReadyToReturnToLobby;
    }

    public bool IsReadyToBeginMatch()
    {
        return isReadyToBeginMatch;
    }

    //used to tell the server that the player has been initialized, loaded and is ready to play
    [Command]
    public void CmdReadyToBeginMatch()
    {
        Debug.Log(LOG_PREFIX + "CmdSetReadyToBeginMatch");
        this.isReadyToBeginMatch = true;
        gameController.OnPlayerReadyToBeginMatch(this);
    }

    //used to tell the server that the player is ready to return to the lobby
    [Command]
    public void CmdReadyToReturnToLobby()
    {
        Debug.Log(LOG_PREFIX + "CmdSetReadyToReturnToLobby");

        this.isReadyToReturnToLobby = true;
        playerUI.SetReadyToggleState(true);
        gameController.OnPlayerReadyToReturnToLobby(this);
    }

    public override string ToString()
    {
        return "GamePlayer=" + GetPlayerName();
    }

    public void OnDestroy()
    {
        Debug.Log(LOG_PREFIX + "OnDestroy()");

        gameController.RemoveGamePlayer(this);
        Destroy(playerUI.gameObject);
    }

    /// HOOKS

    protected abstract void InitClientSide();
    protected virtual UIMPGamePlayer InitPlayerUI()
    {
        if (playerUI == null)
            playerUI = UIElement.Instantiate<UIMPGamePlayer>();
        return playerUI;
    }
}
