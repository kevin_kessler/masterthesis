﻿using UnityEngine;
using System.Collections;
using System.IO;
using UnityEngine.UI;
using System;

public class AudioPlayer : MonoBehaviour {

    private static readonly string LOG_PREFIX = typeof(AudioPlayer).Name + ": ";
    public static readonly string PATH_AUDIO = "Audio" + Path.AltDirectorySeparatorChar;

    private class Sound
    {
        public AudioClip clip;
        public bool isPlaying;

        public Sound(AudioClip clip)
        {
            this.clip = clip;
            isPlaying = false;
        }
    }

    //Sounds will be loaded lazy on demand
    private static Sound wrongSound;
    private static Sound correctSound;
    private static Sound gameWinSound;
    private static Sound gameOverSound;
    private static Sound keyPadSound;
    private static Sound volumeSound;
    private static Sound notificationSound;

    private static AudioClip menuMusic;
    private static AudioClip quizMusic;

    private static float sfxVolume = 1.0f;
    private static float musicVolume = 1.0f;
    private static AudioSource bgMusic;
    private static bool isMuted;

    public static void Mute() { 
        isMuted = true;

        if (null != bgMusic && bgMusic.isPlaying)
            try
            {
                bgMusic.Pause();
            }
            catch (Exception e)
            {
                Debug.LogError(LOG_PREFIX + "Couldn't mute music for unknown reason. Exception:" + e.Message);
            }
    }
    public static void Unmute() { 
        isMuted = false;  
        
        if(null!=bgMusic && !bgMusic.isPlaying)
            try
            {
                bgMusic.Play();
            }
            catch (Exception e)
            {
                Debug.LogError(LOG_PREFIX + "Couldn't unmute music for unknown reason. Exception:" + e.Message);
            }
    }

    public static void SetSFXVolume(float volume)
    {
        sfxVolume = volume;
    }

    public static void SetMusicVolume(float volume)
    {
        musicVolume = volume;

        if (null != bgMusic)
        {
            bgMusic.volume = volume;
        }
    }

	void Start()
	{
        if(null==bgMusic)
            InitBgMusic();
	}

	private void InitBgMusic()
	{
        //for background music
        bgMusic = gameObject.AddComponent<AudioSource>();
        bgMusic.loop = true;
        bgMusic.volume = musicVolume;
	}

    private void PlayBgMusic(AudioClip clip)
    {
        if (null == bgMusic || null==clip)
            return;

        try
        {
            if (bgMusic.clip!=null && clip!=null && bgMusic.clip.Equals(clip) && bgMusic.isPlaying)
                return;

            if(bgMusic.isPlaying)
                bgMusic.Stop();
            bgMusic.clip = clip;
            if (!isMuted)
                bgMusic.Play();
        }
        catch (Exception e)
        {
            Debug.LogError(LOG_PREFIX + "Couldn't play music for unknown reason. Muting now, to prevent further exceptions. Exception:" + e.Message);
            Mute();
        }
    }

    public void PauseBgMusic()
    {
        if (null == bgMusic)
            return;

        try
        {
            if(!isMuted)
                bgMusic.Pause();
        }
        catch (Exception e)
        {
            Debug.LogError(LOG_PREFIX + "Couldn't pause music for unknown reason. Muting now, to prevent further exceptions. Exception:" + e.Message);
            Mute();
        }
    }

    public void ResumeBgMusic()
    {
        if (null == bgMusic)
            return;

        try
        {
            if(!isMuted)
                bgMusic.Play();
        }
        catch (Exception e)
        {
            Debug.LogError(LOG_PREFIX + "Couldn't pause music for unknown reason. Muting now, to prevent further exceptions. Exception:" + e.Message);
            Mute();
        }
    }

    public void PlayMenuMusic(){
        if (null == bgMusic)
            return;
        if (null == menuMusic)
            menuMusic = Resources.Load<AudioClip>(PATH_AUDIO + "speedenza_background-theme-1");

        PlayBgMusic(menuMusic);
    }

    public void PlayQuizMusic(){
        if (null == bgMusic)
            return;
        if(null==quizMusic)
            quizMusic = Resources.Load<AudioClip>(PATH_AUDIO + "setuniman_searching-1m55");
        PlayBgMusic(quizMusic);
    }

    public void StopBgMusic()
    {
        if (null == bgMusic)
            return;

        try
        {
            if(!bgMusic.isPlaying)
                return;

            if (bgMusic.isPlaying)
                bgMusic.Stop();
        }
        catch (Exception e)
        {
            Debug.LogError(LOG_PREFIX + "Couldn't stop music for unknown reason. Muting now, to prevent further exceptions. Exception:" + e.Message);
            Mute();
        }
    }

    private void PlaySound(Sound sound)
    {
        PlaySound(sound, sfxVolume);
    }

    private void PlaySound(Sound sound, float volume)
    {
        if (isMuted || null == sound || volume <= 0)
            return;

        try
        {
            AudioSource.PlayClipAtPoint(sound.clip, Camera.main.transform.position, volume);
            StartCoroutine(TrackIsPlayingRoutine(sound));
        }
        catch (Exception e)
        {
            Debug.LogError(LOG_PREFIX + "Couldn't play sound for unknown reason. Muting now, to prevent further exceptions. Exception:"+e.Message);
            Mute();
        }
    }

    private IEnumerator TrackIsPlayingRoutine(Sound sound)
    {
        sound.isPlaying = true;
        yield return new WaitForSeconds(sound.clip.length);
        sound.isPlaying = false;
    }
	
	public void PlayCorrectSound()
	{
        if (null == correctSound){
            correctSound = new Sound((AudioClip)Resources.Load(PATH_AUDIO + "fins_success-1"));
            if (null == correctSound) {
                Debug.LogWarning(LOG_PREFIX + " could not load " + Utils.DebugUtils.GetMemberName(() => correctSound));
                return;
            }
        }

        PlaySound(correctSound);
	}

    public void PlayWrongSound()
    {
        if (null == wrongSound)
        {
            wrongSound = new Sound((AudioClip)Resources.Load(PATH_AUDIO + "bertrof_game-sound-wrong"));
            if (null == wrongSound)
            {
                Debug.LogWarning(LOG_PREFIX + " could not load " + Utils.DebugUtils.GetMemberName(() => wrongSound));
                return;
            }
        }

        PlaySound(wrongSound);
    }

    public void PlayGameWinSound()
    {
        if (null == gameWinSound)
        {
            gameWinSound = new Sound((AudioClip)Resources.Load(PATH_AUDIO + "littlerobotsoundfactory_jingle-win-synth-05"));
            if (null == gameWinSound)
            {
                Debug.LogWarning(LOG_PREFIX + " could not load " + Utils.DebugUtils.GetMemberName(() => gameWinSound));
                return;
            }
        }

        PlaySound(gameWinSound);
    }

    public void PlayGameOverSound()
    {
        if (null == gameOverSound)
        {
            gameOverSound = new Sound((AudioClip)Resources.Load(PATH_AUDIO + "themusicalnomad_negative-beeps"));
            if (null == gameOverSound)
            {
                Debug.LogWarning(LOG_PREFIX + " could not load " + Utils.DebugUtils.GetMemberName(() => gameOverSound));
                return;
            }
        }

        PlaySound(gameOverSound);
    }

    public void PlayKeyPadSound()
    {
        if (null == keyPadSound)
        {
            keyPadSound = new Sound((AudioClip)Resources.Load(PATH_AUDIO + "keypad"));
            if (null == keyPadSound)
            {
                Debug.LogWarning(LOG_PREFIX + " could not load " + Utils.DebugUtils.GetMemberName(() => keyPadSound));
                return;
            }
        }

        PlaySound(keyPadSound);
    }

    public void PlayVolumeSound(float specificVolume)
    {
        if (null == volumeSound)
        {
            volumeSound = new Sound((AudioClip)Resources.Load(PATH_AUDIO + "kwahmah-02_beepping1"));
            if (null == volumeSound)
            {
                Debug.LogWarning(LOG_PREFIX + " could not load " + Utils.DebugUtils.GetMemberName(() => volumeSound));
                return;
            }
        }

        if(!volumeSound.isPlaying)
            PlaySound(volumeSound, specificVolume < 0 ? sfxVolume : specificVolume);
    }

    public void PlayNotificationSound()
    {
        if (null == notificationSound)
        {
            notificationSound = new Sound((AudioClip)Resources.Load(PATH_AUDIO + "plasterbrain_friend-request_converted"));
            if (null == notificationSound)
            {
                Debug.LogWarning(LOG_PREFIX + " could not load " + Utils.DebugUtils.GetMemberName(() => notificationSound));
                return;
            }
        }

        PlaySound(notificationSound);
    }
}
