﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Utils;

public class GameSelectionController : MonoBehaviour {

    private static bool executedMain = false;
    private static Game lastChosenGame;
    public static Game GetLastChosenGame()
    {
        return lastChosenGame;
    }

    private UIGameSelection gameSelectionUI;
    private SceneLoader sceneLoader;
    private LocalClientInfo localClientInfo;
    private AudioPlayer audioPlayer;

    void Awake()
    {
        if (executedMain)
            return;

        //Init GameManager
        GameManager.GetInstance();

        //Init games list
        Main.main();
        executedMain = true;
    }

    void Start()
    {
        this.transform.position = Vector3.zero;

        sceneLoader = gameObject.AddComponent<SceneLoader>();
        audioPlayer = gameObject.AddComponent<AudioPlayer>();
        audioPlayer.StopBgMusic();

        //init UI
        gameSelectionUI = UIElement.Instantiate<UIGameSelection>(this.transform);

        //listen for localClientInfo change, to update name of local player
        localClientInfo = GameManager.GetInstance().GetPlayerInfo();
        localClientInfo.OnModelChanged += OnLocalPlayerNameChanged;
        OnLocalPlayerNameChanged();

        //Get Games and init respective Gamebuttons
        foreach (Game g in Game.GetAllGames())
        {
            if(g!=null && g.GetModes()!=null && g.GetModes().Count>0)
                InitGameButton(g);
        }
    }

    private void InitGameButton(Game game)
    {
        //create game button and set its onClick behaviour
        Button gameButtonToInit = gameSelectionUI.AddGame(game);
        gameButtonToInit.onClick.AddListener(() =>
        {
            lastChosenGame = game;
            sceneLoader.LoadGameMenuScene(false);
        });
    }

    private void OnLocalPlayerNameChanged()
    {
        gameSelectionUI.GetMenuHeader().SetPlayerName(localClientInfo.GetPlayerName());
    }

    void OnDestroy()
    {
        //deregister listener
        if (null != GameManager.GetInstance())
        {
            localClientInfo.OnModelChanged -= OnLocalPlayerNameChanged;
        }
    }
}
