﻿using UnityEngine;
using Utils;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class SceneLoader : MonoBehaviour{

    //Scenes
    public static readonly string SCENE_MAIN = "Main Scene";
    public static readonly string SCENE_LOBBY = "Lobby Scene";
    public static readonly string SCENE_GAME_MENU = "GameMenu Scene";

    private static string previousScene = null;
    private static string currentScene = SCENE_MAIN;

    void Start(){
        //Init required singletons
        GameManager.GetInstance();
    }

    public void LoadMainScene()
    {
        LoadScene(SCENE_MAIN, false);
    }

    public void LoadGameMenuScene(bool showTransition = true)
    {
        LoadScene(SCENE_GAME_MENU, showTransition);
    }

    public void LoadPreviousScene(bool showTransition = true)
    {
        if (null == previousScene)
            return;

        LoadScene(previousScene, showTransition);
    }

    public void LoadScene(GameMode mode, bool showTransition = true)
    {
        if (null == mode)
            return;

        LoadScene(mode.GetGameSceneName(), showTransition);
    }

    private void LoadScene(string name, bool showTransition)
    {
        //Debug.Log("Loading Scene: " + name);
        previousScene = currentScene;
        currentScene = name;

        if (showTransition)
            UISceneFader.GetInstance().TransitionToScene(name);
        else
            Application.LoadLevel(name);
    }

    public void QuitApplication()
    {
#if UNITY_EDITOR
        EditorApplication.isPlaying = false;
#else 
        Application.Quit();
#endif
    }
}
