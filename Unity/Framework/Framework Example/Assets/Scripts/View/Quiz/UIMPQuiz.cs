﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections.Generic;
using Utils;
using System.Collections;

public class UIMPQuiz : UIQuiz
{
    //Progress Area of Opponent
    [SerializeField] private GameObject progressHolderOpponent;
    private UIProgressMarker[] progressMarkersOpponent;

    [SerializeField] private Text localPlayerName;
    [SerializeField] private Text opponentName;
    [SerializeField] private Image localPlayerBackground;
    [SerializeField] private Image opponentBackground;

    public override void Setup(QuizQuestion firstQuestion, int amountOfQuestions, UnityAction<int> onAnswerSelectAction)
    {
        base.Setup(firstQuestion, amountOfQuestions, onAnswerSelectAction);

        progressMarkersOpponent = new UIProgressMarker[amountOfQuestions];
        for (int i = 0; i < amountOfQuestions; i++)
        {
            progressMarkersOpponent[i] = UIElement.Instantiate<UIProgressMarker>(progressHolderOpponent.transform);
            progressMarkersOpponent[i].SetNumber(i + 1);
        }
    }

    public void UpdateOpponentProgressMarker(int questionIndex, bool correct)
    {
        Debug.Assert(questionIndex >= 0 && questionIndex <= progressMarkersOpponent.Length);

        if(correct)
            progressMarkersOpponent[questionIndex].SetCorrect();
        else
            progressMarkersOpponent[questionIndex].SetWrong();
    }

    public void UpdateOpponentProgressMarker(int questionIndex)
    {
        Debug.Assert(questionIndex >= 0 && questionIndex <= progressMarkersOpponent.Length);
        progressMarkersOpponent[questionIndex].SetNumber(questionIndex + 1);
    }

    public void SetupLocalPlayerVisuals(MPGamePlayer localPlayer){
        localPlayerName.text = localPlayer.GetPlayerName();
        localPlayerBackground.color = localPlayer.GetPlayerColor();
    }

    public void SetupRemotePlayerVisuals(MPGamePlayer remotePlayer)
    {
        opponentName.text = remotePlayer.GetPlayerName();
        opponentBackground.color = remotePlayer.GetPlayerColor();
    }

    public void RemovePauseButton()
    {
        gameHeaderWithSliderUI.RemovePauseButton();
    }
}
