﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System.Collections;

public class UILobbyChat : UIElement {

    [SerializeField] private InputField inputField; 
    [SerializeField] private ScrollRect scrollRect; 
    [SerializeField] private Button sendButton; 
    [SerializeField] private GameObject chatMessageHolder; 
    [SerializeField] private Text inputPlaceHolderText;
    [SerializeField] private Text sendButtonText;

#pragma warning disable 0414
    private UnityAction<string> sendCallback;
#pragma warning restore 0414
    private bool addedMessageDuringLastFrame;

    void Start()
    {
        sendButtonText.text = Global.STRING_LOBBY_CHAT_SEND;
        inputPlaceHolderText.text = Global.STRING_LOBBY_CHAT_PLACEHOLDER;

        //focus input field
#if(UNITY_STANDALONE)
        inputField.ActivateInputField();
#endif
    }

    void Update()
    {
        if (addedMessageDuringLastFrame)
        {
            //auto move scrollbar down
            scrollRect.verticalNormalizedPosition = 0;
            scrollRect.verticalScrollbar.value = 0;
            addedMessageDuringLastFrame = false;
        }

        if (inputField.isFocused)
            return;

        KeyCode pressedKey = Utils.InputUtils.GetPressedKey();
        if (pressedKey == KeyCode.Return || pressedKey == KeyCode.KeypadEnter)
        {
            sendButton.onClick.Invoke();
#if(UNITY_STANDALONE)
            inputField.ActivateInputField();
#endif
        }
    }

    public void SetSendCallback(UnityAction<string> sendCallback)
    {
        if (null == sendCallback)
            return;

        this.sendCallback = sendCallback;

        sendButton.onClick.RemoveAllListeners();
        sendButton.onClick.AddListener(() => 
        {
            if (string.IsNullOrEmpty(inputField.text))
                return;

            sendCallback.Invoke(inputField.text); 
            inputField.text = ""; 
        });
    }

    //isLocalMessage == true -> this message has been sent from the local client. else: localClient received it from remote client
    public void AddChatMessage(string senderName, string messageText, bool isLocalMessage)
    {
        UIChatMessage chatMessageUI = isLocalMessage
                                    ? UIElement.Instantiate<UIChatMessage>(Global.PATH_PREFABS_DIR + "UIChatMessageRight", chatMessageHolder.transform)
                                    : UIElement.Instantiate<UIChatMessage>(Global.PATH_PREFABS_DIR + "UIChatMessageLeft", chatMessageHolder.transform);

        chatMessageUI.Setup(senderName, messageText);

        addedMessageDuringLastFrame = true;
    }
}
