﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System.Collections;

public class UIGameMenuHeader : UIElement {
    [SerializeField] private Button btnLeft;
    [SerializeField] private Button btnRight;
    [SerializeField] private Button btnPlayerName;
    [SerializeField] private Text txtTitle;
    [SerializeField] private Text txtPlayerName;

    private SceneLoader sceneLoader;

    void Awake()
    {
        sceneLoader = gameObject.AddComponent<SceneLoader>();

        //default behavior
        btnLeft.onClick.AddListener(sceneLoader.LoadMainScene);
        btnRight.onClick.AddListener(GameManager.GetInstance().GetSettingsController().ShowSettingsMenu);
        btnPlayerName.onClick.AddListener(GameManager.GetInstance().GetSettingsController().ShowPlayerNamePrompt);
    }

    public Button GetLeftButton()
    {
        return btnLeft;
    }

    public Button GetRightButton()
    {
        return btnRight;
    }

    public Button GetPlayerButton()
    {
        return btnPlayerName;
    }

    public void SetTitleText(string text)
    {
        txtTitle.text = text;
    }

    public void SetPlayerName(string name)
    {
        txtPlayerName.text = name;
    }

    public void HidePlayerName()
    {
        btnPlayerName.gameObject.SetActive(false);
    }
}
