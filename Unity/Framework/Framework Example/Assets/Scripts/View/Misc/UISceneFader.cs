﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UISceneFader : UIElement
{
    private static UISceneFader instance;

    public static UISceneFader GetInstance()
    {

        if (applicationIsQuitting)
        {
            return null;
        }

        if (null == instance)
        {
            instance = UIElement.Instantiate<UISceneFader>(null, "SceneFader");

            //we need to keep this over scenes, to fade in after level was loaded
            DontDestroyOnLoad(instance);
        }
        return instance;
    }

    [SerializeField]
    private Image fadeImage;
    [SerializeField]
    private float fadeSpeed = 15f;
    private bool sceneStarting = false;
    private bool sceneEnding = false;

    public void TransitionToScene(string sceneName)
    {
        StopAllCoroutines();
        Show();
        StartCoroutine(TransitionToSceneRoutine(sceneName));
    }

    private IEnumerator TransitionToSceneRoutine(string sceneName)
    {
        Show();
        yield return StartCoroutine(EndSceneRoutine());
        AsyncOperation asyncLoad = Application.LoadLevelAsync(sceneName);
        while (!asyncLoad.isDone)
            yield return null;
        yield return StartCoroutine(StartSceneRoutine());
        Hide();
    }

    private IEnumerator StartSceneRoutine()
    {
        if (sceneStarting || sceneEnding)
            yield break;

        sceneStarting = true;
        fadeImage.color = Color.black;

        while (sceneStarting)
        {
            // Fade the texture to clear.
            fadeImage.color = Color.Lerp(fadeImage.color, Color.clear, fadeSpeed * Time.deltaTime);

            // If the texture is almost clear...
            if (fadeImage.color.a <= 0.05f)
            {
                // ... set the colour to clear and disable the texture.
                fadeImage.color = Color.clear;
                sceneStarting = false;
            }

            yield return null;
        }
    }


    private IEnumerator EndSceneRoutine()
    {
        if (sceneStarting || sceneEnding)
            yield break;

        sceneEnding = true;
        fadeImage.color = Color.clear;

        while (sceneEnding)
        {
            // fade towards black.
            fadeImage.color = Color.Lerp(fadeImage.color, Color.black, fadeSpeed * Time.deltaTime);

            // If the screen is almost black...
            if (fadeImage.color.a >= 0.95f)
            {
                // ... set the colour to black and disable the texture.
                fadeImage.color = Color.black;
                sceneEnding = false;
            }

            yield return null;
        }
    }

    /// <summary>
    /// When Unity quits, it destroys objects in a random order.
    /// In principle, a Singleton is only destroyed when application quits.
    /// If any script calls Instance after it have been destroyed, 
    ///   it will create a buggy ghost object that will stay on the Editor scene
    ///   even after stopping playing the Application. Really bad!
    /// So, this was made to be sure we're not creating that buggy ghost object.
    /// </summary>
    private static bool applicationIsQuitting = false;
    public void OnDestroy()
    {
        applicationIsQuitting = true;
    }
}