﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using Utils;

public class MyMPPlayer : MPGamePlayer {
    [Client] protected override void InitClientSide(){
        //init game specific player properties if necessary
    }

    //gameplay specific implementation
    void Update(){
        if (!isLocalPlayer) return;

        if (InputUtils.GetPressedKey() == KeyCode.Return)
            CmdPressedReturn();
    }
    
    [Command] public void CmdPressedReturn(){
        ((MyMPMode)gameController).PlayerPressedReturn();
    }
}
