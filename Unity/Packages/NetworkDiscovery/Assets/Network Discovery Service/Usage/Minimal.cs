﻿using UnityEngine;
using System.Collections;
using System.Collections.ObjectModel;
using UnityEngine.Networking;
using System;

public class Minimal : MonoBehaviour, INetworkChangedHandler{
    private NetworkDiscoveryService service;

    void Start(){
        //Set up the network discovery service
        service = gameObject.AddComponent<NetworkDiscoveryService>();
        service.SetClientTimeOut(3); //drop discovered clients if quiet for 3s
        service.SetInfoToBroadcast(InitLocalClientInfo());
        service.RegisterNetworkChangedHandler(this); //register callbacks

        //Start sending and listening on port 42420
        service.StartBroadcasting(42420);
        service.StartListening(42420);
    }

    //Specify the information to be broadcasted
    private LocalClientInfo InitLocalClientInfo(){
        string appName = "MyApp_v1.0";
        string id = SystemInfo.deviceUniqueIdentifier + DateTime.Now.ToString("ssff");
        string name = "Player " + UnityEngine.Random.Range(0, 1000);
        return new LocalClientInfo(appName, id, name, false);
    }

    //Callbacks invoked by the service upon network changes
    public void OnClientJoined(NetworkClientInfo client) {
        Debug.Log("Client:"+ client.GetID() + " joined the network");
    }
    public void OnClientLeft(NetworkClientInfo client) {
        Debug.Log("Client:" + client.GetID() + " left the network");
    }
    public void OnClientUpdated(NetworkClientInfo client) {
        Debug.Log("Client:" + client.GetID() + " updated");
    }
}
