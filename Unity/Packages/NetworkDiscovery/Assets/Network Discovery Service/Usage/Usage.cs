﻿using UnityEngine;
using System.Collections;
using System.Collections.ObjectModel;
using UnityEngine.Networking;

public class Usage : MonoBehaviour, INetworkChangedHandler
{
    private NetworkDiscoveryService discoveryService;
    private LocalClientInfo localClientInfo;
    private ReadOnlyCollection<NetworkClientInfo> currentClients;

    void Start()
    {
        localClientInfo = InitLocalClientInfo();
        nameInput = localClientInfo.GetPlayerName();

        discoveryService = gameObject.AddComponent<NetworkDiscoveryService>();
        discoveryService.SetInfoToBroadcast(localClientInfo);
        discoveryService.RegisterNetworkChangedHandler(this);

        //start sending and listening on port 42420
        discoveryService.StartBroadcasting(42420);
        discoveryService.StartListening(42420);
    }

    LocalClientInfo InitLocalClientInfo()
    {
        string appName = "MyApp_v1.0";
        //string id = SystemInfo.deviceUniqueIdentifier + "-" + Random.Range(0, int.MaxValue) + "-" + System.DateTime.Now.ToString("yyyyMMddHHmmssff");
        string id = System.DateTime.Now.ToString("ssff");
        string name = "Player " + Random.Range(0, 1000);
        return new LocalClientInfo(appName, id, name, false);
    }

    public void OnClientJoined(NetworkClientInfo client) {
        Debug.Log(client.GetPlayerName() + " joined the network");
        currentClients = discoveryService.GetDiscoveredClients();
    }

    public void OnClientLeft(NetworkClientInfo client) {
        Debug.Log(client.GetPlayerName() + " left the network");
        currentClients = discoveryService.GetDiscoveredClients();
    }

    public void OnClientUpdated(NetworkClientInfo client) {
        Debug.Log(client.GetPlayerName() + " updated");
        currentClients = discoveryService.GetDiscoveredClients();
    }

    string nameInput;
    void OnGUI()
    {
        //change player name
        Rect nameLabelRect = new Rect(0, 0, 50, 25);
        Rect nameInputRect = new Rect(55, 0, 100, 25);
        Rect nameButtonRect = new Rect(160, 0, 50, 25);
        GUI.Label(nameLabelRect, "Name:");
        nameInput = GUI.TextField(nameInputRect, nameInput);
        if (GUI.Button(nameButtonRect, "Set") && !string.IsNullOrEmpty(nameInput))
            localClientInfo.SetPlayerName(nameInput);
            

        if(null==currentClients || currentClients.Count <= 0)
            return;

        //client list
        Rect clientRect = new Rect(0, 30, Screen.width, 50);
        for (int i = 0; i < currentClients.Count; i++)
        {
            GUI.Button(clientRect, currentClients[i].GetPlayerName() + " | " + currentClients[i].GetID() + " | " + currentClients[i].GetIP());
            clientRect = new Rect(0, clientRect.y + clientRect.height, clientRect.width, clientRect.height);
        }
    }
}
