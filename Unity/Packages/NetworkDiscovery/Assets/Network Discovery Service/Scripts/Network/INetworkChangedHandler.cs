﻿using UnityEngine;
using System.Collections;

public interface INetworkChangedHandler {

    void OnClientJoined(NetworkClientInfo client);
    void OnClientLeft(NetworkClientInfo client);
    void OnClientUpdated(NetworkClientInfo client);

}
