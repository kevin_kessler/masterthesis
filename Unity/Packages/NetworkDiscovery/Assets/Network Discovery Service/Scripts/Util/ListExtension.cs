﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Utils
{
    public static class ListExtension
    {
        private static System.Random rng = new System.Random();


        //performant way to insert items into a list by order. src: http://stackoverflow.com/questions/12172162/how-to-insert-item-into-list-in-order
        public static void AddSorted<T>(this List<T> @this, T item) where T : System.IComparable<T>
        {
            if (@this.Count == 0)
            {
                @this.Add(item);
                return;
            }
            if (@this[@this.Count - 1].CompareTo(item) <= 0)
            {
                @this.Add(item);
                return;
            }
            if (@this[0].CompareTo(item) >= 0)
            {
                @this.Insert(0, item);
                return;
            }
            int index = @this.BinarySearch(item);
            if (index < 0)
                index = ~index;
            @this.Insert(index, item);
        }


        //shuffles the elements of the given list in a random order. src: http://stackoverflow.com/questions/273313/randomize-a-listt-in-c-sharp
        public static void Shuffle<T>(this List<T> @this)
        {
            int n = @this.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                T value = @this[k];
                @this[k] = @this[n];
                @this[n] = value;
            }
        }
    }
}
