﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.Networking;

[RequireComponent (typeof(LobbyController))]
public class Usage : MonoBehaviour {

    private LobbyController lobby;
    private bool isInLobby = false;

    void Start()
    {
        lobby = GetComponent<LobbyController>();
        lobby.SetLocalPlayerName("Player " + Random.Range(1, 1000));
    }

    private void JoinLobby(string hostAddress)
    {
        UnityAction<bool> onConnectFinished = (success) =>{
            if (!success){
                Debug.LogWarning("Could not connect");
                return;
            }

            lobby.ClearOnLobbyLeaveButtonActions();
            lobby.onLobbyLeaveButton += LeaveLobby;

            lobby.ClearOnLobbyShutDownActions();
            lobby.onLobbyShutDown += LeaveLobby;

            isInLobby = true;
            lobby.Show();
        };

        Debug.Log("Connecting to " + hostAddress + " ...");
        lobby.Connect(hostAddress,  onConnectFinished);
    }

    private void HostLobby()
    {
        UnityAction<bool> onHostFinished = (success) =>{
            if (!success){
                Debug.LogWarning("Could not host");
                return;
            }

            lobby.ClearOnLobbyLeaveButtonActions();
            lobby.onLobbyLeaveButton += StopHosting;

            isInLobby = true;
            lobby.Show();
        };

        //host the lobby and join as localclient
        Debug.Log("Trying to host...");
        lobby.Host(onHostFinished);
    }

    private void LeaveLobby(){
        lobby.Disconnect();
        lobby.Hide();
        isInLobby = false;
    }

    private void StopHosting(){
        lobby.EndHost();
        LeaveLobby();
    }

    string address = "localhost";
    void OnGUI()
    {
        if (isInLobby)
            return;

        Rect addressRect = new Rect(0, 0, 200, 20);
        Rect joinRect = new Rect(0, 25, 200, 50);
        Rect hostRect = new Rect(0, 100, 200, 50);


        string newAddress = GUI.TextField(addressRect, address);
        if (null!= newAddress && !newAddress.Equals(address))
            address = newAddress;

        if (GUI.Button(joinRect, "Connect"))
        {
            JoinLobby(address);
        }

        if (GUI.Button(hostRect, "Host"))
        {
            HostLobby();
        }
    }
}
