﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.Networking;

[RequireComponent (typeof(LobbyController))]
public class Minimal : MonoBehaviour {

    private LobbyController lobby;
    void Start(){
        lobby = GetComponent<LobbyController>();
        lobby.SetLocalPlayerName("Player " + Random.Range(1, 1000));
    }

    //join the lobby as remote client
    private void JoinLobby(string hostAddress){
        lobby.Connect(hostAddress,  OnConnectFinished);
    }

    //host the lobby and join as localclient
    private void HostLobby(){
        lobby.Host(OnHostFinished);
    }

    //disconnect from the lobby, hide UI and remove handlers
    private void LeaveLobby(){
        lobby.Disconnect();
        lobby.Hide();
        lobby.ClearOnLobbyLeaveButtonActions();
        lobby.ClearOnLobbyShutDownActions();
    }

    //stop hosting, disconnect and hide UI
    private void StopHosting(){
        lobby.EndHost();
        LeaveLobby();
    }

    //if successfully connected, set client handlers 
    //and show lobby UI
    private void OnConnectFinished(bool success){
        if (!success) return;
        lobby.onLobbyLeaveButton += LeaveLobby;
        lobby.onLobbyShutDown += LeaveLobby;
        lobby.Show();
    }

    //if successfully hosted, set host handler
    //and show lobby UI
    private void OnHostFinished(bool success){
        if (!success) return;
        lobby.onLobbyLeaveButton += StopHosting;
        lobby.Show();
    }
}
