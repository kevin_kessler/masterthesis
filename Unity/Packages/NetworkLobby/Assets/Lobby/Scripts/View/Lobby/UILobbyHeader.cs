﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System.Collections;

public class UILobbyHeader : UIElement
{
    [SerializeField] private Button btnLeft;
    [SerializeField] private Text txtTitle;

    public Button GetLeftButton()
    {
        return btnLeft;
    }

    public void SetTitleText(string text)
    {
        txtTitle.text = text;
    }
}
