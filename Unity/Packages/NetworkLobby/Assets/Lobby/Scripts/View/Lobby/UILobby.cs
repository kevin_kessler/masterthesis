﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections.Generic;
using Utils;

public class UILobby : UIElement
{
    //Header
    [SerializeField] private GameObject headerHolder;
    private UILobbyHeader header;


    //LobbyInfo Area
    [SerializeField] private Text gameModeTitle;
    [SerializeField] private Text gameModeDescription;
    
    //PlayerArea
    [SerializeField] private Text playerListTitle;
    [SerializeField] private Text playerListColTextColor;
    [SerializeField] private Text playerListColTextName;
    [SerializeField] private Text playerListColTextReady;
    [SerializeField] private GameObject playerListHolder;
    private List<UILobbyPlayer> lobbyPlayerUIs;

    //Chat Area
    [SerializeField] private Text chatTitle;
    [SerializeField] private GameObject chatHolder;
    private UILobbyChat chatUI;

    //bottom area
    [SerializeField] private Text leaveButtonText;
    [SerializeField] private Text readyButtonText;
    [SerializeField] private Button leaveButton;
    [SerializeField] private Button readyButton;

    void Awake()
    {
        //init header and chat ui elements
        header = UIElement.Instantiate<UILobbyHeader>(headerHolder.transform);
        header.transform.SetAsFirstSibling();
        chatUI = UIElement.Instantiate<UILobbyChat>(chatHolder.transform);

        //init list for playerUIs which will be added when a player joins the lobby
        lobbyPlayerUIs = new List<UILobbyPlayer>();

        //setup strings
        header.SetTitleText("Lobby");
        playerListTitle.text = "Players";
        playerListColTextColor.text = "Color";
        playerListColTextName.text = "Name";
        playerListColTextReady.text = "Ready";
        chatTitle.text = "Chat";
        leaveButtonText.text = "Leave";
        readyButtonText.text = "Ready";
    }

    public UILobbyHeader GetMenuHeader()
    {
        return header;
    }

    public UILobbyChat GetChatUI()
    {
        return chatUI;
    }

    public Button GetBackButton()
    {
        return header.GetLeftButton();
    }

    public Button GetLeaveButton()
    {
        return leaveButton;
    }

    public Button GetReadyButton()
    {
        return readyButton;
    }

    public void SetLeaveButtonAction(UnityAction leaveButtonAction)
    {
        if (null == leaveButtonAction)
            return;

        GetBackButton().onClick.RemoveAllListeners();
        GetBackButton().onClick.AddListener(leaveButtonAction);

        leaveButton.onClick.RemoveAllListeners();
        leaveButton.onClick.AddListener(leaveButtonAction);
    }

    public void SetReadyButtonAction(UnityAction readyButtonAction)
    {
        if (null == readyButtonAction)
            return;

        readyButton.onClick.RemoveAllListeners();
        readyButton.onClick.AddListener(readyButtonAction);
    }

    public void AddLobbyPlayerUI(UILobbyPlayer lobbyPlayerUI)
    {
        if (null == lobbyPlayerUI)
            return;

        lobbyPlayerUIs.Add(lobbyPlayerUI);
        lobbyPlayerUI.transform.SetParent(playerListHolder.transform, false);
    }

    public void RemoveLobbyPlayerUI(UILobbyPlayer lobbyPlayerUI)
    {
        if (null == lobbyPlayerUI || !lobbyPlayerUIs.Contains(lobbyPlayerUI))
            return;

        lobbyPlayerUIs.Remove(lobbyPlayerUI);
        lobbyPlayerUI.transform.SetParent(null);
    }

    public void SetGameModeTitle(string title)
    {
        gameModeTitle.text = title;
    }

    public void SetGameModeDescription(string text)
    {
        gameModeDescription.text = text;
    }

}
