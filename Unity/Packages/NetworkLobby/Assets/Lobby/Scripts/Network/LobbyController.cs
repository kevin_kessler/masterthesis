﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class LobbyController : NetworkLobbyManager {

    public static readonly Color[] PLAYER_COLORS = new Color[] { Color.blue, Color.red, Color.green, Color.magenta, Color.yellow, Color.cyan };
    private static readonly string LOG_PREFIX = typeof(LobbyController).Name+": ";
    public const int COUNTDOWN_TO_START_MATCH = 3;

    public event UnityAction onLobbyShutDown; //being invoked when we are client and the lobby shuts down
    public event UnityAction onLobbyLeaveButton;
    public void ClearOnLobbyLeaveButtonActions() { onLobbyLeaveButton = null; }
    public void ClearOnLobbyShutDownActions() { onLobbyShutDown = null; }


    private UILobby lobbyUI;
    private Dictionary<uint, LobbyPlayer> lobbyPlayers;
    private bool isStartingClient;
    private bool isStartingHost;
    private string localPlayerName = "Player";

    public void Start()
    {
        //hide default ui and instantiate custom one
        showLobbyGUI = false;
        lobbyUI = UIElement.Instantiate<UILobby>(gameObject.transform);
        lobbyUI.SetGameModeTitle("GameModeTitle");
        lobbyUI.SetGameModeDescription("Awesome GameMode Description!");
        lobbyUI.SetLeaveButtonAction(() => { if (null != onLobbyLeaveButton) onLobbyLeaveButton.Invoke(); });
        lobbyUI.Hide();

        lobbyPlayers = new Dictionary<uint, LobbyPlayer>();
    }

    public void Show()
    {
        lobbyUI.Show();
    }

    public void Hide()
    {
        lobbyUI.Hide();
    }

    public void SetLocalPlayerName(string name)
    {
        this.localPlayerName = name;
    }

    public string GetLocalPlayerName()
    {
        return localPlayerName;
    }

    public void Connect(string address, UnityAction<bool> onFinishCallback)
    {
        if (isStartingClient || isStartingHost)
            return;
        isStartingClient = true;
        StartCoroutine(ConnectRoutine(address, onFinishCallback));
    }

    public void Disconnect() { StartCoroutine(DisconnectRoutine()); }

    public void EndHost() { StartCoroutine(StopHostRoutine()); }

    public void Host(UnityAction<bool> onFinishCallback)
    {
        if (isStartingHost || isStartingClient)
            return;
        isStartingHost = true;
        StartCoroutine(HostRoutine(onFinishCallback));
    }

    private IEnumerator ConnectRoutine(string address, UnityAction<bool> onFinishCallback)
    {
        isStartingClient = true;
        networkAddress = address;
        StartClient();

        //wait for connection to be established
        float connectingTime = 0;
        float connectingTimeout = 3;
        while (!IsClientConnected() && connectingTime < connectingTimeout)
        {
            connectingTime += Time.deltaTime;
            yield return null;
        }

        bool success = IsClientConnected();
        if (!success)
            StopClient();

        isStartingClient = false;
        Debug.Log(LOG_PREFIX + "Attempted to join Lobby. Successful: " + success);
        if (null != onFinishCallback) onFinishCallback.Invoke(success);
    }

    private IEnumerator HostRoutine(UnityAction<bool> onFinishCallback)
    {
        isStartingHost = true;
        try
        {
            base.StartHost();
        }
        catch (System.Exception e)
        {
            Debug.LogError(e);
            StopHost();
            StopClient();
            if (null != onFinishCallback) onFinishCallback.Invoke(false);
            isStartingHost = false;
            yield break;
        }

        float time = 0;
        float timeout = 3;
        while (!NetworkServer.active && time < timeout)
        {
            time += Time.deltaTime;
            yield return null;
        }
        bool success = NetworkServer.active;

        isStartingHost = false;
        Debug.Log(LOG_PREFIX + "Attempted to host Lobby. Successful: " + success);
        if (null != onFinishCallback) onFinishCallback.Invoke(success);
    }

    private IEnumerator StopHostRoutine()
    {
        base.StopHost();
        while (NetworkServer.active)
            yield return null;
    }

    private IEnumerator DisconnectRoutine()
    {
        base.StopClient();
        while (IsClientConnected())
            yield return null;
    }

    public void AddLobbyPlayer(LobbyPlayer lp)
    {
        Debug.Log(LOG_PREFIX + "AddLobbyPlayer. lobbyplayer=" + lp.netId.Value);

        //add lobby player to maintaining list and to gui list
        if (null != lp && !lobbyPlayers.ContainsKey(lp.netId.Value))
        {
            lobbyPlayers.Add(lp.netId.Value, lp);
            lp.transform.SetParent(transform);
            lobbyUI.AddLobbyPlayerUI(lp.GetUI());
        }
    }

    public void RemoveLobbyPlayer(uint id)
    {
        Debug.Log(LOG_PREFIX + "RemoveLobbyPlayer. lobbyplayer="+id);

        if (!lobbyPlayers.ContainsKey(id))
            return;

        //actually remove the leaving player
        lobbyPlayers.Remove(id);
    }

    //called on clients
    public UILobby GetUI()
    {
        return lobbyUI;
    }

    //called on server, which knows all the players colors
    private Color[] GetPlayerColors()
    {
        Color[] colors = new Color[lobbyPlayers.Count];
        int i = 0;
        foreach (LobbyPlayer lp in lobbyPlayers.Values)
        {
            colors[i] = lp.GetPlayerColor();
            i++;
        }
        return colors;
    }

    //called on server to determine a currently unused color.
    //iterates through the color array, starting at currentColors position
    public Color GetUnusedPlayerColor(Color currentColor)
    {
        Color[] colorsInUse = GetPlayerColors();
        int currentColorIndex = System.Array.IndexOf(PLAYER_COLORS, currentColor);

        bool currentColorInUse = false;
        do
        {
            currentColorIndex = (currentColorIndex + 1) % PLAYER_COLORS.Length;
            currentColor = PLAYER_COLORS[currentColorIndex];
            currentColorInUse = false;
            foreach (Color usedColor in colorsInUse)
                if (currentColor == usedColor)
                    currentColorInUse = true;

        } while (currentColorInUse);

        return currentColor;
    }

    // ----------------- Server callbacks ------------------

    //This is called on the server when it is told that a client has finished switching from the lobby scene to a game player scene.
    public override bool OnLobbyServerSceneLoadedForPlayer(GameObject lobbyPlayerObject, GameObject gamePlayerObject)
    {
        Debug.Log(LOG_PREFIX + "OnLobbyServerSceneLoadedForPlayer (" + lobbyPlayerObject.GetComponent<LobbyPlayer>().GetPlayerName() + ")");
        base.OnLobbyServerSceneLoadedForPlayer(lobbyPlayerObject, gamePlayerObject);

        //transfer data from lobbyplayer to gameplayer as soon as its ready for it
        LobbyPlayer lobbyPlayer = lobbyPlayerObject.GetComponent<LobbyPlayer>();
        GamePlayer gamePlayer = gamePlayerObject.GetComponent<GamePlayer>();
        gamePlayer.SetPlayerName(lobbyPlayer.GetPlayerName());
        gamePlayer.SetPlayerColor(lobbyPlayer.GetPlayerColor());

        return true;
    }


    //called when all players are ready to start match
    public override void OnLobbyServerPlayersReady()
    {
        Debug.Log(LOG_PREFIX + "OnLobbyServerPlayersReady");

        //start countdown when all players are ready
        StartCoroutine(ServerCountdownToStartRoutine());
    }

    public IEnumerator ServerCountdownToStartRoutine()
    {
        float remainingTime = COUNTDOWN_TO_START_MATCH+1;
        int currentCountDown = COUNTDOWN_TO_START_MATCH+1;

        while (currentCountDown > 0)
        {
            yield return null;

            //to avoid flooding the network of message, we only inform client when the number of plain seconds change.
            int newFloorTime = Mathf.FloorToInt(remainingTime);
            if (newFloorTime != currentCountDown)
            {
                currentCountDown = newFloorTime;
                foreach(LobbyPlayer lp in lobbyPlayers.Values)
                {
                    lp.RpcUpdateStartCountdown(currentCountDown);
                }
            }

            remainingTime -= Time.deltaTime;
        }

        if(!Application.loadedLevelName.Equals(playScene))
            ServerChangeScene(playScene);
    }

    // ----------------- Server callbacks End ------------------

    // ----------------- Client callbacks Begin ------------------

    //being invoked when we are client and the lobby shuts down / kicks us
    //conn = the connection from us as local client to the server which was hosting the lobby
    public override void OnLobbyClientDisconnect(NetworkConnection conn)
    {
        Debug.Log(LOG_PREFIX+"OnLobbyClientDisconnect");
        base.OnLobbyClientDisconnect(conn);

        if (null != onLobbyShutDown)
        {
            onLobbyShutDown.Invoke();
        }
    }

    //Called on clients when a scene has completed loading, if the scene load was initiated by the server.
    //Scene changes can cause player objects to be destroyed. 
    //The default implementation of OnClientSceneChanged in the NetworkManager is to add a player object for the connection if no player object exists.
    public override void OnClientSceneChanged(NetworkConnection conn)
    {
        Debug.Log(LOG_PREFIX + "OnClientSceneChanged");
        base.OnClientSceneChanged(conn);

        //when entering lobbyScene, show lobby UI
        if (Application.loadedLevelName == lobbyScene)
            Show();
        else
            Hide();
    }
    // ----------------- Client callbacks End ------------------
}
