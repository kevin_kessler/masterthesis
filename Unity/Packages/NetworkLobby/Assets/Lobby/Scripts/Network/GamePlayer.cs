﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;
using System.Collections;

public class GamePlayer : NetworkBehaviour {

    protected string LOG_PREFIX;

    [SyncVar(hook = "SetPlayerColor")]
    private Color playerColor;

    [SyncVar(hook = "SetPlayerName")]
    private string playerName;

    public override void OnStartClient()
    {
        LOG_PREFIX = "[" + this.GetType().Name + "][" + playerName + "]: ";
        Debug.Log(LOG_PREFIX + "OnStartClient");

        SetPlayerColor(playerColor);
        SetPlayerName(playerName);
    }

    public void SetPlayerName(string name)
    {
        this.playerName = name;
        LOG_PREFIX = "[" + this.GetType().Name + "][" + playerName + "]: ";
    }

    public void SetPlayerColor(Color color)
    {
        this.playerColor = color;
    }

    public string GetPlayerName()
    {
        return playerName;
    }

    public Color GetPlayerColor()
    {
        return playerColor;
    }

    public override string ToString()
    {
        return "GamePlayer=" + GetPlayerName();
    }

    public void OnDestroy()
    {
        Debug.Log(LOG_PREFIX + "OnDestroy()");
    }

    void OnGUI()
    {
        if (!isLocalPlayer)
            return;

        Rect backRect = new Rect(25, 25, 200, 50);
        if (GUI.Button(backRect, "Back"))
            FindObjectOfType<LobbyController>().SendReturnToLobby();
    }
}
