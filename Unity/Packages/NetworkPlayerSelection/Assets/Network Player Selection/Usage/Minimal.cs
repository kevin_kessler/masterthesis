﻿using UnityEngine;
using System.Collections;
using System.Collections.ObjectModel;
using UnityEngine.Networking;

public class Minimal : MonoBehaviour{
    private NetworkDiscoveryService service;
    private NetworkPlayerSelection selection;

    void Start(){
        //init service and start broadcasting
        service = gameObject.AddComponent<NetworkDiscoveryService>();
        service.SetInfoToBroadcast(InitLocalClientInfo());
        service.StartBroadcasting(42420);

        //init player selection and handover discovery service
        selection = gameObject.AddComponent<NetworkPlayerSelection>();
        selection.SetDiscoveryService(service);
        selection.AddOnSubmittedHandler(OnPlayerSelected);

        //show player selection and listen to port 42420 as long as shown
        selection.Show(42420);
    }

    private void OnPlayerSelected(NetworkClientInfo player){
        Debug.Log("Selected " + player.GetPlayerName() + " | IP: " + player.GetIP());
    }

    LocalClientInfo InitLocalClientInfo(){
        string appName = "MyApp_v1.0";
        string id = SystemInfo.deviceUniqueIdentifier + "-" + Random.Range(0, int.MaxValue);
        string name = "Player " + Random.Range(0, 1000);
        return new LocalClientInfo(appName, id, name, false);
    }
}
