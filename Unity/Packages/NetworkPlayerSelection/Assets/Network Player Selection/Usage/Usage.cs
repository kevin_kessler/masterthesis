﻿using UnityEngine;
using System.Collections;
using System.Collections.ObjectModel;
using UnityEngine.Networking;

public class Usage : MonoBehaviour
{
    private NetworkDiscoveryService discoveryService;
    private LocalClientInfo localClientInfo;
    private NetworkPlayerSelection opponentSelection;
    private int port = 42420;

    void Start()
    {
        localClientInfo = InitLocalClientInfo();
        nameInput = localClientInfo.GetPlayerName();

        //start the network discovery broadcasting. (the listening is only active while opponent selection is active)
        GameObject discoveryObject = new GameObject("NetworkDiscoveryService");
        discoveryObject.transform.SetParent(gameObject.transform);
        discoveryService = discoveryObject.AddComponent<NetworkDiscoveryService>();
        discoveryService.SetInfoToBroadcast(localClientInfo);
        discoveryService.StartBroadcasting(port);

        //prepare the opponent selection menu. handover discoveryservice to enable / disable listening
        GameObject oppnentSelectionObject = new GameObject("NetworkPlayerSelection");
        oppnentSelectionObject.transform.SetParent(gameObject.transform);
        opponentSelection = oppnentSelectionObject.AddComponent<NetworkPlayerSelection>();
        opponentSelection.SetDiscoveryService(discoveryService);
        opponentSelection.AddOnSubmittedHandler(OnOpponentSelected);
    }

    private void OnOpponentSelected(NetworkClientInfo selectedOpponent)
    {
        Debug.Log("Selected " + selectedOpponent.GetPlayerName() + " | IP: " + selectedOpponent.GetIP());
    }

    LocalClientInfo InitLocalClientInfo()
    {
        string appName = "MyApp_v1.0";
        string id = SystemInfo.deviceUniqueIdentifier + "-" + Random.Range(0, int.MaxValue) + "-" + System.DateTime.Now.ToString("yyyyMMddHHmmssff");
        string name = "Player " + Random.Range(0, 1000);
        return new LocalClientInfo(appName, id, name, false);
    }

    string nameInput;
    void OnGUI()
    {
        //change player name
        Rect nameLabelRect = new Rect(0, 0, 60, 25);
        Rect nameInputRect = new Rect(65, 0, 100, 25);
        Rect nameButtonRect = new Rect(170, 0, 50, 25);
        GUI.Label(nameLabelRect, "Name:");
        nameInput = GUI.TextField(nameInputRect, nameInput);
        if (GUI.Button(nameButtonRect, "Set") && !string.IsNullOrEmpty(nameInput))
            localClientInfo.SetPlayerName(nameInput);

        //change isPlaying
        Rect isPlayingLabelRect = new Rect(0, 30, 60, 25);
        Rect isPlayingButtonRect = new Rect(65, 30, 100, 25);
        GUI.Label(isPlayingLabelRect, "IsPlaying:");
        if (GUI.Button(isPlayingButtonRect, localClientInfo.IsPlaying().ToString()))
            localClientInfo.SetIsPlaying(!localClientInfo.IsPlaying());

        //show player selection
        Rect buttonRect = new Rect(Screen.width-250, 0, 250, 50);
        if (GUI.Button(buttonRect, "Show Network Player Selection"))
            opponentSelection.Show(port);
    }
}
