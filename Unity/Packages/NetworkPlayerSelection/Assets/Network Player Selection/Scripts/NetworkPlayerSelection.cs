﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Utils;
using System.Linq;

public class NetworkPlayerSelection : MonoBehaviour, INetworkChangedHandler {

    private UIToggleListDialog playerSelectionDialogUI;
    private Sprite isPlayingSprite;

    private ReadOnlyCollection<NetworkClientInfo> availablePlayers;
    private NetworkClientInfo selectedPlayer = null;

    private NetworkDiscoveryService discoveryService;

    private event UnityAction<NetworkClientInfo> onSelectionSubmitted;

    void Awake()
    {
        playerSelectionDialogUI = UIElement.Instantiate<UIToggleListDialog>(this.transform, "UIPlayerSelection");
        playerSelectionDialogUI.Setup(
            "Network Player Selection",
            "Challenge a Player",
            "Challenge Player",
            OnPlayerSelectionClose,
            OnPlayerSelected,
            OnSelectionSubmitted
            );

        isPlayingSprite = Resources.Load<Sprite>("Images/Icons/gamepad");
    }

    public void SetDiscoveryService(NetworkDiscoveryService discoveryService)
    {
        //unregister from old service
        if (null != this.discoveryService)
            this.discoveryService.RemoveNetworkChangedHandler(this);

        this.discoveryService = discoveryService;

        if (null == discoveryService)
        {
            this.discoveryService = null;
            this.availablePlayers = null;
            this.selectedPlayer = null;
            return;
        }

        //register on new service for network updates
        this.discoveryService = discoveryService;
        this.discoveryService.RegisterNetworkChangedHandler(this);
        this.availablePlayers = this.discoveryService.GetDiscoveredClients();
    }

    public void Show(int listenPort)
    {
        discoveryService.StartListening(listenPort);
        UpdatePlayerSelectionDialogUI();
        playerSelectionDialogUI.Show();
    }

    public void AddOnSubmittedHandler(UnityAction<NetworkClientInfo> onSelectionSubmittedHandler)
    {
        if (null != onSelectionSubmittedHandler)
            onSelectionSubmitted += onSelectionSubmittedHandler;
    }

    public void ClearOnSelectionSubmittedHandlers()
    {
        onSelectionSubmitted = null;
    }

    private void UpdatePlayerSelectionDialogUI()
    {
        //deselect
        if (null != selectedPlayer && selectedPlayer.IsPlaying())
            selectedPlayer = null;

        //remove old toggle entries
        playerSelectionDialogUI.ClearToggles();

        //create new toggle entries
        List<DictionaryEntry> namesToIsSelectable = GetNamesToIsSelectableOfAvailablePlayers();
        int selectedOpponendIndex = GetIndexOfSelectedPlayer();
        for(int i=0; i < namesToIsSelectable.Count; i++){
            string playerName = (string)namesToIsSelectable[i].Key;
            bool isSelectable = (bool)namesToIsSelectable[i].Value;
            playerSelectionDialogUI.AddToggle(playerName, isSelectable, isSelectable ? null : isPlayingSprite, i == selectedOpponendIndex);
        }
    }

    //triggered when a toggle of the dialog is clicked
    private void OnPlayerSelected(int index)
    {
        selectedPlayer = availablePlayers[index];
    }

    //triggered when the submit button of the dialog is clicked
    private void OnSelectionSubmitted(int index)
    {
        playerSelectionDialogUI.Hide();
        discoveryService.StopListening();

        selectedPlayer = availablePlayers[index];
        if (null != onSelectionSubmitted)
            onSelectionSubmitted.Invoke(selectedPlayer);
    }

    private void OnPlayerSelectionClose()
    {
        selectedPlayer = null;
        discoveryService.StopListening();
        playerSelectionDialogUI.Hide();
    }

    public void OnClientJoined(NetworkClientInfo client)
    {
        UpdateAvailablePlayers();
    }

    public void OnClientUpdated(NetworkClientInfo client)
    {
        UpdateAvailablePlayers();
    }

    public void OnClientLeft(NetworkClientInfo client)
    {
        //deselect selected client, if it was him that left
        if (null != selectedPlayer && !discoveryService.GetDiscoveredClients().Contains(selectedPlayer))
            selectedPlayer = null;

        UpdateAvailablePlayers();
    }

    private void UpdateAvailablePlayers()
    {
        if (null == discoveryService)
            return;

        availablePlayers = discoveryService.GetDiscoveredClients();
        UpdatePlayerSelectionDialogUI();
    }

    private List<DictionaryEntry> GetNamesToIsSelectableOfAvailablePlayers()
    {
        List<DictionaryEntry> playerNamesToIsPlaying = new List<DictionaryEntry>();
        if (null == availablePlayers || availablePlayers.Count <= 0)
            return playerNamesToIsPlaying;

        foreach (NetworkClientInfo clientInfo in availablePlayers){
            string name = clientInfo.GetPlayerName();
            bool isPlaying = clientInfo.IsPlaying();
            playerNamesToIsPlaying.Add(new DictionaryEntry(name, !isPlaying));
        }

        return playerNamesToIsPlaying;
    }

    private int GetIndexOfSelectedPlayer()
    {
        if (null == availablePlayers || availablePlayers.Count <= 0 || null == selectedPlayer)
            return -1;

        return availablePlayers.IndexOf(selectedPlayer);
    }

    void OnDestroy()
    {
        //unregister for network updates
        SetDiscoveryService(null);
    }

    //if any other level is loaded while the selection is open -> close it
    void OnLevelWasLoaded(int levelIndex)
    {
        OnPlayerSelectionClose();
    }
}
