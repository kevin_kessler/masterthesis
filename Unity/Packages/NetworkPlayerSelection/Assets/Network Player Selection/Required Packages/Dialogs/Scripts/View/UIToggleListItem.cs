﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Utils;
using System.Linq;

public class UIToggleListItem : UIElement
{
    [SerializeField]
    private ToggleImage toggleImage;

    [SerializeField]
    private ToggleText toggleText;

    [SerializeField]
    private Toggle toggle;

    public Toggle GetToggle()
    {
        return toggle;
    }

    public void SetItemImage(Sprite sprite)
    {
        toggleImage.GetComponent<Image>().sprite = sprite;

        if (null == sprite)
            HideItemImage();
        else
            ShowItemImage();
    }

    public void SetText(string text)
    {
        toggleText.GetComponent<Text>().text = text;
    }
    
    public void SetGroup(ToggleGroup group){
        toggle.group = group;
    }

    public void SetInteractable(bool interactable)
    {
        toggle.interactable = interactable;
    }

    public void AddOnValueChangedHandler(UnityAction<bool> handler)
    {
        toggle.onValueChanged.AddListener(handler);
    }

    public void ShowItemImage()
    {
        toggleImage.GetComponent<Image>().gameObject.SetActive(true);
    }

    public void HideItemImage()
    {
        toggleImage.GetComponent<Image>().gameObject.SetActive(false);
    }
}
