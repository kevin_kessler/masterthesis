﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(Image))]
public class ToggleImage : MonoBehaviour {
        
    [SerializeField] private Color activeColor;
    [SerializeField] private Color inactiveColor;
    [SerializeField] private Toggle toggle;

    private Image imgToChange;

    void Start()
    {
        imgToChange = GetComponent<Image>();
        toggle.onValueChanged.AddListener(SwitchImageColor);
        SwitchImageColor(toggle.isOn);
    }

    public void SwitchImageColor(bool active)
    {
        if (null != imgToChange)
            imgToChange.color = active ? activeColor : inactiveColor;
    }
}
