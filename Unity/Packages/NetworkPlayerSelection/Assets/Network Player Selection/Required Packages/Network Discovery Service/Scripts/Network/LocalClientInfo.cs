﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;

//doesn't have IP since the local client could be connected to several networks and/or could be natted
public class LocalClientInfo {
    private UnityAction onModelChangedHandler;
    public event UnityAction OnModelChanged
    {
        add
        {
            onModelChangedHandler += value;
        }
        remove
        {
            onModelChangedHandler -= value;
        }
    }

    protected string appName;
    protected string id;
    protected string playerName;
    protected bool isPlaying;

    public LocalClientInfo(string appName, string id, string playerName, bool isPlaying)
    {
        this.id = id;
        SetPlayerName(playerName);
        SetIsPlaying(isPlaying);
        SetAppName(appName);
    }

    public void SetPlayerName(string playerName)
    {
        if (null!=this.playerName && this.playerName.Equals(playerName))
            return;

        this.playerName = playerName;
        NotifyModelChangedHandlers();
    }

    public void SetIsPlaying(bool isPlaying)
    {
        if (this.isPlaying == isPlaying)
            return;

        this.isPlaying = isPlaying;
        NotifyModelChangedHandlers();
    }

    public void SetAppName(string appName)
    {
        if (null != this.appName && this.appName.Equals(appName))
            return;

        this.appName = appName;
        NotifyModelChangedHandlers();
    }

    public string GetID()
    {
        return id;
    }

    public string GetPlayerName()
    {
        return playerName;
    }

    public string GetAppName()
    {
        return appName;
    }

    public bool IsPlaying()
    {
        return isPlaying;
    }

    protected void NotifyModelChangedHandlers()
    {
        if (null == onModelChangedHandler)
            return;

        onModelChangedHandler.Invoke();
    }
}
