﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof (CanvasGroup))]
public class UIImageFlasher : UICanvasGroupFlasher {

    public Image imageOfCanvasGroupToBeFlashed;

    public void SetKeepAspectRatio(bool keep)
    {
        imageOfCanvasGroupToBeFlashed.preserveAspect = keep;
    }

    public void SetImage(Sprite sprite){
        imageOfCanvasGroupToBeFlashed.sprite = sprite;
    }

    public IEnumerator FlashRoutine(Color imageColor)
    {
        Color tmp = imageOfCanvasGroupToBeFlashed.color;
        imageOfCanvasGroupToBeFlashed.color = imageColor;
        yield return StartCoroutine(base.Flash());
        imageOfCanvasGroupToBeFlashed.color = tmp;
    }

    public void Flash(Color imageColor)
    {
        StartCoroutine(FlashRoutine(imageColor));
    }
}
