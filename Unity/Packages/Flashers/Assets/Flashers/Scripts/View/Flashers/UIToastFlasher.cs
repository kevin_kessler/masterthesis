﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof (CanvasGroup))]
public class UIToastFlasher : UICanvasGroupFlasher {

    public static void CreateAndDisplayToast(MonoBehaviour coRoutineServer, string toastMessage, Color textColor, Color bgColor)
    {
        if (null == coRoutineServer)
            return;

        coRoutineServer.StartCoroutine(CreateAndDisplayToastRoutine(coRoutineServer, toastMessage, textColor, bgColor));
    }

    private static IEnumerator CreateAndDisplayToastRoutine(MonoBehaviour coRoutineServer, string toastMessage, Color textColor, Color bgColor)
    {
        UIToastFlasher toaster = UIElement.Instantiate<UIToastFlasher>();
        DontDestroyOnLoad(toaster); //also show message while scene is being changed
        yield return coRoutineServer.StartCoroutine(toaster.FlashRoutine(toastMessage, textColor, bgColor));
        Object.Destroy(toaster.gameObject); //destroy message after being shown
    }

    public Text textOfCanvasGroupToBeFlashed;
    public Image imageOfCanvasGroupToBeFlashed;

    public void SetFontSize(int fontSize)
    {
        textOfCanvasGroupToBeFlashed.fontSize = fontSize;
        textOfCanvasGroupToBeFlashed.resizeTextMaxSize = fontSize;
    }

    private IEnumerator FlashRoutine(Color textColor, Color backgroundColor)
    {
        Color tmp1 = textOfCanvasGroupToBeFlashed.color;
        Color tmp2 = imageOfCanvasGroupToBeFlashed.color;
        textOfCanvasGroupToBeFlashed.color = textColor;
        imageOfCanvasGroupToBeFlashed.color = backgroundColor;
        yield return StartCoroutine(base.Flash());
        textOfCanvasGroupToBeFlashed.color = tmp1;
        imageOfCanvasGroupToBeFlashed.color = tmp2;
    }

    public IEnumerator FlashRoutine(string textToFlash, Color textColor, Color backgroundColor)
    {
        string tmp = textOfCanvasGroupToBeFlashed.text;
        textOfCanvasGroupToBeFlashed.text = textToFlash;
        yield return StartCoroutine(FlashRoutine(textColor, backgroundColor));
        textOfCanvasGroupToBeFlashed.text = tmp;
    }

    public IEnumerator FlashRoutine(string textToFlash)
    {
        string tmp = textOfCanvasGroupToBeFlashed.text;
        textOfCanvasGroupToBeFlashed.text = textToFlash;
        yield return StartCoroutine(base.Flash());
        textOfCanvasGroupToBeFlashed.text = tmp;
    }


    public void Flash(string textToFlash, Color textColor, Color backgroundColor)
    {
        StartCoroutine(FlashRoutine(textToFlash, textColor, backgroundColor));
    }

    public void Flash(string textToFlash)
    {
        StartCoroutine(FlashRoutine(textToFlash));
    }
}
