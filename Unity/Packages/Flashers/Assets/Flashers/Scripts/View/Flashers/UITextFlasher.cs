﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof (CanvasGroup))]
public class UITextFlasher : UICanvasGroupFlasher {

    [SerializeField] private Text textOfCanvasGroupToBeFlashed;
    [SerializeField] private Outline strokeOfCanvasGroupToBeFlashed;

    public void SetFontSize(int fontSize)
    {
        textOfCanvasGroupToBeFlashed.fontSize = fontSize;
    }

    private IEnumerator FlashRoutine(Color textColor, Color strokeColor)
    {
        Color tmp = textOfCanvasGroupToBeFlashed.color;
        textOfCanvasGroupToBeFlashed.color = textColor;
        strokeOfCanvasGroupToBeFlashed.effectColor = strokeColor;
        yield return StartCoroutine(base.Flash());
        textOfCanvasGroupToBeFlashed.color = tmp;
    }

    public IEnumerator FlashRoutine(string textToFlash, Color textColor)
    {
        string tmp = textOfCanvasGroupToBeFlashed.text;
        textOfCanvasGroupToBeFlashed.text = textToFlash;
        yield return StartCoroutine(FlashRoutine(textColor, Color.clear));
        textOfCanvasGroupToBeFlashed.text = tmp;
    }

    public IEnumerator FlashRoutine(string textToFlash, Color textColor, Color strokeColor)
    {
        string tmp = textOfCanvasGroupToBeFlashed.text;
        textOfCanvasGroupToBeFlashed.text = textToFlash;
        yield return StartCoroutine(FlashRoutine(textColor, strokeColor));
        textOfCanvasGroupToBeFlashed.text = tmp;
    }

    public IEnumerator FlashRoutine(string textToFlash)
    {
        string tmp = textOfCanvasGroupToBeFlashed.text;
        textOfCanvasGroupToBeFlashed.text = textToFlash;
        yield return StartCoroutine(FlashRoutine(textOfCanvasGroupToBeFlashed.color, strokeOfCanvasGroupToBeFlashed.effectColor));
        textOfCanvasGroupToBeFlashed.text = tmp;
    }

    public void Flash(string textToFlash, Color textColor, Color strokeColor){
        StartCoroutine(FlashRoutine(textToFlash, textColor, strokeColor));
    }

    public void Flash(string textToFlash, Color textColor)
    {
        StartCoroutine(FlashRoutine(textToFlash, textColor));
    }

    public void Flash(string textToFlash)
    {
        StartCoroutine(FlashRoutine(textToFlash));
    }
}
