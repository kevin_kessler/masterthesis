﻿using UnityEngine;
using System.Collections;

public class Usage : MonoBehaviour {

	
	void Start () {
        //flash image in red
        UIImageFlasher imageFlasher = InitImageFlasher();
        imageFlasher.Flash(Color.red);

        //flash text message with blue text and red stroke
        UITextFlasher textFlasher = InitTextFlasher();
        //textFlasher.Flash("This is a Text Flasher", Color.blue, Color.red);

        //flash a toast message with blue text and yellow background
        UIToastFlasher toastFlasher = InitToastFlasher();
        //toastFlasher.Flash("This is a Toast Flasher", Color.blue, Color.yellow);
	}

    UIImageFlasher InitImageFlasher()
    {
        UIImageFlasher flasher = UIElement.Instantiate<UIImageFlasher>();
        flasher.SetFadeSpeed(0.5f);
        flasher.SetStayDuration(1);
        flasher.SetImage(Resources.Load<Sprite>("Images/face"));
        flasher.SetKeepAspectRatio(true);
        return flasher;
    }

    UITextFlasher InitTextFlasher()
    {
        UITextFlasher flasher = UIElement.Instantiate<UITextFlasher>();
        flasher.SetFadeSpeed(2f);
        flasher.SetStayDuration(1);
        flasher.SetFontSize(50);
        return flasher;
    }

    UIToastFlasher InitToastFlasher()
    {
        UIToastFlasher flasher = UIElement.Instantiate<UIToastFlasher>();
        flasher.SetFadeSpeed(2f);
        flasher.SetStayDuration(1);
        flasher.SetFontSize(50);
        return flasher;
    }
}
