﻿using UnityEngine;
using System.Collections;

public class Usage : MonoBehaviour {

    [SerializeField] private GameObject timeSliderHolder;
    [SerializeField] private GameObject countSliderHolder;
    [SerializeField] private GameObject progressSliderHolder;

	void Start () {
        UITimeSlider timeSlider = InitTimeSlider();
        timeSlider.Continue();
        timeSlider.AddTime(5);

        UICountSlider countSlider = InitCountSlider();
        StartCoroutine(DummyCounter(countSlider));

        UIProgressSlider progressSlider = InitProgressSlider();
        StartCoroutine(DummyProgressConsumer(progressSlider));
	}

    UITimeSlider InitTimeSlider(){
        UITimeSlider slider = UIElement.Instantiate<UITimeSlider>(timeSliderHolder.transform);
        slider.SetPrependingText("Time: ");
        slider.SetAppendingText("s");
        slider.OnFinished += OnSliderFinished;
        slider.Reset(30);
        slider.Pause();
        return slider;
    }

    UICountSlider InitCountSlider()
    {
        UICountSlider slider = UIElement.Instantiate<UICountSlider>(countSliderHolder.transform);
        slider.SetPrependingText("Trials: ");
        slider.SetAppendingText(" / 20");
        slider.Reset(20);
        slider.OnFinished += OnSliderFinished;
        return slider;
    }

    UIProgressSlider InitProgressSlider()
    {
        UIProgressSlider slider = UIElement.Instantiate<UIProgressSlider>(progressSliderHolder.transform);
        slider.SetPrependingText("Progress: ");
        slider.SetAppendingText("%");
        slider.OnFinished += OnSliderFinished;
        return slider;
    }

    void OnSliderFinished()
    {
        Debug.Log("Slider Finished!");
    }

    IEnumerator DummyCounter(UICountSlider slider)
    {
        while (slider.GetCurrentValue() > 0)
        {
            slider.Decrement();
            yield return new WaitForSeconds(1);
        }
    }

    IEnumerator DummyProgressConsumer(UIProgressSlider slider)
    {
        float curProgress = 0;
        while (curProgress < 1){
            curProgress += 0.05f;
            slider.UpdateProgress(curProgress);
            yield return new WaitForSeconds(0.5f);
        }
    }
}
