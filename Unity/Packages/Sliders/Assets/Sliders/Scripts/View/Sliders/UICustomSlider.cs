﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public abstract class UICustomSlider : UIElement
{

    [SerializeField] protected Slider slider;
    [SerializeField] protected Text valueText;
    [SerializeField] protected string appendingText = "";
    [SerializeField] protected string prependingText = "";

    [SerializeField] protected bool adjustColorAlongWithValue;
    [SerializeField] protected Image fillImage;
    [SerializeField] protected Color minColor = Color.red;
    [SerializeField] protected Color maxColor = Color.green;

    [SerializeField] protected float currentValue;

    protected float maxValue;
    protected bool finished;

    protected UnityAction onFinishedHandler;
    public event UnityAction OnFinished{
        add{ onFinishedHandler += value; }
        remove{ onFinishedHandler -= value; }
    }

    protected virtual void Start()
    {
        UpdateSlider();
    }

    public float GetCurrentValue()
    {
        return currentValue;
    }

    public virtual void Reset()
    {
        finished = false;
        currentValue = maxValue;
        UpdateSlider();
    }

    public bool IsFinished()
    {
        return finished;
    }

    public virtual void Reset(float newMaxValue)
    {
        maxValue = newMaxValue;
        Reset();
    }

    protected float GetValueRatio()
    {
        return (maxValue <= 0) ? 1 : currentValue / (float)maxValue;
    }

    virtual protected void UpdateText()
    {
        //update text
        valueText.text = string.Format("{0}{1}{2}", prependingText, currentValue, appendingText);
    }

    private void UpdateSliderValue()
    {
        float ratio = GetValueRatio();

        //show at least a tiny fraction of the bar
        if (ratio <= 0)
            ratio = 0.001f;

        //update slider value
        slider.value = ratio;
    }

    protected void UpdateColor()
    {
        fillImage.color = Color.Lerp(minColor, maxColor, GetValueRatio());
    }

    public void UpdateSlider()
    {
        UpdateSliderValue();
        UpdateText();

        if (adjustColorAlongWithValue)
            UpdateColor();
    }

    protected void NotifyAboutSliderFinished()
    {
        onFinishedHandler.Invoke();
    }

    public void SetPrependingText(string text)
    {
        prependingText = text;
    }
    public void SetAppendingText(string text)
    {
        appendingText = text;
    }
}
