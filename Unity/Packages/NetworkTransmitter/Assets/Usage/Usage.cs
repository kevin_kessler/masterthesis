﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using Utils;

[RequireComponent(typeof(NetworkTransmitter))]
public class Usage : NetworkBehaviour {

    bool initialised = false;
    bool sending = false;
    UILoadingIndicator loadingIndicator;
    NetworkTransmitter transmitter;
    int transmissionId = 0;
    Texture2D imageToTransfer;

    public override void OnStartServer()
    {
        Debug.Log("OnStartServer");
        transmitter = GetComponent<NetworkTransmitter>();
        transmitter.OnDataFragmentSent += (id, bytes) => {
            Debug.Log("Data fragment sent ...");
        };
        transmitter.OnDataComepletelySent += (id, bytes) => {
            sending = false;
            Debug.Log("Transmission ended ...");
        };

        initialised = true;
    }

    public override void OnStartClient()
    {
        Debug.Log("OnStartClient");
        loadingIndicator = InitLoadingIndicator();
        loadingIndicator.Hide();

        transmitter = GetComponent<NetworkTransmitter>();
        transmitter.OnDataFragmentReceived += (id, bytes) => {
            imageToTransfer = null;
            loadingIndicator.Show();

            float progress = transmitter.GetProgressOfTransmission(id);
            loadingIndicator.UpdateProgress(progress);
        };

        transmitter.OnDataCompletelyReceived += (id, bytes) => {
            Debug.Log("Completely received data.");
            imageToTransfer = Utils.AssetUtils.LoadTextureFromBytes(bytes);
            loadingIndicator.Hide();
        };

        initialised = true;
    }

    [Server]
    void SendData()
    {
        string fileDir = Application.streamingAssetsPath+"/";
        byte[] data = Utils.AssetUtils.LoadBytesFromFile(fileDir, "image.jpg");
        //byte[] data = Utils.AssetUtils.LoadBytesFromFile(fileDir, "big_image.jpg");
        transmitter.SendBytesToClients(transmissionId, data);
        sending = true;
        Debug.Log("Transmission started ...");
    }
    
    [Client]
    UILoadingIndicator InitLoadingIndicator()
    {
        UILoadingIndicator loader = UIElement.Instantiate<UILoadingIndicator>();
        loader.SetIndicationText("Transmitting data...");
        loader.SetShowProgressBar(true);
        loader.SetAppendingProgressBarText("%");
        loader.SetIsCancelAble(false);
        return loader;
    }

    void OnGUI() 
    {
        if (null != imageToTransfer){
            Rect screen = new Rect(0,0, Screen.width, Screen.height);
            GUI.DrawTexture(screen, imageToTransfer);
        }

        if(!initialised || sending || !isServer)
            return;

        Rect sendRect = new Rect(Screen.width - 300, Screen.height-100, 300, 100);
        if (GUI.Button(sendRect, "Start Transmission"))
        {
            SendData();
        }
    }
}
