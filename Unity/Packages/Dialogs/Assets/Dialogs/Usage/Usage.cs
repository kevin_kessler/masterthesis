﻿using UnityEngine;
using System.Collections;

public class Usage : MonoBehaviour {

	void Start () 
    {
        UIDialog dialog;
        dialog = InitDecisionDialog();
        dialog = InitImageDialog();
        dialog = InitInfoDialog();
        dialog = InitPromptDialog();
        dialog = InitToggleListDialog();

        //dialog.AddOnCloseAction(dialog.Hide);
        //dialog.Show();

        //Video dialog
        UIVideoDialog vdialog = InitVideoDialog();
        vdialog.PlayVideo("Video Dialog", "sample_video.ogv", OnVideoFinished);
	}

    private void OnCloseCallback() { Debug.Log("Closed"); }
    private void OnAcceptCallback() { Debug.Log("Accepted"); }
    private void OnAcceptCallback(string value) { Debug.Log("Accepted with value:" + value); }
    private void OnAcceptCallback(int value) { Debug.Log("Accepted with value:" + value); }

    private void OnVideoFinished() { Debug.Log("Video Finished!"); }

    private UIDecisionDialog InitDecisionDialog()
    {
        UIDecisionDialog dialog = UIElement.Instantiate<UIDecisionDialog>();
        dialog.SetDialogTitle("Decision Dialog");
        dialog.SetInfoText("You have been invited. Do you accept?");
        dialog.SetInfoIcon(Resources.Load<Sprite>("Images/Icons/info"));
        dialog.AddOnCloseAction(OnCloseCallback);
        dialog.AddOnAcceptAction(OnAcceptCallback);
        return dialog;
    }

    private UIImageDialog InitImageDialog()
    {
        UIImageDialog dialog = UIElement.Instantiate<UIImageDialog>();
        dialog.SetDialogTitle("Image Dialog");
        dialog.SetImage(Resources.Load<Sprite>("Images/book"));
        dialog.AddOnCloseAction(OnCloseCallback);
        return dialog;
    }

    private UIInfoDialog InitInfoDialog()
    {
        UIInfoDialog dialog = UIElement.Instantiate<UIInfoDialog>();
        dialog.SetDialogTitle("Info Dialog");
        dialog.SetInfoText("This is some info text");
        dialog.SetInfoIcon(Resources.Load<Sprite>("Images/Icons/info"));
        dialog.AddOnCloseAction(OnCloseCallback);
        return dialog;
    }

    private UIPromptDialog InitPromptDialog()
    {
        UIPromptDialog dialog = UIElement.Instantiate<UIPromptDialog>();
        dialog.SetDialogTitle("Prompt Dialog");
        dialog.SetInfoText("Enter your name");
        dialog.SetPlaceholderText("Name...");
        dialog.SetInfoIcon(Resources.Load<Sprite>("Images/Icons/info"));
        dialog.AddOnCloseAction(OnCloseCallback);
        dialog.AddOnAcceptAction(OnAcceptCallback);
        return dialog;
    }

    private UIToggleListDialog InitToggleListDialog()
    {
        UIToggleListDialog dialog = UIElement.Instantiate<UIToggleListDialog>();
        dialog.SetDialogTitle("Toggle List Dialog");
        dialog.SetListTitle("Select an entry...");

        dialog.AddToggle("Entry 0", true, Resources.Load<Sprite>("Images/Icons/info"), true);
        dialog.AddToggle("Entry 1", true, Resources.Load<Sprite>("Images/Icons/cancel"), false);
        dialog.AddToggle("Entry 2", true, Resources.Load<Sprite>("Images/Icons/gear"), false);
        dialog.AddToggle("Entry 3", true, Resources.Load<Sprite>("Images/Icons/ok"), false);
        dialog.AddToggle("Entry 3", true, Resources.Load<Sprite>("Images/Icons/gamepad"), false);

        dialog.AddOnCloseAction(OnCloseCallback);
        dialog.AddOnSubmitAction(OnAcceptCallback);
        return dialog;
    }

    private UIVideoDialog InitVideoDialog()
    {
        UIVideoDialog dialog = UIElement.Instantiate<UIVideoDialog>();
        return dialog;
    }
}
