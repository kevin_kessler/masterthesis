﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections;
using Utils;

public class UIToggleListDialog : UIDialog
{
    [SerializeField] private Button submitButton;
    [SerializeField] private Text submitButtonText;
    [SerializeField] private GameObject listHolder;
    [SerializeField] private GameObject toggleListPrefab;
    private UISimpleToggleListGroup toggleList;

    private event UnityAction<int> onSubmitHandler;

    protected override void Awake()
    {
        base.Awake();
        InitToggleList();
        submitButton.onClick.AddListener(HandleOnSubmit);
    }

    protected void Update()
    {
        submitButton.interactable = toggleList.GetSelectedIndex() != -1;

        if (!allowKeyboardInput || !IsShowing())
            return;

        KeyCode pressedKey = InputUtils.GetPressedKey();
        switch (pressedKey)
        {
            case KeyCode.Return:
            case KeyCode.KeypadEnter:
                if (submitButton.interactable)
                    HandleOnSubmit();
                break;
            case KeyCode.Escape:
                ExecuteOnCloseActions();
                break;
        }
    }

    public void AddToggle(string toggleName, bool isInteractable = true, Sprite itemImage = null, bool isSelected = false)
    {
        Toggle t = toggleList.CreateAndAddToggle(toggleName, isInteractable, itemImage);
        t.isOn = isSelected;
    }

    public void ClearToggles()
    {
        toggleList.Clear();
    }

    public void SetListTitle(string listTitle)
    {
        toggleList.SetListTitle(listTitle);
    }

    private void InitToggleList()
    {
        toggleList = Instantiate(toggleListPrefab).GetComponent<UISimpleToggleListGroup>();
        DebugUtils.Assert(null != toggleList, DebugUtils.GenerateComponentMissingMsg(() => toggleListPrefab, typeof(UISimpleToggleListGroup)));
        toggleList.transform.SetParent(listHolder.transform, false);
    }

    public void SetSubmitButtonText(string text)
    {
        submitButtonText.text = text;
    }

    public virtual void ClearOnSubmitActions()
    {
        onSubmitHandler = null;
    }

    //the submitcallback will have the index of the toggle item that was selected on submit
    public virtual void AddOnSubmitAction(UnityAction<int> onSubmitCallback)
    {
        if(null!=onSubmitCallback)
            onSubmitHandler += onSubmitCallback;
    }

    public void ClearOnToggleSelectActions()
    {
        toggleList.ClearOnToggleSelectActions();
    }

    //the onToggleSelectCallback will have the index of the toggle item that was selected on submit
    public void AddOnToggleSelectAction(UnityAction<int> onToggleSelectCallback)
    {
        if(null!=onToggleSelectCallback)
            toggleList.AddOnToggleSelectAction(onToggleSelectCallback);
    }

    private void HandleOnSubmit()
    {
        if (null != onSubmitHandler)
        {
            int selectedIndex = toggleList.GetSelectedIndex();
            onSubmitHandler.Invoke(selectedIndex);
        }
    }

    public void Setup(string dialogTitle, string listTitle, string submitText, UnityAction onDialogClose, UnityAction<int> onToggle, UnityAction<int> onSubmit)
    {
        SetDialogTitle(dialogTitle);
        SetListTitle(listTitle);
        SetSubmitButtonText(submitText);

        ClearOnCloseActions();
        AddOnCloseAction(onDialogClose);

        ClearOnToggleSelectActions();
        AddOnToggleSelectAction(onToggle);

        ClearOnSubmitActions();
        AddOnSubmitAction(onSubmit);
    }
}
  
