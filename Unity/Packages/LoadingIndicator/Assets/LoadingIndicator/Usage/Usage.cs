﻿using UnityEngine;
using System.Collections;

public class Usage : MonoBehaviour {

	void Start () {
        UILoadingIndicator loader = UIElement.Instantiate<UILoadingIndicator>();
        loader.SetIndicationText("Loading game world... Please wait");
        loader.SetIsCancelAble(true);
        loader.AddOnCancelAction(OnCancel);
        loader.SetShowProgressBar(true);
        loader.SetAppendingProgressBarText("%");
        loader.AddOnProgressCompleteAction(OnComplete);
        loader.AddOnProgressCompleteAction(loader.Hide);
        loader.Show();

        StartCoroutine(DummyConsumer(loader));
    }

    void OnCancel(){
        Debug.Log("Loading Canceled");
    }

    void OnComplete(){
        Debug.Log("Loading Complete");
    }

    IEnumerator DummyConsumer(UILoadingIndicator loader)
    {
        float curProgress = 0;
        while (curProgress < 1)
        {
            curProgress += 0.05f;
            loader.UpdateProgress(curProgress);
            yield return new WaitForSeconds(0.5f);
        }
    }
}
