﻿using UnityEngine;
using System.Collections;

public class Usage : MonoBehaviour {

    [SerializeField]
    private GameObject keypadHolder;

	void Start () {
        //instantiate keypad and set it as child of keypadHolder
        UIKeypad keypad = UIElement.Instantiate<UIKeypad>(keypadHolder.transform);

        //assign submit handlers
        keypad.AddOnSubmitAction(OnSubmit);
        keypad.AddOnSubmitAction(keypad.ClearDigits);
	}

    void OnSubmit(string submittedValue){
        Debug.Log("Submitted:" + submittedValue);
    }
}
