﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System.Collections.Generic;
using Utils;
using System;

public class UIKeypad : UIElement
{
    [SerializeField] private InputField inputField;
    [SerializeField] private Button submitButton;
    [SerializeField] private bool allowKeyboardInput = true;

    private event UnityAction onAnyKeyPressedHandler;
    private event UnityAction<string> onSubmitHandler;

    void Start()
    {
        submitButton.onClick.AddListener(OnSubmit);
        this.inputField.contentType = InputField.ContentType.IntegerNumber;
        this.inputField.interactable = false;
    }

    public void AddOnAnyKeyPressedAction(UnityAction onAnyKeyPressedAction)
    {
        if (null == onAnyKeyPressedAction)
            return;

        onAnyKeyPressedHandler += onAnyKeyPressedAction;
    }

    public void AddOnSubmitAction(UnityAction<string> onSubmitAction)
    {
        if (null == onSubmitAction)
            return;

        onSubmitHandler += onSubmitAction;
    }

    public void AddOnSubmitAction(UnityAction onSubmitAction)
    {
        if (null == onSubmitAction)
            return;

        onSubmitHandler += (value) => { onSubmitAction(); };
    }

    public void AppendDigit(string number)
    {
        
        inputField.text += number;
        OnAnyKeyPress();
    }

    public void ClearDigits()
    {
        inputField.text = "";
        OnAnyKeyPress();
    }

    public void RemoveDigit()
    {
        if (inputField.text.Length >= 1)
            inputField.text = inputField.text.Remove(inputField.text.Length - 1);
        OnAnyKeyPress();
    }

    private void OnAnyKeyPress()
    {
        if(null != onAnyKeyPressedHandler)
            onAnyKeyPressedHandler();
    }

    private void OnSubmit()
    {
        if (null != onSubmitHandler && inputField.text.Length > 0)
            onSubmitHandler(inputField.text);
    }

    void Update()
    {
        if (!allowKeyboardInput)
            return;

        KeyCode pressedKey = InputUtils.GetPressedKey();
        switch (pressedKey)
        {
            case KeyCode.Keypad0:
            case KeyCode.Alpha0:
                AppendDigit("0");
                break;
            case KeyCode.Keypad1:
            case KeyCode.Alpha1:
                AppendDigit("1");
                break;
            case KeyCode.Keypad2:
            case KeyCode.Alpha2:
                AppendDigit("2");
                break;
            case KeyCode.Keypad3:
            case KeyCode.Alpha3:
                AppendDigit("3");
                break;
            case KeyCode.Keypad4:
            case KeyCode.Alpha4:
                AppendDigit("4");
                break;
            case KeyCode.Keypad5:
            case KeyCode.Alpha5:
                AppendDigit("5");
                break;
            case KeyCode.Keypad6:
            case KeyCode.Alpha6:
                AppendDigit("6");
                break;
            case KeyCode.Keypad7:
            case KeyCode.Alpha7:
                AppendDigit("7");
                break;
            case KeyCode.Keypad8:
            case KeyCode.Alpha8:
                AppendDigit("8");
                break;
            case KeyCode.Keypad9:
            case KeyCode.Alpha9:
                AppendDigit("9");
                break;
            case KeyCode.Delete:
            case KeyCode.Backspace:
                RemoveDigit();
                break;
            case KeyCode.KeypadEnter:
            case KeyCode.Return:
                OnSubmit();
                break;

            default: break;
        }
    }
}
