\chapter{XML Structure}
\label{appendix:xml}

\lstset{language=XML,
	%keyword coloring
	classoffset=0,
	morekeywords={width, enabled, correctIndex},
	keywordstyle=\color{commentcolor},
	%type coloring
	classoffset=1,
	morekeywords={TheoryBook, Page, Heading, Text, Image, Caption, Quiz, QuestionsPerMatch, ShuffleQuestions, DefaultSolveDuration, Question, ImageName, Answers, Zeroth, First, Second, Third, SolveDuration},
	keywordstyle=\color{typecolor}
}

As explained in chapter \ref{c:sgframework}, the introduced framework comes with some exogenous black-box game modes that can be filled by custom content in form of XML.
To give an overview of the syntax and structure of the different XML files, this chapter provides examples for a theory and a quiz XML. 
It is important to note that custom XML files have to be UTF-8 encoded in order to be interpreted correctly.

\section{Theory XML}
As aforementioned, the theory mode displays the interpreted XML content in form of a virtual book.
Therefore, its XML structure is modelled as shown in listing \ref{lst:appendix_theory}.
\begin{lstlisting}[caption={Example of a XML for the theory mode},label=lst:appendix_theory]
<TheoryBook>
	<Page>
		<Heading>My Heading</Heading>
		<Text>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</Text>
		<Text>Ut labore et dolore magna aliqua. Ut enim ad minim venia.</Text>
	</Page>

	<Page>
		<Image width="0.5">myimage.png</Image>
		<Caption>My Image Caption</Caption>
		<Heading>Another Heading</Heading>
		<Text>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</Text>
	</Page>

	<Page>
		<Heading>Next Heading</Heading>
		<Image width="0.75">myimage.png</Image>
		<Text>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</Text>
	</Page>
</TheoryBook>
\end{lstlisting}

\noindent
The \lstinlined{TheoryBook} node is the document's root.
It serves as container of the book's pages, which are declared as \lstinlined{Page} elements.
Each page can contain an arbitrary combination of any of the supported style elements listed below.
\begin{itemize}
\item \textbf{Heading} - Represents a big heading which is aligned to the left.
\item \textbf{Text} - Defines a simple paragraph which displays the tag's content as normal text.
\item \textbf{Image} - Used to display an image. The tag's content must equal the name of the image file (including the extension). The file must reside in the same directory as the XML.
The size of the image can be adjusted by the tag's \lstinlined{width} attribute.
The given value must be between 0 and 1 and represents the image's width relative to the pages' width.
A width of 0.75, for example, will cause the image to take 75\% of the pages' width.
While scaling, the image's original aspect ratio is kept, which means the image's height is also affected by the given width parameter.
\item \textbf{Caption} - Represents a small centred caption which can be used to label images or paragraphs.
\end{itemize}

\noindent
The pages as well as their inner elements will appear in the same order as specified.
Accordingly, the above given XML example will be displayed as shown in figure \ref{graphic:appendix_theory}.

\begin{figure}[ht!]
	\centering
	\includegraphics[width=0.75\textwidth]{appendix/theory}
	\caption{Theory content produced by XML code of listing \ref{lst:appendix_theory}}
	\label{graphic:appendix_theory}
\end{figure}

\noindent
It is important to note that all text elements (heading, caption, text) have fix font-sizes.
This means that if there is too much text at the same page, it's content will overflow vertically.
The same applies for images. If too many images and / or too much text is on a page, the content overflows.
Accordingly, one has to check how the content is being displayed from time to time while creating it.

\section{Quiz XML}
As aforementioned, the questions for the quiz modes are also defined by XML files.
A quiz contains a pool of questions. 
Settings like the number and order of questions that are going to be displayed in the actual quiz mode can be configured via the XML as shown in listing \ref{lst:appendix_quiz} below.
\pagebreak
\begin{lstlisting}[caption={Example of a XML for the quiz modes},label=lst:appendix_quiz]
<Quiz>
	<QuestionsPerMatch>0</QuestionsPerMatch>
	<ShuffleQuestions enabled="True"/>
	<DefaultSolveDuration>20</DefaultSolveDuration>
	
	<Question>
		<Text>Who is president of the United States?</Text>
		<ImageName></ImageName>
		<Answers correctIndex="0">
			<Zeroth>Barack Obama</Zeroth>
			<First>Abraham Lincoln</First>
			<Second>John F. Kennedy</Second>
			<Third>Angela Merkel</Third>
		</Answers>
		<SolveDuration>10</SolveDuration>
	</Question>
	
	<Question>
		<Text>What does this image show?</Text>
		<ImageName>myimage.png</ImageName>
		<Answers correctIndex="3">
			<Zeroth>Square</Zeroth>
			<First>Rectangle</First>
			<Second>Hexagon</Second>
			<Third>Octagon</Third>
		</Answers>
		<SolveDuration></SolveDuration>
	</Question>
</Quiz>
\end{lstlisting}

\noindent
The \lstinlined{Quiz} node is the document's root and serves as container of the questions.
Besides the questions it also holds the following configuration tags:

\begin{itemize}
\item \textbf{QuestionsPerMatch} - Describes the number of questions that one quiz round / match should contain. This can be left blank or set to 0 to include all questions of the pool. 
If a specific number is chosen, each round will contain a random selection of the questions from the pool. The size of the selection then equals the specified number.
\item \textbf{ShuffleQuestions} - This tag's \lstinlined{enabled} attribute can be set to \lstinlined{True} or \lstinlined{False}. If set to true, the questions will appear in random order each time the quiz mode starts.
If set to false, the questions always appear in the same order. If not specified, this value will be true by default.
\item \textbf{DefaultSolveDuration} - Each question can have an individual amount of time in which it has to be solved before it is counted as wrong. If no individual time is specified, the default duration will apply which can be set within this tag. The entered value represents the given time in seconds. If not specified, this value will be 20 by default.
\end{itemize}

\noindent
Each of the questions contains of a question text, an optional image, four possible answers from which only one can be marked as correct, and an optional individual solve duration.
\begin{itemize}
\item \textbf{Text} - The displayed text of the question.
\item \textbf{ImageName} - Used to add an image to the question. The value should equal the image's filename (including the extension). The image file must reside in the same directory as the XML file. This value is optional and can be left blank. It is worth noting that quiz questions will be distributed over the network in the case of the multiplayer mode. Therefore, the images' file sizes should be reasonable small.
\item \textbf{Answers} - A node that contains four child nodes, one for each possible answer (Zeroth, First, Second, Third). 
The child nodes' names indicate their index which is important for the \lstinlined{correctIndex} attribute. 
This attribute determines which of the four answers is the correct one. Accordingly it must be between 0 and 3.
\item \textbf{SolveDuration} - This is the aforementioned individual solve duration in seconds. If set, the player will have the specified amount of seconds to solve this particular question. This value is optional and can be left blank. In this case, the above described default solve duration kicks in.
\end{itemize}

\noindent
When the quiz mode is loaded, the questions will be read and interpreted from XML.
The above XML example would result in the quiz match illustrated by figure \ref{graphic:appendix_quiz}.

\begin{figure}[ht!]
	\centering
	\includegraphics[width=0.75\textwidth]{appendix/quiz}
	\caption{Quiz content produced by XML code of listing \ref{lst:appendix_quiz}}
	\label{graphic:appendix_quiz}
\end{figure}