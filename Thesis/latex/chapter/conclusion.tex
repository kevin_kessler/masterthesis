\chapter{Conclusion}
\label{c:conclusion}

In this chapter, the results of the thesis are summarised and evaluated in respect to the goals mentioned in \ref{s:intro_goals}.
Furthermore, suggestions for potential future work in the underlying field are given.

\section{Results}

As stated in the introduction, the main goal of this thesis was to simplify and accelerate the development of Serious Games.
To achieve this goal, it has been separated into several sub goals which were subject of discussion in the respective chapters.
Since the sub goals build upon each other, the chapters do so as well.

Before the development process of Serious Games could be analysed, it was necessary to clarify what the term \textit{Serious Game} actually means.
The introduction of the different types and purposes of Serious Games in chapter \ref{c:seriousgames} contributed to the comprehension of the term and their characteristics. 
In order to find out how successful Serious Games can be developed, it was necessary to analyse what good Serious Games are actually made of.
The beginning of chapter \ref{c:sgprocess} served for that purpose by explaining essentials as well as motivational and multiplayer specific aspects of good Serious Games.
Furthermore the chapter provided a general approach for Serious Games development which can be used as a guide on how to proceed when developing.
To identify how the development process of Serious Games can further be improved, common development related problems were discussed at the end of the chapter. 

The discussion yielded that all of the mentioned problems are related to implementation.
Accordingly, by actively supporting this task, the situation was expected to improve.
Hence, chapter \ref{c:sgunity} provided suggestions on best practices that one should follow when developing Serious Games with Unity.
Furthermore, the chapter also introduced several reusable Unity packages which were developed in the course of this thesis.
The packages were developed by applying the aforementioned best practices and by targeting the in chapter \ref{c:sgprocess} discussed characteristics of good Serious Games in order to provide convenient and stable solutions for common Serious Games development tasks.
By giving examples about when and how to use the best practices and packages, the chapter contributed largely to the support of the implementation task.

To even further improve the situation, a framework for Serious Games development which is based on the best practices and the mentioned packages was developed.
Chapter \ref{c:sgframework} was used to discuss the framework in detail. 
It provided a solid overview of the framework's purpose, of how it works, and of how to use it.
Those are the three aspects a framework's documentation should cover (see \cite{framework_entwicklung_paper}) and which are necessary to ease the learning curve for interested developers.
Accordingly, this chapter further contributed to the support of the implementation task.

The evaluation of the framework in chapter \ref{c:evaluation} yielded that it successfully contributes to the solution of all of the problems discussed in chapter \ref{c:sgprocess}.
However, as usual for frameworks, its frame comes with certain limitations that holds developers within a certain range of possibilities such that they have to decide whether their game concept fits into the framework.
In general, however, the framework successfully covers a broad range of common Serious Games development tasks which allows to rapidly develop new multiplayer Serious Games.
The showcase project of chapter \ref{c:sgframework} serves as a good example.
The games included in the showcase satisfy the expectations of the collaborating schools' teachers, who are willing to utilise them during their computer science classes.
Accordingly, the goals of the thesis have been reached.

\section{Future Work}
\label{s:future_work}
As aforementioned, frameworks usually evolve from white-box to black-box frameworks over a long period of time.
In the case of the here presented framework this means that future work could yield more and more concrete game mode implementations which act as black-box parts and thereby could be reused and adjusted for other projects.

Furthermore, there are several aspects the framework could be extended by.
As Pree states in \cite{framework_entwicklung_buch}, at the beginning no framework is perfect which means that they require many iterations until they are complete.
The following list of recommendations can be taken to extend the framework in future iterations.

\begin{itemize}
\item \textbf{Individual User Accounts} - Integration of a system that is capable of storing indivdiual user progress such as scores or statistics. 
This could be achieved by file based savegames, which could be loaded on startup or manually by the user. 
Another possibility would be to offer a web application that is compatible to the framework in order to allow servers to store user specific information.
In that case, users could register and login on the server in order to save and load their progress.

\item \textbf{Unlockable Game Modes} - Integrate a generic solution for unlockable game modes. 
This is a common motivational feature which is described in subsection \ref{ss:what_makes_good_sg_motivation}.
If implemented, it should be combined with individual user accounts to store the users unlocking progress as described above.

\item \textbf{Achievement System} - Integrate a generic solution for an achievement system. 
This feature also boosts motivation and is described in subsection \ref{ss:what_makes_good_sg_motivation}.
If implemented, it should be combined with individual user accounts to store the users unlocked achievements as described above.

\item \textbf{Score System} - Integrate a generic solution for a score system. 
The score system should be realised on a per game mode basis. This means, that game modes which support player scores should have their own list of highscores/leaderboards.
The scores should be stored on a central server that can be queried to show the scores of all participating players (e.g. of the network or the Internet).
For further information see subsection \ref{ss:what_makes_good_sg_motivation}.

\item \textbf{More than two players} - Currently, the match making system only supports two players.
This could be enhanced by allowing players to join or by inviting more players to already opened lobbies (or running games) by extending the match making capabilities (see Peer-to-Peer System in subsection \ref{ss:sgframework_myfw_frame}).

\item \textbf{Non-Lobby Multiplayer Modes} - Currently, multiplayer modes have to use the lobby and match invitation system to establish connections between players. 
Some game modes, however, might benefit from running on a dedicated server that represents a continuous/permanent game world (see Dedicated Server in subsection \ref{ss:what_makes_good_sg_multiplayer}).
In that case, the lobby is not needed and players should immediately be redirected to the running game.
This could be approached by providing a non-lobby multiplayer game mode class and by extending the match making system for those modes in order to allow players to join running games (see above).

\item \textbf{Online Multiplayer} - Since the games developed in the course of this thesis are designed for usage in classrooms, it was a high priority to support match making over LAN.
Because of this, the framework currently only supports local networks. To allow playing over greater distances, support for match making via Internet (e.g. with Unity's Internet Services) should be added in future iterations.

\item \textbf{Game Mode Selection in Lobby} - When players want to change the selected multiplayer mode, they currently have to leave the lobby and re-establish the connection by choosing the game mode they want.  It would be more convenient to offer a possibility to change the current multiplayer mode while staying connected to the lobby (e.g. via a select box).

\item \textbf{Bidirectional Network Transmitter} - Currently the introduced Network Transmitter Package only supports transfers from server to clients. Some use cases, however, might profit from sending larger byte arrays from client to server. Accordingly, this should be added by future work.

\item \textbf{Controls for Video Dialog} - The video dialog component currently supports only simple video playback (start/stop). 
For usability reasons, further controls like pause, volume, mute or a position slider could be added.

\item \textbf{Collaborative Quiz} - In addition to the existing quiz modes, a collaborative multiplayer quiz could be added. 
The idea would be to let players collaboratively answer questions by only proceeding to the next question if they answer the current one in consensus.
That way, cognitive team skills could be fostered in addition to the learning benefits.

\item \textbf{Shuffle Quiz Answers} - To make the quiz modes richer in variety, the answer buttons could be shuffled in random order when a question loads.
\end{itemize}

\noindent
State-of-the-art topics are often subject to frequent change or evolution.
So did it happen with UNET while working on this thesis.
During the past months, UNET's documentation constantly grew and improved.
Furthermore, an official component which allows discovering clients in the local area network has been integrated into UNET in the meantime\footnote{See \url{http://docs.unity3d.com/Manual/UNetDiscovery.html} [February 2016].}.
Hence, it might make sense to replace the introduced Network Discovery Package by the new official component in future development iterations.
