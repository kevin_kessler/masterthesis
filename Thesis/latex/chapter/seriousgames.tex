\chapter{Serious Games}
\label{c:seriousgames}
Games have accompanied mankind since the dawn of time.
They are expressions of play, which is a basic part of the human nature.
Over the time a variety of different game types like sports games, board games or guessing games has evolved.
In this thesis, however, the term \textit{game} refers to video games.

Video games (or digital games) are games that are controlled by a computer and usually serve to entertain the player. 
But not all games are made for pure entertainment purposes. 
The following sections explain how far this applies to \textit{Serious Games}, how they can be classified, what their most common purpose is, and especially how they can be used in an educational context.
\footnote{For reader's convenience, this chapter is partially taken from author's previous work \cite{ba_kk}.}

\section{Definition}
\label{s:sg_definition}
When reading about Serious Games, one finds different definitions about what a serious game actually is. 
Many of those are often based on what the person giving the definition does.
In other words, they often differ depending on the context and on the person who determines them.
For example, the definition given by the game designers David Michael and Sande Chen is too specific: 
\begin{quote} 
\centering 
``A serious game is a game in which education (in its various forms) is the primary goal, rather than entertainment.''\cite{Michael:2005:SGG:1051239}
\end{quote}

\noindent
This definition expects always an educational purpose from a serious game. However, it is easy to find an example which does not match this requirement.
For instance, a game which tries to improve the health of its players' by encouraging them to move, does not necessarily have anything to do with education.
Nevertheless, it still can be considered as serious. Thus, a more general definition for the term is required.

Ben Sawyer, a pioneer and co-founder of important serious game initiatives, defines them as follows:
\begin{quote} 
\centering 
``Any meaningful use of computerized game/game industry resources whose chief mission is not entertainment.''\cite{sg_landscape}
\end{quote}

\noindent
According to his definition Serious Games spread to far more areas than only education.
Consequently he disagrees with the game designers' opinion in that point. 
But more important is the aspect, which both definitions have in common.
They agree that, contrary to usual video games, Serious Games are designed for a primary purpose other than entertainment. 
Furthermore, this is the core statement, which most definitions for Serious Games coincide with.
No matter if designed for areas like defence, politics, health care or education, a serious game can always be denoted as a game for non-entertainment purposes.

The term \textit{Serious Game} itself was coined by Sawyer in 2002 in his paper about the usage of digital games for public policy (see \cite{sawyer_gamebasedlearning}).
By coining the term and co-founding the so called \textit{Serious Games Initiative} as well as the \textit{Games for Health Project}\footnote{\url{http://www.gamesforhealth.org/} [January 2016].}, Sawyer became a recognized leader within the Serious Games community of the United States of America.

\section{Classification}
\label{s:sg_classification}
After reading that Serious Games are not primarily aiming at entertainment, the question of what their main purpose is suggests itself.
The answer is: it depends.
For instance, some of them are intended to teach or train the player in a specific subject, whereas others can be foreseen to improve the health of their players or to spread propaganda. 
Therefore a classification depending on their purpose makes sense.
According to Sawyer and the games researcher Peter Smith, Serious Games can partially be classified by their purpose as follows (see also \cite{sg_taxonomy}):

\begin{itemize}
\item Work
\item Advertisement
\item Education
\item Health
\item Science and Research
\item Training
\item Production
\end{itemize}

\noindent
However, this approach is not specific enough to create a clear definition for the classification of Serious Games.
As an illustration, two of them which belong to the \textit{Games for Training} class, can be compared. 

In the game \textit{Zero Hour: America's Medic}\footnote{\url{http://serious.gameclassification.com/EN/games/17987-Zero-Hour-Americas-Medic/index.html} [January 2016].} 
the players can improve and apply their skills as a medical first responder. 
In a virtual environment which simulates a crash scene, the players have to save the lives of the injured e.g. by bandaging wounds. 
The game \textit{Disney Stars}\footnote{\url{http://serious.gameclassification.com/EN/games/43213-Disney-Stars-The-Virtual-Sell/index.html} [January 2016].} on the other hand,
provides the virtual setting of a travel agency and puts the player into a sales conversation with virtual customers. 
The player's objective is to sell a holiday trip to the client. 

As a result one can see that both of the introduced games pursue the purpose to train their players.
The target audiences, however, are totally different. 
While the first example addresses medics, the second one is only intended for salesmen.
For that reason we also have to specify the market at which a serious game is aimed.
Sawyer distinguishes between the following markets (see also \cite{sg_taxonomy}):

\begin{itemize}
\item Corporate
\item Defence
\item Education
\item Government and Non-Governmental Organisations
\item Healthcare
\item Industry
\item Marketing and Communications
\end{itemize}

\noindent
By combining the different purposes and markets, Sawyer and Smith created the \textit{Taxonomy of Serious Games}, which is shown in figure \ref{graphic:taxonomy}.


\begin{figure}[ht!]
\centering
\includegraphics[width=\textwidth]{seriousgames/Sawyer_SeriousGamesTaxonomy}
\caption{Serious Games Taxonomy by Ben Sawyer and Peter Smith (Source: \cite{sg_taxonomy})}
\label{graphic:taxonomy}
\end{figure}

\noindent
The taxonomy is their attempt to create a clear definition of how to classify Serious Games. According to it the two previously mentioned games can be clearly classified. 
\textit{Zero Hour: America's Medic}, whose market category is \textit{Healthcare} can be considered as \textit{training game for health professionals}, 
whereas \textit{Disney Stars}, which belongs to the corporate market, falls into the category \textit{Employee Training}.

\section{The Most Common Type}
\label{s:sg_most_common_type}
To find out which category includes the largest amount of Serious Games, the sizes of the markets as well as the distribution of the purposes are investigated. 

At first the markets are explored.
According to Sawyer's presentation \cite{sg_taxonomy}, which he held at the \textit{Serious Games Summit 2008}, the education market was leading by that time.
Investigations of the author's previous work also yielded the same result for 2014. Based on the database of a collaborative classification system for Serious Games\footnote{\url{http://serious.gameclassification.com/EN/about/article.html} [January 2016].} the education sector remained the leading market for Serious Games with a share of 42\% as shown in figure \ref{graphic:marketchart_sgc}.

\begin{figure}[ht!]
\centering
\includegraphics[width=\textwidth]{seriousgames/marketchart_gameclassification_com}
\caption{Distribution of Serious Games by market according to the database of serious.gameclassification.com in 2014 (Source: \cite{ba_kk})}
\label{graphic:marketchart_sgc}
\end{figure} 

\noindent
Looking at the distribution of Serious Games by purpose, illustrated by figure \ref{graphic:purposechart_sgc}, education seems to outweigh the other categories again. With a share of 44\%, it becomes obvious that, according to the classification database, most of the Serious Games are made for educational purpose.

\begin{figure}[ht!]
\centering
\includegraphics[width=\textwidth]{seriousgames/purposechart_gameclassification_com}
\caption{Distribution of Serious Games by purpose according to the database of serious.gameclassification.com in 2014 (Source: \cite{ba_kk})}
\label{graphic:purposechart_sgc}
\end{figure}

After the biggest market and the most frequently used intention have been identified, the most common type can be determined. 
By having a look at Sawyer's taxonomy (see figure \ref{graphic:taxonomy}) and applying the above mentioned results, this type turns out to be \textit{Learning}.
This means that most of the existing Serious Games can be classified as belonging to this type.
Furthermore, this conclusion conforms to Sawyer's and Smith's projected taxonomy from 2008, which illustrates the distribution of Serious Games to the respective categories (see figure \ref{graphic:projected_taxonomy}). 

\begin{figure}[ht!]
\centering
\includegraphics[width=0.75\textwidth]{seriousgames/Sawyer_SeriousGamesProjectedTaxonomy}
\caption{Projected taxonomy of Serious Games by Ben Sawyer and Peter Smith (2008, Source: \cite{sg_taxonomy})}
\label{graphic:projected_taxonomy}
\end{figure}

\noindent
By showing that the biggest amount of (their observed) Serious Games belong to the combination of the educational market and the educational purpose (the learning genre), the result is confirmed once more.

\section{Application in Education}
\label{s:sg_application_in_edu}
Although Serious Games are recognized as a modern educational concept (see subsection \ref{ss:sg_as_edu_concept} below), their application still needs to emerge and find greater acceptance. Kindergartens, schools, colleges, and other educational institutions could profit a lot from Serious Games, if they used them. To support this statement, this section explains why it makes sense to use Serious Games as a new teaching and learning concept.

\subsection{Serious Games as Educational Concept}
\label{ss:sg_as_edu_concept}
The idea of using media for teaching and/or learning is not new. 
There are many established educational concepts with both, analogue and digital media.
A conventional concept is learning and teaching by analogue media like texts or books.
Modern ones include the usage of digital media like the Internet or educational films.
Serious Games belong to these modern concepts.
Among them, they can be positioned as shown in figure \ref{graphic:sg_educoncepts} (see also \cite{educoncepts}).

\begin{figure}[ht!]
\centering
\includegraphics[width=0.75\textwidth]{seriousgames/educational_concepts}
\caption{The Position of Serious Games Among Other Educational Concepts (Source: \cite{educoncepts})}
\label{graphic:sg_educoncepts}
\end{figure}

\noindent
It shows that the biggest application field of Serious Games belongs to the entertaining education field, meaning educational concepts that try to make learning enjoyable. 
The small part which is not settled within this field, refers to Serious Games that are not aiming on education (e.g. games for health).
On the contrary, the DGBL (Digital Game Based Learning) segment represents those whose sole purpose is teaching and/or learning, meaning all Serious Games that belong to the most common type (see \ref{s:sg_most_common_type}).
The main characteristic of DGBL is that knowledge/education is imparted automatically while interacting playfully with the game.

\subsection{Didactic}
\label{ss:sg_didactic}
From the didactic point of view, there is a huge potential of using digital and interactive media, such as Serious Games, as a form of teaching and/or learning.
Michael Kerres, a German pioneer of E-Learning concepts, names the following important potentials of that usage (see \cite{mediendidaktik_kerres}): 

\begin{itemize}
\item Support of other learning concepts and new qualities of learning,
\item A different organisation of learning, and
\item Shortened learning periods.
\end{itemize}

\noindent
Projected to Serious Games, those potentials apply as follows.

\subsubsection{Support of other learning concepts and new qualities of learning}
By a serious game's media elements like images, videos or animations, certain learning contexts can be perfectly illustrated.
This illustration can be used to support other teaching approaches such as courses at university.
Furthermore, learners can project their thoughts cognitively and emotionally into the learning tasks of a Serious Game.
Thus they establish a good understanding and retention of the solved tasks.

\subsubsection{A different organisation of learning}
Serious Games give more flexibility in the organisation of teaching and learning.
In the contrary to conventional learning concepts like lectures at university, the learners are not necessarily bound to fix learning periods or learning places, when playing Serious Games.
Serious Games can be played at home and at any time, and if they are well designed, they do not need a teacher nearby to explain the addressed topics.
Thus, these games become a useful complement for teaching and learning concepts.
Especially students in correspondence courses or distance learning courses can profit from the usage of Serious Games, as those are available at any time and any place and illustrate the topics in a vivid way.

\subsubsection{Shortened learning periods}
In school classes, for example, the teacher determines the pace of teaching.
On the one hand, this can slow down some students' progress, while on the other hand some students may be lost by the teacher.
Players of Serious Games, however, can determine and adjust the way they play/learn.
This means they can regulate their pace of learning and, if required, repeat specific tasks to deepen their understanding.
Thus, shortened learning periods (in average of the group) can be reached.

\subsection{Serious Games in Classrooms}
\label{ss:sgs_in_classrooms}
Taking the aforementioned didactic potentials (see \ref{ss:sg_didactic}) directly into the environment of class rooms, even more benefits can be spotted.

\subsubsection{Supporting the Teacher}
Will Wright, a famous game designer, once said:
\begin{quote} "If a learner is motivated, there's no stopping him [or her]". \end{quote}

\noindent
However, a teacher's task of motivating students to delve into (often as dry perceived) school subjects can be quite difficult.
To ease this, Serious Games can be used as appetizers for the respective subjects. 
Introducing a topic in a playful way might raise the students' interest before going more into depth, causing them to pay more attention when it comes to the difficult parts.

Furthermore, teachers can have difficulties handling large class-sizes. 
Compared to small school classes, large ones deliver lower scores in standardised tests and have problems with behavioural issues (see \cite{class_size_research}). 
Additionally teachers of large classes feel less productive and efficient. 
Those problems are partially caused by the fact that teachers can not adequately take care of each and every student at the same time.
Whereas some students understand difficult topics quite fast, others have problems and need individual help from the teacher. 
However, while the weak are receiving personal help, the strong ones need to be kept busy.
Since games can contain different levels of difficulty which can be designed as easy or complex as one likes, they seem to fit perfect for this need.
Thus, Serious Games can be used to keep the strong students busy by higher levels of difficulty while teachers help the weaker ones on the one hand.
On the other hand, the lower levels of difficulty can be used by the weaker students to stabilise their understanding of the respective subject. 

\subsubsection{Multiplayer Environment}
Nowadays most schools possess one or more computer labs which students can use to learn about IT subjects or do some research on the Internet.
The computers in those labs are usually connected to the same network (and/or to the Internet).
Similar to LAN parties, where people gather with their computers to play video games together, those computer rooms provide a good basis for a multiplayer environment.
This allows the use of multiplayer Serious Games in the respective schools, while the students are able to sit and play next to each other.

As an e-Learning research laboratory in Spain found out, learning together through multiplayer games improves the students' social and cognitive development in several ways.
According to their investigation \cite{vision_from_collaborative_learning}, students learn more and become more positive about school, subjects, teachers, and each other, when they work cooperatively on the same subject.
Furthermore they state "thanks to collaborative learning, individuals can obtain skills which they would not otherwise have due to the positive imbalance which occurs between pupils" \cite{vision_from_collaborative_learning}.
Thus, the potential of using multiplayer Serious Games in classes becomes obvious. 

\subsection{Current Trend}
\label{ss:sg_edu_state_of_the_art}
The amount of published Serious Games is growing steadily.
As stated in \cite{ba_kk} and illustrated by figure \ref{graphic:timegraph_sg}, the advent of the Serious Games Wave and the release of mobile touch devices in 2007 caused that the number of Serious Games has more than doubled over the past decade.

\begin{figure}[ht!]
	\centering
	\includegraphics[width=\textwidth]{seriousgames/timegraph_sg}
	\caption{Growth of serious games since 1980, according to the database of serious.gameclassification.com in Jan. 2014. (Source \cite{ba_kk})}
	\label{graphic:timegraph_sg}
\end{figure}

Although those numbers are a great indicator for the need and acceptance of Serious Games, schools seem to use them hardly.
Local schools in South West Germany report that many games can not be used in classes because they do not really fit their curriculum or they are not supported by their used platforms (personal communication, November 2015). 
However, there are a few games which are actually used.
The Saarpfalz-Gymnasium in Homburg, for example, uses some games from the (analogue) Serious Games collection \textit{Computer Science Unplugged}\footnote{\url{http://csunplugged.org/} [January 2016].}.
Those games are positive examples, especially because they were designed to be used in classrooms and help teachers to guide students into the subject of computer science.
Many other Serious Games, however, are designed for very specific target audiences, which makes them incompatible for most classes. 
%This could be because they are developed for certain companies (employee education)
%In large classes, this might cause problems as described in section \ref{ss:sgs_in_classrooms}.

Another notable aspect in regards of current Serious Games is that most of them do not support multiplayer functionality. 
Even though there are a lot of Serious Games and even though some of them are actually used in classes, most of them automatically exclude the multiplayer benefits addressed in subsection \ref{ss:sgs_in_classrooms}.
A quick search for the keyword \textit{multiplayer} in the aforementioned database of the collaborative classification system yielded only 51 results of over 3200 entries.
Additionally, only 24 entries were found when looking for the keyword \textit{coop}, which should yield cooperative multiplayer games.
This is consistent with the results of investigations on multiplayer functionality of several mobile Serious Games in Google's \textit{Play Store}
\footnote{Google's official store and portal for Android apps, games and other content for Android-powered devices. \url{https://play.google.com/store} [January 2016].}.

Summarising the current trend, it can be said that there are a lot of published Serious Games but most of them do not fit for the specific needs of specific schools.
In Addition, most of them do not release their full potential because of ignoring multiplayer functionalities. 
Thus, it seems a more mature development approach for Serious Games is required in order to offer more adequate games for schools.


%\section{Didactic Evaluation of LearnIT}
%\subsection{Purpose}
%Sinn und Zweck der Evaluation beschreiben.
%Was soll untersucht werden und warum?\newline
%--\textgreater Eher in Richtung "Macht es Sinn SG im Unterricht einzusetzen?"\newline
%bzw. "war das spiel als appetizer wirksam?", "hat es dein interesse am thema geweckt"...

%\subsection{Procedure / Approach / Method / Execution}
%Vorgehensweise: Welche daten werden wie von wem erhoben?
%-evtl. zwei eval. runden: vor SG nutzung vs. nach SG nutzung.

%\subsection{Result}
%Auswertung und Interpretation der gesammelten Daten
%wenn positiv: rechtfertigt sich weiter mit thematik zu beschaeftigen
