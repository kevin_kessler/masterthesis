\chapter{Introduction}
\label{c:introduction}
%\printinunitsof{cm}\prntlen{\textwidth}

Pong, Pac-Man, Super Mario and World of Warcraft are the titles of some of the most famous products of the video game industry, which keeps growing rapidly since the mid-1970s.
Nowadays video games are a well integrated part of our society as the industry's annual revenue of more than \$22bn confirms (see \cite{ESA_EF15}). 
The majority of video games are intended to give players an entertaining experience in order to make as much financial profit as possible.
Since 2002, however, a wave of games that are used for purposes other than entertainment has emerged. 
Those games often address markets like education, information or training and are known as \textit{Serious Games} because they are used in a more serious context than their solely entertaining relatives.

Over the past decade Serious Games gained more and more attention, since they are successfully utilised in many different industries such as defence, healthcare and education.
While institutions from the healthcare sector, for example, use them for rehabilitation or staff training, educational institutions use Serious Games to explain and teach difficult subjects in a compelling way.

\section{Motivation}
\label{s:intro_motivation}

In contrast to their entertainment counterparts, Serious Games often address very specific target audiences such as visitors of a research institution or students of a local school.
Accordingly, Serious Games are usually bound to low sales which affects their development significantly because low sales results in low budget.
Since time is money, developing software with a low budget usually comes with tight schedules.
As a consequence, Serious Games often suffer from sloppy implementation and lack of motivational aspects which results in not being well received by their audience.
Hence, the development process should be supported by tools which are designated for Serious Games development and which enable developers to produce better games with the same amount of time and money.

As previous work \cite{ba_kk} yielded, the game engine Unity3d is a tool which suits well for Serious Games development.
However, it comes with certain pitfalls that inexperienced developers can easily run into.
Furthermore, Unity's recent releases come with a new GUI system as well as a new networking system.
For either of both there is currently not much content available which causes developers to implement common requirements from scratch instead of being able to build upon reusable packages.
Accordingly, working with the new systems can be challenging but it also offers the opportunity to contribute useful packages to the community. 

Especially Unity's new networking system seems to be interesting for Serious Games development.
Currently there are very few Serious Games with (networking) multiplayer capabilities because their development is often expensive and can take a lot of time.
However, multiplayer games can be very motivating for players which is an important aspect for Serious Games since they often have to present dry subjects in an appealing way.
Hence, the process of Serious Games creation should especially be supported in the networking domain.

\section{Goals}
\label{s:intro_goals}

As described above, Serious Games development often suffers from low budgets and tight schedules.
To be able to create successful Serious Games in spite of these difficulties, the development process must be simplified and accelerated which is the main goal of this thesis.

In order to achieve this, it is necessary to find out what makes good Serious Games and how they can be developed.
Another goal of this thesis is to develop best practices in respect to working with Unity since they might result in stable, time saving and thus cost efficient strategies.
Additionally, the thesis aims to yield reusable Unity packages for convenient substitution of common Serious Games development tasks including networking activities.
Altogether those achievements are subject to result in a framework for Serious Games development with Unity.
The idea of the framework is to allow to produce games easier and faster and thereby enable developers to include more features (including multiplayer) and create a better experience without needing to increase the project's budget or to postpone the deadline.
Accordingly, the main goal of the thesis is intended to be reached by developing and providing such a framework.

\section{Collaboration}
\label{s:intro_collab}
Serious Games are often used for educational purposes.
Accordingly, it makes sense to acquire the above mentioned approaches (best practices, packages, framework) by going through the process of creating educational Serious Games in the course of this thesis.
To make this scenario as realistic as possible, those games are developed in close collaboration with the local German high schools Saarpfalz-Gymnasium and Helmholtz-Gymnasium.
Regular meetings and tests with teachers and students are used to acquire insight in how appropriate the respective approaches are and in how far they need to be adjusted.
Accordingly, the results of these observations flow into the final approaches presented in this thesis.  
As an extra, the resulting games which will be designated for the usage in classrooms, are intended to be utilised in classes of the mentioned schools wherever they can support the curriculum. 

\section{Previous Work}
\label{s:intro_previous_work}
This thesis can be read as continuation of the author's Bachelor's Thesis: \textit{Serious Games Development with Modern Game Engines} \cite{ba_kk}.

As its title implies, the Bachelor's Thesis also discussed the topic of Serious Games.
It was written at the Education Group of the European Organisation for Nuclear Research (CERN) in Switzerland.
CERN's Media Lab, which is part of the Education Group, is responsible for explaining the organisation's complex experiments through media including Serious Games.
Accordingly, the author dealt with this topic during his twelve-month stay at the Media Lab.
Thus, it was chosen as subject for the investigations of the Bachelor's Thesis.

In concrete terms, \cite{ba_kk} is a case study that determines the most suitable game engine for Serious Games development.
Therefore, it discusses theoretical details of Serious Games and game engines.
The engines then are subject of a comparison that ultimately yields the best suitable one.
To verify the capabilities of the outcome, the determined engine is used to develop a prototyped Serious Game which then is used to evaluate the result. 

In sum, the Bachelor's Thesis served to find out \textit{what} engine should be used for Serious Games development.
In contrast to that, the here presented Master's Thesis will investigate \textit{how} to use that engine efficiently for Serious Games development (see section \ref{s:intro_goals}).

\section{Structure of the Thesis}
\label{s:intro_structure}
To provide an overview of the strategy and structure of the thesis, each chapter's content will be outlined briefly in the following.

While the current chapter explains the motivation and purpose of the thesis, chapter \ref{c:seriousgames} is used to explain the term \textit{Serious Game} in detail.
Furthermore, the different purposes and fields of application of Serious Games are discussed and used to introduce a classification system for them as well as to determine their most common type.
As aforementioned, Serious Games for classrooms will be developed in collaboration with local schools as a part of this thesis. 
Hence, the chapter closes by discussing benefits of utilising Serious Games for education.

The thesis continues by investigating characteristics of good Serious Games in chapter \ref{c:sgprocess}, which are required to determine how to develop successful ones.
Additionally, the chapter provides insight to a development approach which can be used as a guide or adviser for general Serious Games development.
Then, common problems from which the development of Serious Games often suffers are discussed in order to emphasize the need for aids that ease and accelerate development.

Chapter \ref{c:sgunity} begins by introducing the game engine Unity3d which serves as basis of the mentioned aids.
It continues by investigating the engine's important development aspects (including GUI and networking) as well as common related problems from which best practices are derived.
Then the chapter presents reusable Unity packages which emerged in the course of this thesis on the basis of the developed best practices and the previously mentioned good Serious Game characteristics.
The chapter closes by discussing the disadvantages of solely relying on reusable packages and suggesting to comprise them into a framework instead.

Chapter \ref{c:sgframework} takes up the framework idea and starts by explaining the essentials of frameworks in general.
Afterwards the framework for Serious Games development, which is developed in the course of this thesis, is introduced and explained in detail.
Then the games that emerged in collaboration with the schools and by using the framework serve as showcase in order to present the framework's capabilities.
Furthermore, the challenges that had to be faced during the process of development are discussed.

In addition to the showcase example of chapter \ref{c:sgframework}, chapter \ref{c:evaluation} evaluates the framework in respect to the common problems of Serious Games development discussed in chapter \ref{c:sgprocess}.
This evaluation serves to examine whether the developed implementation aids (best practices, packages and framework) fulfil the main goal of the thesis, namely simplifying and accelerating the development of Serious Games.

Finally, in chapter \ref{c:conclusion} the results of the thesis are summarised and a prospect of potential future work is given.