\chapter{Evaluation of the Framework}
\label{c:evaluation}
Subsection \ref{ss:sgprocess_problems_solution} suggested to utilise a framework as a possible solution that addresses the introduced Serious Game development problems of section \ref{s:sgprocess_problems}.
Now that such a framework has been developed and introduced it is time to evaluate in how far it contributes to the solution of those problems, which is what this chapter is about.

\section{Purpose and Method}
As aforementioned, all introduced problems are related to implementation and result ultimately in delay and/or additional development costs.
Accordingly, the framework aims to address these problems in order to ease implementation and thereby reduce the development durations and costs.
Thus, the purpose of the evaluation is to find out in how far the framework manages to fulfil this aim.

Ralph Johnson, a member of the famous \textit{Gang of Four}\footnote{See \url{http://c2.com/cgi/wiki?GangOfFour} [February 2016].}, states in \cite{framework_entwicklung_paper} that there is no magic formula for evaluating frameworks.
However, he mentions that you should make a check list of features that you want the framework to support and then see if it does.
In the case of this evaluation, the check list will consist of the problems of subsection \ref{s:sgprocess_problems}.
This means that they will again be subject of discussion, but this time from the perspective of how far the framework contributes to their solution.
If it does address and solve them, it definitely improves development (simplify and accelerate) which is the main value of a framework (cf. \cite{framework_entwicklung_paper}).

\section{Evaluation}
\subsubsection{Dry Subjects}
Subsection \ref{ss:sgprocess_problems_dry_subjects} explained that Serious Games often have to make dry subjects more appealing to their target audience.
As explained in subsection \ref{ss:what_makes_good_sg_motivation}, there are several techniques that help to achieve this, but implementing them from scratch can take a lot of time.
Accordingly, the framework should offer support in this respect.

\begin{itemize}
\item \textbf{Feedback and Help} - The framework offers feedback mechanics in various forms.
Since it comprises the in section \ref{s:sgunity_packages} introduced packages, developers can use dialog windows, flashers, sliders and loading indicators to provide adequate feedback whenever needed.
Furthermore, the framework itself already gives feedback where appropriate. For example, loading indicators are active while a user is waiting for a match request or while connecting to the lobby.
Additionally, the framework is capable to offer mode specific help to users.
Integrated game modes can have tutorial videos attached, which can be used to explain how to play the mode.
Furthermore the pause menu of singleplayer modes can optionally offer a help button which can be used to show customisable help content.
Accordingly, the feedback and help aspects are fully covered by the framework.
\item \textbf{Portability} - As aforementioned, Unity provides multi-platform support, which allows to deploy created games on any of the modern platforms.
Since the framework is based on Unity, it profits from the same benefits. Accordingly, games created by the framework are also compatible to multiple platforms.
Furthermore, the GUI that comes with the framework is designed responsively, so that it fits on different screen sizes including mobile phones.
However, when developing concrete game implementations, developers need to take care of their game's UI (including input controls) to make it suitable for the platforms they want to address.
\item \textbf{Multiplayer} - As described in section \ref{s:sgframework_myfw} the framework manages the whole match making process for two-player games in a LAN. 
This includes scanning the network, exchanging match requests and responses, and redirecting players to the same virtual lobby room in which they can communicate and configure.
Furthermore, it frames the actual multiplayer gameplay by a synchronised start and a synchronised end, on which the players again are redirected to the lobby.
The only multiplayer aspects developers have to provide themselves, is the actual gameplay which is game specific and thus not covered.
Accordingly, the motivational factor \textit{Multiplayer}  is well covered for local area networks.
Multiplayer via Internet, however, is not yet supported and could be addressed by future work. 
\item \textbf{Immersion} - The immersion aspect is partially covered. 
The framework offers simple audio and video playback via its \lstinlined{AudioPlayer} and \lstinlined{VideoDialog} components. 
Thus, serious content can be accompanied and emphasised by audiovisual content. 
Concrete game world immersion, however, is game specific and thus not covered by the framework. 
\item \textbf{Progress and Rewards} - In the current version, the framework is lacking of general progress and reward mechanics. 
This aspect had low priority during development, since the focus lay on developing game modes for class rooms. 
The collaborating teachers did not find it necessary or could even imagine to find it restricting if, for example, certain game modes were locked by default.
They rather want to be able to access any content whenever it fits their curriculum. 
Furthermore, there is no ranking or score support yet. 
In a networked multiplayer environment, this would require individual (central) servers that hold user- and score-information which is why this feature was on low priority for classrooms, too.
In general, however, such features are a good motivational factor and thus should be added in future work.
\end{itemize}

\noindent
Briefly summarised, it can be said that four of the five motivational aspects for countering dry subjects are well supported by the framework.

\subsubsection{Interdisciplinary Difficulties}
In subsection \ref{ss:sgprocess_problems_interdisc} the characteristics of endogenous and exogenous games have been discussed.
While endogenous games are usually designed for the specific serious content, exogenous games provide interfaces to attach customisable serious content.
The disadvantages of solely using one of both have been discussed and it has been stated that combining both approaches could resolve the issues.

Hence, the framework comes with a mixture of endogenous and exogenous approaches, which allows to use the advantages of both.
While the white-box parts of the framework can be used to implement exogenous games, the existing black box parts already offer endogenous game modes.
The theory and quiz modes are reusable in a generic way since their serious content is based on exchangeable XML files.

In the example of the presented showcase (see section \ref{s:sgframework_showcase}), the computational complexity game is a combination of endogenous and exogenous approaches since the graph coloring game modes are endogenous while the theory and quiz modes are exogenous.

In sum, the framework supports both types of games, comes with ready to use exogenous game modes and can be extended by either of both.

\subsubsection{Engine Dependant Difficulties}
Subsection \ref{ss:sgprocess_problems_engine} described the problem of how complex game engines can be and that inexperienced developers can work into the wrong direction easily.
Here is where the main characteristics of a framework really pay off.
As stated in section \ref{s:sgframework_frameworks_in_general}, frameworks predefine a software's architecture and interplay resulting in a frame that guides developers within certain barriers.
In the case of the here presented framework this is realised by reducing the complexity of development to the implementation of gameplay.
Everything else, from the start of the application up to the point where the actual game mode has been loaded, is handled by the frame.
But the framework's support goes even beyond that.
Since the white-box parts also define a structure (hook invocation chain) and since stable packages can be used during concrete implementation, the framework also supports the development of gameplay.

Furthermore, the framework reduces the amount of required Unity specific knowledge.
For example, the \lstinlined{AudioPlayer} and \lstinlined{VideoDialog} components offer simple media playback without requiring developers to know anything about Unity's audio or video system.
Also, the framework manages all networking tasks up to where the network gameplay begins, which means that developers neither have to struggle with UNET's lower level components nor do they have to care about the network player management.

Additionally, the framework and its packages are based on the well established MVC architecture pattern (see section \ref{s:sgframework_myfw}).
Thus, it forces developers to build upon on a stable architecture instead of giving them the chance to come up with something own, which might lead into the wrong direction.

Altogether, the framework successfully contributes to the workflow of developers, helping them to avoid engine dependant pitfalls on their way to their Serious Game.

\subsubsection{Low Budget and Tight Schedule}
As the above addressed problems all affect the duration of development, solving them can already decrease the required time significantly.
In general, the framework accelerates development because it takes care of many common tasks which otherwise would have to be implemented and tested by oneself.
Furthermore, it offers tools and controls in form of reusable packages to work with.

As Pree states in his book \cite{framework_entwicklung_buch}, developers have to write much less code (up to 90\%) when concreting a framework to an application in comparison to when creating the same application the conventional way.
The presented framework tries to achieve this by reducing the amount of required work to the minimum, namely implementing the individual gameplay (see section \ref{s:sgframework_myfw}).

Unfortunately, this is hardly quantifiable since the differences between developing a game with and without the framework always depend on the game concept and the context.
If, for example, one would want to create a simple singleplayer Serious Game without wanting to develop further games in future it would probably not be worth to learn how to use the framework first. 
However, if one wants to develop a LAN based multiplayer Serious Game, one would surely benefit from using the framework since it takes care of a lot of networking which one otherwise would need to learn and implement by oneself.

In any case, the framework provides a solid architecture and a stable frame with a defined interplay of components.
Thus, several planning steps such as defining the architecture of the game can be omitted.
Furthermore, less testing is necessary when using the framework, because less code needs to be written and the interplay between the components is already set (obviates modification).
Accordingly, the amount of time consuming and costly tasks is reduced which contributes to the solution of the tight schedules and low budgets problem.

\section{Result}
In summary, the evaluation yielded that each of the problems is being addressed by the framework.
Some of them are solved more directly than others, but all of them are treated at least partially.
Accordingly, the goal of simplifying and accelerating the development process of Serious Games has been reached.

Unfortunately, there is no metric by which one could measure in how far it simplifies or accelerates exactly.
A possible indicator could be the lines of code one would write for a concrete game implementation, however, this is context dependant as mentioned above.

Furthermore, there are parts which are currently not included or covered by the framework such as the progress and reward aspects (see also section \ref{s:future_work} below).
However, in the light of the fact that most frameworks require many iterations and a lot of hard work (cf. \cite{framework_entwicklung_buch} and \cite{framework_entwicklung_paper}) until they are mature, this was to be expected.

As a result, the framework is currently useful for creating specific types of Serious Games such as collaborative multiplayer ones.
However, it also comes with certain restrictions such as that the match making currently only supports local area networks.
As the framework grows (via future work) it will become less restrictive but also more complex, which can make it harder to understand.
Accordingly, you need to ponder whether the framework suits your intended context before you decide to use it. 

Simply put, the framework successfully simplifies and accelerates the development of specific types of Serious Games. 