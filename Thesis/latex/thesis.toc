\select@language {english}
\select@language {ngerman}
\select@language {english}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Motivation}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Goals}{2}{section.1.2}
\contentsline {section}{\numberline {1.3}Collaboration}{2}{section.1.3}
\contentsline {section}{\numberline {1.4}Previous Work}{2}{section.1.4}
\contentsline {section}{\numberline {1.5}Structure of the Thesis}{3}{section.1.5}
\contentsline {chapter}{\numberline {2}Serious Games}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}Definition}{5}{section.2.1}
\contentsline {section}{\numberline {2.2}Classification}{6}{section.2.2}
\contentsline {section}{\numberline {2.3}The Most Common Type}{8}{section.2.3}
\contentsline {section}{\numberline {2.4}Application in Education}{10}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}Serious Games as Educational Concept}{10}{subsection.2.4.1}
\contentsline {subsection}{\numberline {2.4.2}Didactic}{10}{subsection.2.4.2}
\contentsline {subsubsection}{Support of other learning concepts and new qualities of learning}{11}{section*.10}
\contentsline {subsubsection}{A different organisation of learning}{11}{section*.11}
\contentsline {subsubsection}{Shortened learning periods}{11}{section*.12}
\contentsline {subsection}{\numberline {2.4.3}Serious Games in Classrooms}{11}{subsection.2.4.3}
\contentsline {subsubsection}{Supporting the Teacher}{11}{section*.13}
\contentsline {subsubsection}{Multiplayer Environment}{12}{section*.14}
\contentsline {subsection}{\numberline {2.4.4}Current Trend}{12}{subsection.2.4.4}
\contentsline {chapter}{\numberline {3}Serious Games Development}{14}{chapter.3}
\contentsline {section}{\numberline {3.1}Characteristics of Good Serious Games}{14}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Scope}{14}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Essentials}{15}{subsection.3.1.2}
\contentsline {subsubsection}{Clearly Defined Purpose}{15}{section*.16}
\contentsline {subsubsection}{Different Levels of Difficulty}{15}{section*.17}
\contentsline {subsection}{\numberline {3.1.3}Motivation}{16}{subsection.3.1.3}
\contentsline {subsubsection}{Progress and Rewards}{16}{section*.19}
\contentsline {subsubsection}{Balanced Immersion}{18}{section*.23}
\contentsline {subsubsection}{Feedback and Help}{19}{section*.24}
\contentsline {subsubsection}{Portability}{19}{section*.25}
\contentsline {subsubsection}{Multiplayer Modes}{19}{section*.26}
\contentsline {subsection}{\numberline {3.1.4}Multiplayer}{20}{subsection.3.1.4}
\contentsline {section}{\numberline {3.2}Development Process}{22}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Roles}{23}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Process Model}{24}{subsection.3.2.2}
\contentsline {section}{\numberline {3.3}Problems}{27}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Low Budget}{27}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Tight Schedule}{27}{subsection.3.3.2}
\contentsline {subsection}{\numberline {3.3.3}Dry Subjects}{28}{subsection.3.3.3}
\contentsline {subsection}{\numberline {3.3.4}Interdisciplinary Difficulties}{28}{subsection.3.3.4}
\contentsline {subsection}{\numberline {3.3.5}Engine Dependant Difficulties}{28}{subsection.3.3.5}
\contentsline {subsection}{\numberline {3.3.6}Approach to Solution}{29}{subsection.3.3.6}
\contentsline {chapter}{\numberline {4}Developing Serious Games with Unity 5}{30}{chapter.4}
\contentsline {section}{\numberline {4.1}Why Unity?}{30}{section.4.1}
\contentsline {section}{\numberline {4.2}Essentials}{31}{section.4.2}
\contentsline {section}{\numberline {4.3}GUI}{35}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Relevance}{35}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}UGUI}{35}{subsection.4.3.2}
\contentsline {subsection}{\numberline {4.3.3}Problems}{35}{subsection.4.3.3}
\contentsline {subsection}{\numberline {4.3.4}Best Practices}{38}{subsection.4.3.4}
\contentsline {section}{\numberline {4.4}Networking}{40}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}Relevance}{40}{subsection.4.4.1}
\contentsline {subsection}{\numberline {4.4.2}UNET}{40}{subsection.4.4.2}
\contentsline {subsection}{\numberline {4.4.3}Problems}{43}{subsection.4.4.3}
\contentsline {subsubsection}{Poor Documentation}{43}{section*.40}
\contentsline {subsubsection}{Limitations}{44}{section*.41}
\contentsline {subsection}{\numberline {4.4.4}Best Practices}{44}{subsection.4.4.4}
\contentsline {subsubsection}{Debugging}{44}{section*.42}
\contentsline {subsubsection}{NetworkBehaviour}{45}{section*.43}
\contentsline {subsubsection}{Other}{51}{section*.46}
\contentsline {section}{\numberline {4.5}Packages}{52}{section.4.5}
\contentsline {subsection}{\numberline {4.5.1}Dialog Windows}{52}{subsection.4.5.1}
\contentsline {subsubsection}{Description}{52}{section*.47}
\contentsline {subsubsection}{Purpose}{53}{section*.49}
\contentsline {subsubsection}{Usage}{53}{section*.50}
\contentsline {subsection}{\numberline {4.5.2}Flashers}{54}{subsection.4.5.2}
\contentsline {subsubsection}{Description}{54}{section*.51}
\contentsline {subsubsection}{Purpose}{55}{section*.53}
\contentsline {subsubsection}{Usage}{55}{section*.54}
\contentsline {subsection}{\numberline {4.5.3}Keypad}{55}{subsection.4.5.3}
\contentsline {subsubsection}{Description}{55}{section*.55}
\contentsline {subsubsection}{Purpose}{55}{section*.57}
\contentsline {subsubsection}{Usage}{56}{section*.58}
\contentsline {subsection}{\numberline {4.5.4}Sliders}{57}{subsection.4.5.4}
\contentsline {subsubsection}{Description}{57}{section*.59}
\contentsline {subsubsection}{Purpose}{57}{section*.61}
\contentsline {subsubsection}{Usage}{57}{section*.62}
\contentsline {subsection}{\numberline {4.5.5}Loading Indicator}{58}{subsection.4.5.5}
\contentsline {subsubsection}{Description}{58}{section*.63}
\contentsline {subsubsection}{Purpose}{59}{section*.65}
\contentsline {subsubsection}{Usage}{59}{section*.66}
\contentsline {subsection}{\numberline {4.5.6}Network Transmitter}{60}{subsection.4.5.6}
\contentsline {subsubsection}{Description}{60}{section*.67}
\contentsline {subsubsection}{Purpose}{61}{section*.69}
\contentsline {subsubsection}{Usage}{61}{section*.70}
\contentsline {subsection}{\numberline {4.5.7}Network Discovery Service}{63}{subsection.4.5.7}
\contentsline {subsubsection}{Description}{63}{section*.71}
\contentsline {subsubsection}{Purpose}{64}{section*.73}
\contentsline {subsubsection}{Usage}{64}{section*.74}
\contentsline {subsection}{\numberline {4.5.8}Network Player Selection}{65}{subsection.4.5.8}
\contentsline {subsubsection}{Description}{65}{section*.75}
\contentsline {subsubsection}{Purpose}{66}{section*.77}
\contentsline {subsubsection}{Usage}{66}{section*.78}
\contentsline {subsection}{\numberline {4.5.9}Network Lobby}{67}{subsection.4.5.9}
\contentsline {subsubsection}{Description}{67}{section*.79}
\contentsline {subsubsection}{Purpose}{67}{section*.81}
\contentsline {subsubsection}{Usage}{68}{section*.82}
\contentsline {section}{\numberline {4.6}Working with Packages}{70}{section.4.6}
\contentsline {chapter}{\numberline {5}Framework for Serious Games Development}{71}{chapter.5}
\contentsline {section}{\numberline {5.1}Frameworks in General}{71}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Characteristics}{71}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}Classification}{72}{subsection.5.1.2}
\contentsline {subsubsection}{White-Box-Frameworks}{72}{section*.83}
\contentsline {subsubsection}{Black-Box-Frameworks}{73}{section*.84}
\contentsline {subsubsection}{Frameworks in Practice}{73}{section*.85}
\contentsline {section}{\numberline {5.2}Implemented Framework}{73}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Architecture}{74}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}Frame}{75}{subsection.5.2.2}
\contentsline {subsubsection}{Game Selection Controller}{75}{section*.90}
\contentsline {subsubsection}{Game Manager}{75}{section*.91}
\contentsline {subsubsection}{Settings Controller}{76}{section*.92}
\contentsline {subsubsection}{Network Controller}{76}{section*.93}
\contentsline {subsubsection}{Peer-to-Peer System}{76}{section*.94}
\contentsline {subsubsection}{Game Menu Controller}{78}{section*.97}
\contentsline {subsection}{\numberline {5.2.3}Extension}{79}{subsection.5.2.3}
\contentsline {subsubsection}{SPGameController}{80}{section*.98}
\contentsline {subsubsection}{MPGameController and MPGamePlayer}{81}{section*.100}
\contentsline {subsection}{\numberline {5.2.4}Usage}{83}{subsection.5.2.4}
\contentsline {subsubsection}{Implementing a Singleplayer Game Mode}{85}{section*.103}
\contentsline {subsubsection}{Implementing a Multiplayer Game Mode}{85}{section*.104}
\contentsline {subsubsection}{Integrating Games and Game Modes}{87}{section*.105}
\contentsline {subsubsection}{Using Existing Game Modes}{88}{section*.106}
\contentsline {section}{\numberline {5.3}Showcase Project}{89}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Idea and Purpose}{89}{subsection.5.3.1}
\contentsline {subsection}{\numberline {5.3.2}Genre, Audience, and Platforms}{89}{subsection.5.3.2}
\contentsline {subsection}{\numberline {5.3.3}Roles}{90}{subsection.5.3.3}
\contentsline {subsection}{\numberline {5.3.4}Implemented Games and Game Modes}{90}{subsection.5.3.4}
\contentsline {subsubsection}{Theory Mode}{91}{section*.108}
\contentsline {subsubsection}{Quiz Modes}{91}{section*.110}
\contentsline {subsubsection}{Number Guessing Modes}{92}{section*.112}
\contentsline {subsubsection}{Graph Colouring Modes}{93}{section*.114}
\contentsline {subsubsection}{Noteworthy Implementation Aspects}{95}{section*.116}
\contentsline {section}{\numberline {5.4}Challenges}{95}{section.5.4}
\contentsline {chapter}{\numberline {6}Evaluation of the Framework}{97}{chapter.6}
\contentsline {section}{\numberline {6.1}Purpose and Method}{97}{section.6.1}
\contentsline {section}{\numberline {6.2}Evaluation}{97}{section.6.2}
\contentsline {subsubsection}{Dry Subjects}{97}{section*.117}
\contentsline {subsubsection}{Interdisciplinary Difficulties}{99}{section*.118}
\contentsline {subsubsection}{Engine Dependant Difficulties}{99}{section*.119}
\contentsline {subsubsection}{Low Budget and Tight Schedule}{99}{section*.120}
\contentsline {section}{\numberline {6.3}Result}{100}{section.6.3}
\contentsline {chapter}{\numberline {7}Conclusion}{101}{chapter.7}
\contentsline {section}{\numberline {7.1}Results}{101}{section.7.1}
\contentsline {section}{\numberline {7.2}Future Work}{102}{section.7.2}
\contentsline {chapter}{\numberline {A}XML Structure}{104}{appendix.A}
\contentsline {section}{\numberline {A.1}Theory XML}{104}{section.A.1}
\contentsline {section}{\numberline {A.2}Quiz XML}{105}{section.A.2}
\contentsline {chapter}{\numberline {B}DVD-ROM}{108}{appendix.B}
\contentsline {chapter}{Bibliography}{109}{appendix.B}
\select@language {english}
\select@language {english}
\select@language {english}
\select@language {english}
\select@language {english}
\select@language {english}
\select@language {german}
\select@language {english}
\select@language {english}
\select@language {german}
\select@language {english}
\select@language {english}
\select@language {english}
\select@language {english}
\select@language {english}
\select@language {english}
\select@language {english}
\select@language {english}
\select@language {english}
\select@language {german}
\select@language {english}
\select@language {english}
\select@language {german}
\select@language {english}
\select@language {english}
\select@language {english}
\select@language {english}
\select@language {english}
\select@language {english}
\select@language {english}
\select@language {ngerman}
\select@language {english}
