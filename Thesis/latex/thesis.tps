[FormatInfo]
Type=TeXnicCenterProjectSessionInformation
Version=2

[Frame0]
Flags=0
ShowCmd=1
MinPos.x=-1
MinPos.y=-1
MaxPos.x=-1
MaxPos.y=-1
NormalPos.left=4
NormalPos.top=26
NormalPos.right=1388
NormalPos.bottom=523
Class=LaTeXView
Document=thesis.tex

[Frame0_View0,0]
TopLine=0
Cursor=1265

[Frame1]
Flags=0
ShowCmd=1
MinPos.x=-1
MinPos.y=-1
MaxPos.x=-1
MaxPos.y=-1
NormalPos.left=4
NormalPos.top=26
NormalPos.right=1388
NormalPos.bottom=523
Class=LaTeXView
Document=pretext\abstract.tex

[Frame1_View0,0]
TopLine=0
Cursor=1169

[Frame2]
Flags=0
ShowCmd=1
MinPos.x=-1
MinPos.y=-1
MaxPos.x=-1
MaxPos.y=-1
NormalPos.left=4
NormalPos.top=26
NormalPos.right=1388
NormalPos.bottom=523
Class=LaTeXView
Document=chapter\concepts.tex

[Frame2_View0,0]
TopLine=176
Cursor=9499

[Frame3]
Flags=0
ShowCmd=1
MinPos.x=-1
MinPos.y=-1
MaxPos.x=-1
MaxPos.y=-1
NormalPos.left=4
NormalPos.top=26
NormalPos.right=1388
NormalPos.bottom=523
Class=LaTeXView
Document=appendix\A\info.tex

[Frame3_View0,0]
TopLine=0
Cursor=156

[Frame4]
Flags=0
ShowCmd=1
MinPos.x=-1
MinPos.y=-1
MaxPos.x=-1
MaxPos.y=-1
NormalPos.left=4
NormalPos.top=26
NormalPos.right=1388
NormalPos.bottom=523
Class=LaTeXView
Document=appendix\B\info.tex

[Frame4_View0,0]
TopLine=0
Cursor=466

[Frame5]
Flags=0
ShowCmd=1
MinPos.x=-1
MinPos.y=-1
MaxPos.x=-1
MaxPos.y=-1
NormalPos.left=4
NormalPos.top=26
NormalPos.right=1388
NormalPos.bottom=523
Class=LaTeXView
Document=pretext\kurzfassung.tex

[Frame5_View0,0]
TopLine=0
Cursor=173

[Frame6]
Flags=0
ShowCmd=1
MinPos.x=-1
MinPos.y=-1
MaxPos.x=-1
MaxPos.y=-1
NormalPos.left=4
NormalPos.top=26
NormalPos.right=1388
NormalPos.bottom=523
Class=LaTeXView
Document=chapter\jcd.tex

[Frame6_View0,0]
TopLine=0
Cursor=0

[Frame7]
Flags=0
ShowCmd=1
MinPos.x=-1
MinPos.y=-1
MaxPos.x=-1
MaxPos.y=-1
NormalPos.left=4
NormalPos.top=26
NormalPos.right=1388
NormalPos.bottom=523
Class=LaTeXView
Document=chapter\introduction.tex

[Frame7_View0,0]
TopLine=0
Cursor=4616

[Frame8]
Flags=0
ShowCmd=1
MinPos.x=-1
MinPos.y=-1
MaxPos.x=-1
MaxPos.y=-1
NormalPos.left=4
NormalPos.top=26
NormalPos.right=1388
NormalPos.bottom=523
Class=LaTeXView
Document=chapter\use_cases.tex

[Frame8_View0,0]
TopLine=0
Cursor=3506

[Frame9]
Flags=0
ShowCmd=1
MinPos.x=-1
MinPos.y=-1
MaxPos.x=-1
MaxPos.y=-1
NormalPos.left=4
NormalPos.top=26
NormalPos.right=1388
NormalPos.bottom=523
Class=LaTeXView
Document=chapter\hardware_details.tex

[Frame9_View0,0]
TopLine=0
Cursor=1705

[Frame10]
Flags=0
ShowCmd=1
MinPos.x=-1
MinPos.y=-1
MaxPos.x=-1
MaxPos.y=-1
NormalPos.left=4
NormalPos.top=26
NormalPos.right=1388
NormalPos.bottom=523
Class=LaTeXView
Document=chapter\conclusions.tex

[Frame10_View0,0]
TopLine=42
Cursor=6586

[Frame11]
Flags=0
ShowCmd=1
MinPos.x=-1
MinPos.y=-1
MaxPos.x=-1
MaxPos.y=-1
NormalPos.left=4
NormalPos.top=26
NormalPos.right=1388
NormalPos.bottom=523
Class=BibTeXView
Document=literature\literature.bib

[Frame11_View0,0]
TopLine=64
Cursor=2800

[Frame12]
Flags=0
ShowCmd=1
MinPos.x=-1
MinPos.y=-1
MaxPos.x=-1
MaxPos.y=-1
NormalPos.left=4
NormalPos.top=26
NormalPos.right=1388
NormalPos.bottom=523
Class=LaTeXView
Document=chapter\tools.tex

[Frame12_View0,0]
TopLine=230
Cursor=12014

[Frame13]
Flags=0
ShowCmd=1
MinPos.x=-1
MinPos.y=-1
MaxPos.x=-1
MaxPos.y=-1
NormalPos.left=4
NormalPos.top=26
NormalPos.right=1388
NormalPos.bottom=523
Class=LaTeXView
Document=chapter\current_state.tex

[Frame13_View0,0]
TopLine=132
Cursor=15251

[SessionInfo]
FrameCount=14
ActiveFrame=0

